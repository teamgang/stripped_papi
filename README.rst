# Public Notes
* git history here is wiped to prevent accidental leakage of keys

## TODO:
ci/cd pipeline to deploy api
add app logo to google oauth consent screen
  https://console.cloud.google.com/apis/credentials/consent?project=pushboy-d292b
in UI select message display format (bigpicture, default, etc)
rename dad to cad
load surfer form disk

search for lint rule
[dart library_private_types_in_public_api] [I] Avoid using private types in public APIs.                        https://dart-lang.github.io/linter/lints/library_private_types_in_public_api.html      


test sync user settings
test hidden bois
test security
test clashing routes
test money flow
test account in arrears code
test jwt refresh
test 24 hour apple auth check

create index columns as needed

on iOS images are an extension that is added from flutterfire docs

### art
krita
PikoPixel.app
grafx2
grabc

### Low Priority
* hide secrets, maybe with starlette.datastructres import Secret
* Make an interface for the app that feels lego-like; make a way way for people to learn how to program
* leverage time sensitive notifications in iOS
* don't delete projects/networks/channels. instead, move them to a deleted table. Or, set a deleted flag
* give developers option to autogenerate an example network with 2 channels
* follow related flutter/iOS blogs
* add btcpay
* api routes for everything
* users can create their own notification hooks (can we monetize this?)
* implement prometheus

## NOTES:
* testing image url = 'https://via.placeholder.com/350x150';
* credit cards are attached to developer accounts, not projects. if multiple 
  developers manage a project, payment settings might appear inaccurately
* google terms link: https://pushboi.io/legal/terms-users/latest
* google privacy policy link: https://pushboi.io/legal/privacy=policy-users/latest
* api/v1/surfer/signIn.withApple.callback

### boi_tx
* to edit boi transmission, you need to edit the files shipper.py and boi_tx.dart

* rabbitmq: http://localhost:15672/
  * papi/1234

# this is supposed to fix padding for python. do we need it?
# jin.static_icon = f"{jin.static_icon}{'=' * ((4 - len(jin.static_icon) % 4) % 4)}"

# fcm restrictions
fcm multicast limited to 500 devices at a time
message size limited to 4096 bytes
https://firebase.google.com/docs/cloud-messaging/send-message#send-messages-to-multiple-devices


# Architecture
Tying the basket to payment_attempt becomes a bit awkward when it comes to autopayment

# plugin notes
* uni links: requires intent filters
