#!/bin/sh

cd scripts/database/
./startDb.sh
cd ../rabbitmq/
./start.sh
cd ../..

cd src/papi
# TODO: disable this when https is set up
export OAUTHLIB_INSECURE_TRANSPORT=1

# can be postgresql; might support sqlite one day
export DB_CHOICE=postgresql
export PAPI_ENV=local
export PAPI_ROOT=$HOME/projects/papi
#uvicorn asgi:app --reload --host 0.0.0.0
uvicorn asgi:app \
	--reload \
	--uds /papi/tmp/papi.uvicorn.sock \
	--forwarded-allow-ips='*' \
	--proxy-headers 
