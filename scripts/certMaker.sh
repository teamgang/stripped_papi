#!/bin/sh

##
## CA certs and keys
##
# generate private key for CA
openssl genrsa 2048 > /papi/certs/ca-key.pem

# generate x509 certificate
openssl req \
	-new \
	-x509 \
	-nodes \
	-days 3650 \
	-key /papi/certs/ca-key.pem \
	-out /papi/certs/ca-cert.pem

##
## Rabbit server certs and keys
##
# private key and cert request for rabserver
openssl req \
	-newkey rsa:2048 \
	-nodes \
	-days 365000 \
	-keyout /papi/certs/rabserver-key.pem \
	-out /papi/certs/rabserver-req.pem

# this is to fix docker permissions. kinda sucks, but data is public
chmod 664 /papi/certs/rabserver-key.pem

# the X509 certificate for the rabserver
openssl x509 \
	-req \
	-days 365000 \
	-set_serial 01 \
	-in /papi/certs/rabserver-req.pem \
	-out /papi/certs/rabserver-cert.pem \
	-CA /papi/certs/ca-cert.pem \
	-CAkey /papi/certs/ca-key.pem

##
## Rabbit client certs and keys
##
# private key and cert request for rabclient
openssl req \
	-newkey rsa:2048 \
	-nodes \
	-days 365000 \
	-keyout /papi/certs/rabclient-key.pem \
	-out /papi/certs/rabclient-req.pem

# the X509 certificate for the rabclient
openssl x509 \
	-req \
	-days 365000 \
	-set_serial 01 \
	-in /papi/certs/rabclient-req.pem \
	-out /papi/certs/rabclient-cert.pem \
	-CA /papi/certs/ca-cert.pem \
	-CAkey /papi/certs/ca-key.pem
