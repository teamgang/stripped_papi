#!/bin/sh
imageName=$(cat IMAGENAME)
containerName=$(cat CONTAINERNAME)
sudo docker rm $containerName
sudo docker build \
	-t $imageName \
	-f ./postgresql.dockerfile ./

sudo docker run \
	--name $containerName \
	-p 5432:5432 \
	$imageName
