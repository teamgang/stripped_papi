FROM postgres:13.0-alpine
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres
ENV POSTGRES_DB papi
#ADD 00-db-init.sql /docker-entrypoint-initdb.d/
#ADD 11-db-characters.pgsql /docker-entrypoint-initdb.d/
EXPOSE 5432
