-- we can create a plan_demuxer table to lookup copies of a plan
-- rules: semi-colon separated list of rules
DROP TABLE IF EXISTS plans CASCADE;
CREATE TABLE plans (
	plan_id SERIAL,
	add_custom_price INT NOT NULL,
	add_custom_units INT NOT NULL,
	add_limitless_price INT NOT NULL,
	add_limitless_units INT NOT NULL,
	add_static_price INT NOT NULL,
	add_static_units INT NOT NULL,
	bundled_static INT NOT NULL,
	bundled_custom INT NOT NULL,
	bundled_limitless INT NOT NULL,
	monthly_credit INT DEFAULT 0,
	monthly_free_custom INT DEFAULT 0,
	monthly_free_limitless INT DEFAULT 0,
	monthly_free_static INT DEFAULT 0,
	monthly_free_static_edit_tokens INT DEFAULT 0,
	months INT DEFAULT 12,
	price INT NOT NULL,
	price_currency TEXT NOT NULL,
	static_edit_token_price INT NOT NULL,

	admin_notes TEXT NOT NULL,
	channel_limit INT NOT NULL,
	description TEXT NOT NULL,
	is_available BOOL NOT NULL,
	is_tailored BOOL NOT NULL DEFAULT false,
	rules TEXT NOT NULL,
	subscription_limit INT NOT NULL,
	title TEXT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (plan_id)
);


DROP TABLE IF EXISTS plan_change_requests CASCADE;
CREATE TABLE plan_change_requests (
	plan_change_id UUID NOT NULL,
	is_cancellation BOOL,
	developer_id UUID NOT NULL,
	plan_id_next INT,
	project_id UUID NOT NULL,
	terms_accepted_version TEXT,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (plan_change_id)
);
