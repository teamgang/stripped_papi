DROP TABLE IF EXISTS projects CASCADE;
CREATE TABLE projects (
	project_id UUID NOT NULL UNIQUE,
	creator_id UUID NOT NULL,
	credit INT DEFAULT 0,
	debt INT DEFAULT 0,
	default_plastic_id TEXT DEFAULT '',
	first_period INT NOT NULL,
	is_autopay_enabled BOOL DEFAULT false,
	is_bill_overdue BOOL DEFAULT false,
	is_closed BOOL DEFAULT false,
	is_in_arrears BOOL DEFAULT false,
	name TEXT DEFAULT '',
	notes TEXT DEFAULT '',
	plan_id INT DEFAULT 1,
	plan_id_next INT DEFAULT 1,
	time_last_bill_issued BIGINT DEFAULT 0,
	time_last_paid_off BIGINT DEFAULT 0,
	time_plan_expires BIGINT DEFAULT 0,

	custom_bought BIGINT NOT NULL DEFAULT 0,
	custom_sent BIGINT NOT NULL DEFAULT 0,
	limitless_bought BIGINT NOT NULL DEFAULT 0,
	limitless_sent BIGINT NOT NULL DEFAULT 0,
	static_bought BIGINT NOT NULL DEFAULT 0,
	static_sent BIGINT NOT NULL DEFAULT 0,
	subscribers BIGINT NOT NULL DEFAULT 0,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (project_id),
	CONSTRAINT fk_projects_plan_id
		FOREIGN KEY (plan_id)
		REFERENCES plans(plan_id)
);


DROP TABLE IF EXISTS project_has_developers CASCADE;
CREATE TABLE project_has_developers (
	developer_id UUID NOT NULL,
	project_id UUID NOT NULL,
	privilege TEXT DEFAULT '',
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (developer_id, project_id),
	CONSTRAINT fk_project_has_developers_to_projects_id
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id),
	CONSTRAINT fk_project_has_developers_to_developers_id
		FOREIGN KEY (developer_id)
		REFERENCES developers(developer_id) 
);


DROP TABLE IF EXISTS networks CASCADE;
CREATE TABLE networks (
	network_id UUID NOT NULL UNIQUE,
	project_id UUID NOT NULL,
	abbrev TEXT DEFAULT '',
	color_primary TEXT NOT NULL,
	color_secondary TEXT NOT NULL,
	description TEXT DEFAULT '',
	is_deleted BOOL DEFAULT false,
	name TEXT DEFAULT '',
	notes TEXT DEFAULT '',
	page1 TEXT DEFAULT '',
	page2 TEXT DEFAULT '',
	route TEXT UNIQUE DEFAULT '',
	route_lower TEXT UNIQUE DEFAULT '',
	thumb_selected TEXT DEFAULT '',
	status TEXT DEFAULT '',
	subscriber_count INT DEFAULT 0,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (network_id),
	CONSTRAINT fk_network_belongs_to_project
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id)
);
CREATE INDEX idx_networks_route_lower ON networks(route_lower);
CREATE INDEX idx_networks_project_id ON networks(project_id);


DROP TABLE IF EXISTS channels CASCADE;
CREATE TABLE channels (
	channel_id UUID NOT NULL UNIQUE,
	network_id UUID NOT NULL,
	project_id UUID NOT NULL,
	abbrev TEXT DEFAULT '',
	color_primary TEXT NOT NULL,
	color_secondary TEXT NOT NULL,
	description TEXT DEFAULT '',
	is_deleted BOOL DEFAULT false,
	name TEXT DEFAULT '',
	notes TEXT DEFAULT '',
	page1 TEXT DEFAULT '',
	route TEXT DEFAULT '',
	route_lower TEXT DEFAULT '',

	static_body TEXT DEFAULT '',
	static_edit_expires BIGINT DEFAULT 0,
	static_thumb_selected TEXT DEFAULT '',
	static_title TEXT DEFAULT '',
	static_url TEXT DEFAULT '',
	thumb_selected TEXT DEFAULT '',
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (channel_id),
	CONSTRAINT fk_channel_belongs_to_network
		FOREIGN KEY (network_id)
		REFERENCES networks(network_id)
);
CREATE INDEX idx_channels_route_lower ON channels(route_lower);
CREATE INDEX idx_channels_network_id ON channels(network_id);
CREATE INDEX idx_channels_project_id ON channels(project_id);
