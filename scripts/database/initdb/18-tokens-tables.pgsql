DROP TABLE IF EXISTS network_tokens CASCADE;
CREATE TABLE network_tokens (
	network_id UUID NOT NULL,
	project_id UUID NOT NULL,
	token TEXT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (token)
);
CREATE INDEX idx_network_tokens_network_id ON network_tokens(network_id);


DROP TABLE IF EXISTS channel_tokens CASCADE;
CREATE TABLE channel_tokens (
	channel_id UUID NOT NULL,
	project_id UUID NOT NULL,
	token TEXT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (token)
);
CREATE INDEX idx_channel_tokens_channel_id ON channel_tokens(channel_id);


DROP TABLE IF EXISTS stat_edit_tokens CASCADE;
CREATE TABLE stat_edit_tokens (
	stat_edit_token_id UUID NOT NULL,
	project_id UUID NOT NULL,
	channel_id UUID,
	status TEXT NOT NULL, -- purchased, consumed
	time_activated BIGINT DEFAULT 0,
	time_expires BIGINT DEFAULT 0,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (stat_edit_token_id)
);
CREATE INDEX idx_stat_edit_tokens_channel_id ON stat_edit_tokens(channel_id);
