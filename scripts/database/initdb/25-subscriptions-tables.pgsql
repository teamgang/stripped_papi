DROP TABLE IF EXISTS network_subscriptions CASCADE;
CREATE TABLE network_subscriptions (
	network_sub_id UUID NOT NULL,
	network_id UUID NOT NULL,
	surfer_or_instance_id TEXT NOT NULL, 
	can_network_manage_this_subscription BOOL DEFAULT FALSE,
	is_silenced BOOL NOT NULL DEFAULT FALSE,
	-- sync_enabled BOOL DEFAULT FALSE, -- sync is enabled until user disables it
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (network_sub_id),
	CONSTRAINT fk_network_subscriptions_network_id
		FOREIGN KEY (network_id)
		REFERENCES networks(network_id)
);
CREATE INDEX idx_network_subscriptions_network_id_plus_surfer_or_instance_id ON 
	network_subscriptions(network_id, surfer_or_instance_id);


DROP TABLE IF EXISTS channel_subscriptions CASCADE;
CREATE TABLE channel_subscriptions (
	network_sub_id UUID NOT NULL,
	channel_id UUID NOT NULL,
	network_id UUID NOT NULL,
	is_silenced BOOL NOT NULL DEFAULT FALSE,
	is_subscribed BOOL NOT NULL DEFAULT FALSE,
	surfer_sound TEXT NOT NULL DEFAULT '',
	use_surfer_sound BOOL NOT NULL DEFAULT FALSE,
	-- sync_enabled BOOL DEFAULT FALSE, -- sync is enabled until user disables it
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (network_sub_id, channel_id),
	CONSTRAINT fk_channel_subscriptions_channel_id
		FOREIGN KEY (channel_id)
		REFERENCES channels(channel_id)
);
