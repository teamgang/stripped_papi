DROP TABLE IF EXISTS bois CASCADE;
CREATE TABLE bois (
	boi_id UUID NOT NULL,
	channel_id UUID NOT NULL,
	network_id UUID NOT NULL,
	project_id UUID NOT NULL,
	body TEXT DEFAULT '' NOT NULL,
	count_failure INT DEFAULT 0 NOT NULL,
	count_success INT DEFAULT 0 NOT NULL,
	count_recipient_instance_ids INT DEFAULT 0 NOT NULL,

	-- cm = count_metric
	cm_created INT DEFAULT 0 NOT NULL,
	cm_dismissed INT DEFAULT 0 NOT NULL,
	cm_displayed INT DEFAULT 0 NOT NULL,
	cm_tapped INT DEFAULT 0 NOT NULL,
	cm_viewed_in_app INT DEFAULT 0 NOT NULL,

	error TEXT DEFAULT '' NOT NULL,
	is_hidden BOOL DEFAULT FALSE,
	layout TEXT NOT NULL,  -- default, bigPicture, bigText
	notes TEXT DEFAULT '' NOT NULL,
	period INT NOT NULL,
	plan_id INT NOT NULL,
	plasticity TEXT NOT NULL,
	status TEXT NOT NULL,
	title TEXT DEFAULT '' NOT NULL,
	thumb_selected TEXT DEFAULT '' NOT NULL,
	token TEXT DEFAULT '' NOT NULL,
	url TEXT DEFAULT '' NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (boi_id),
	CONSTRAINT fk_bois_to_channel_id
		FOREIGN KEY (channel_id)
		REFERENCES channels(channel_id),
	CONSTRAINT fk_bois_to_network_id
		FOREIGN KEY (network_id)
		REFERENCES networks(network_id),
	CONSTRAINT fk_bois_to_project_id
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id)
);


DROP TABLE IF EXISTS boi_recipients CASCADE;
CREATE TABLE boi_recipients (
	boi_recipient_id UUID NOT NULL,
	boi_id UUID,
	instance_id TEXT,
	network_sub_id UUID,

	is_silenced BOOL,
	surfer_sound TEXT,
	use_surfer_sound BOOL,

	-- m = metric
	m_created BOOL DEFAULT FALSE,
	m_dismissed BOOL DEFAULT FALSE,
	m_displayed BOOL DEFAULT FALSE,
	m_tapped BOOL DEFAULT FALSE,
	m_viewed_in_app BOOL DEFAULT FALSE,

	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (boi_recipient_id)
);
CREATE INDEX idx_boi_recipients_boi_id_instance_id 
	ON boi_recipients(boi_id, instance_id);
