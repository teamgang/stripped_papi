DROP TABLE IF EXISTS charges CASCADE;
CREATE TABLE charges (
	charge_id UUID NOT NULL,
	charge_type INT NOT NULL,
	currency TEXT DEFAULT 'USD',
	boi_id UUID,
	period INT NOT NULL,
	plan_id INT,
	price INT NOT NULL,
	project_id UUID NOT NULL,
	units INT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (charge_id),
	CONSTRAINT fk_charges_boi_id
		FOREIGN KEY (boi_id)
		REFERENCES bois(boi_id),
	CONSTRAINT fk_charges_project_id
		FOREIGN KEY (project_id) REFERENCES projects(project_id)
);
CREATE INDEX idx_charges_project_id_and_period ON 
	charges(project_id, period);


-- payment_platform: btcpay, stripe
-- payment_purpose: credit, credit_back, overage, period
DROP TABLE IF EXISTS payments CASCADE;
CREATE TABLE payments (
	payment_id UUID NOT NULL,
	project_id UUID NOT NULL,
	bill_id UUID,
	developer_notes TEXT,
	btcpay_invoice_id TEXT,
	payment_intent_id TEXT,
	payment_platform INT NOT NULL,
	stripe_invoice_id TEXT,
	-- eveything else mirrors stripe_payment_attempts
	admin_notes TEXT,
	basket TEXT NOT NULL,
	credit_applied INT NOT NULL,
	currency TEXT DEFAULT 'USD',
	is_autopay_selected BOOL NOT NULL,
	plastic_id TEXT,
	silo TEXT NOT NULL,
	status INT NOT NULL,
	terms_accepted_version TEXT NOT NULL,
	total_price INT NOT NULL,
	total_price_after_credit INT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (payment_id),
	CONSTRAINT fk_payments_project_id
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id)
);
CREATE INDEX idx_payments_project_id ON 
	payments(project_id);


DROP TABLE IF EXISTS stripe_payment_attempts CASCADE;
CREATE TABLE stripe_payment_attempts (
	attempt_id UUID NOT NULL,
	bill_id UUID,
	project_id UUID NOT NULL,
	payment_intent_id TEXT,
	admin_notes TEXT DEFAULT '',
	basket TEXT NOT NULL,
	credit_applied INT NOT NULL,
	currency TEXT DEFAULT 'USD',
	is_autopay_selected BOOL NOT NULL,
	plan_id INT,
	plastic_id TEXT NOT NULL,
	silo TEXT NOT NULL,
	status INT NOT NULL,
	terms_accepted_version TEXT NOT NULL,
	total_price INT NOT NULL,
	total_price_after_credit INT NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (attempt_id),
	CONSTRAINT fk_stripe_payment_attempts_project_id
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id)
);
CREATE INDEX idx_stripe_payment_attempts_status ON 
	stripe_payment_attempts(status);


DROP TABLE IF EXISTS agreement_accepted_logs CASCADE;
CREATE TABLE agreement_accepted_logs (
	developer_id UUID NOT NULL,
	project_id UUID NOT NULL,
	is_accepted BOOL NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (developer_id, time_created)
);


DROP TABLE IF EXISTS bills CASCADE;
CREATE TABLE bills (
	bill_id UUID NOT NULL,
	admin_notes TEXT,
	bill_type TEXT NOT NULL,
	charge_ids TEXT DEFAULT '[]',
	credit_applied INT DEFAULT 0,
	credit_overflow INT DEFAULT 0,
	currency TEXT DEFAULT 'USD',
	notes TEXT DEFAULT '',
	period INT NOT NULL,
	price INT NOT NULL,
	project_id UUID NOT NULL,
	status TEXT NOT NULL,
	was_autopay_used BOOL DEFAULT FALSE,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0,
	PRIMARY KEY (bill_id),
	CONSTRAINT fk_monthly_bills_project_id
		FOREIGN KEY (project_id)
		REFERENCES projects(project_id)
);
