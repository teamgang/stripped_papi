DROP TABLE IF EXISTS plogs CASCADE;
CREATE TABLE plogs (
	plog_id UUID NOT NULL PRIMARY KEY,
	channel_id UUID,
	developer_id UUID,
	instance_id TEXT,
	network_id UUID,
	project_id UUID,
	surfer_id UUID,

	category TEXT,
	code INT,
	level TEXT,
	meta TEXT,
	msg TEXT,
	platform TEXT,
	user_type TEXT,
	version TEXT,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0
);
