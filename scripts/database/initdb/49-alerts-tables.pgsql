DROP TABLE IF EXISTS emails CASCADE;
CREATE TABLE emails (
	email_id UUID NOT NULL PRIMARY KEY,
	from_ TEXT,
	body TEXT,
	developer_id UUID,
	project_id UUID,
	subject TEXT,
	to_ TEXT,
	was_allow_email_bills_enabled BOOL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0
);


DROP TABLE IF EXISTS alerts CASCADE;
CREATE TABLE alerts (
	alert_id UUID NOT NULL PRIMARY KEY,
	developer_id UUID,
	surfer_id UUID,
	instance_id UUID,
	project_id UUID,
	network_id UUID,
	channel_id UUID,
	msg TEXT,
	subject TEXT,
	was_viewed BOOL NOT NULL,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0
);
