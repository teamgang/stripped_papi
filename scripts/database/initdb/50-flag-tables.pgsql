DROP TABLE IF EXISTS flags CASCADE;
CREATE TABLE flags (
	flag_id UUID NOT NULL PRIMARY KEY,
	surfer_id UUID,
	category TEXT,
	msg TEXT,

	boi_id UUID,
	time_created BIGINT DEFAULT 0,
	time_updated BIGINT DEFAULT 0
);
