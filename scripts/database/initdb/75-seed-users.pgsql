INSERT INTO developers (email, developer_id, password, stripe_customer_id, is_email_verified) VALUES ('2@2.2', '22222222-2222-2222-2222-222222222222', '$2b$12$BbCQQnL5ogrp4ZzUf9Ej0uEKrhTyftcFuttJ7nmKC.sCb9s.o6bDm', 'cus_Kdken4gtPizbB7', true);
INSERT INTO developers (email, developer_id, password, is_email_verified) VALUES ('9@9.9', '99999999-9999-9999-9999-999999999999', '$2b$12$BbCQQnL5ogrp4ZzUf9Ej0uEKrhTyftcFuttJ7nmKC.sCb9s.o6bDm', true);

INSERT INTO surfers (email, surfer_id, password, is_email_verified) VALUES ('2@2.2', '22222222-2222-2222-2222-222222222222', '$2b$12$BbCQQnL5ogrp4ZzUf9Ej0uEKrhTyftcFuttJ7nmKC.sCb9s.o6bDm', true);
INSERT INTO surfers (email, surfer_id, password, is_email_verified) VALUES ('9@9.9', '99999999-9999-9999-9999-999999999999', '$2b$12$BbCQQnL5ogrp4ZzUf9Ej0uEKrhTyftcFuttJ7nmKC.sCb9s.o6bDm', true);
