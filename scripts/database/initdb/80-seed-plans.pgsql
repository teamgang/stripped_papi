-- Free
INSERT INTO plans (
	admin_notes,
	description,
	is_available,
	is_tailored,
	channel_limit,
	subscription_limit,
	bundled_static,
	bundled_custom,
	bundled_limitless,
	title,
	add_static_price,
	add_static_units,
	add_custom_price,
	add_custom_units,
	add_limitless_price,
	add_limitless_units,
	price,
	price_currency,
	static_edit_token_price,
	rules,
	time_created, 
	time_updated) VALUES (
	'Seed for free plan',
	'Experiment with notifications.',
	true,
	false,
	2, -- channel limit
	5, -- subscription limit
	100, -- static bundled
	50, -- custom bundled,
	50, -- limitless bundled,
	'Free',
	0, -- static price
	0, -- static units
	0, -- custom price
	0, -- custom units
	0, -- limitless price
	0, -- limitless units
	0, -- price
	'USD',
	10000,
	'no_notification_purchases;', -- rules
	1635357669,
	1635357669);

-- Growth
INSERT INTO plans (
	admin_notes,
	description,
	is_available,
	is_tailored,
	channel_limit,
	subscription_limit,
	bundled_static,
	bundled_custom,
	bundled_limitless,
	title,
	add_static_price,
	add_static_units,
	add_custom_price,
	add_custom_units,
	add_limitless_price,
	add_limitless_units,
	price,
	price_currency,
	static_edit_token_price,
	rules,
	time_created, 
	time_updated) VALUES (
	'Seed for growth plan',
	'Create complex networks for sizeable projects.',
	true,
	false,
	6, -- channel limit
	0, -- subscription limit
	100000, -- static bundled
	1000, -- custom bundled,
	1000, -- limitless bundled,
	'Growth',
	10, -- static price
	100000, -- static units
	10, -- custom price
	10000, -- custom units
	20, -- limitless price
	10000, -- limitless units
	2000, -- price
	'USD',
	10000,
	'', -- rules
	1635357669,
	1635357669);

-- Outreach
INSERT INTO plans (
	admin_notes,
	description,
	is_available,
	is_tailored,
	channel_limit,
	subscription_limit,
	bundled_static,
	bundled_custom,
	bundled_limitless,
	title,
	add_static_price,
	add_static_units,
	add_custom_price,
	add_custom_units,
	add_limitless_price,
	add_limitless_units,
	price,
	price_currency,
	static_edit_token_price,
	rules,
	time_created, 
	time_updated) VALUES (
	'Seed for outreach plan',
	'Deliver content to a large network.',
	true,
	false,
	0, -- channel limit
	0, -- subscription limit
	600000, -- static bundled
	20000, -- custom bundled,
	20000, -- limitless bundled,
	'Outreach',
	7, -- static price
	100000, -- static units
	7, -- custom price
	10000, -- custom units
	14, -- limitless price
	10000, -- limitless units
	6000, -- price
	'USD',
	10000,
	'',
	1635357669,
	1635357669);

-- Global
INSERT INTO plans (
	admin_notes,
	description,
	is_available,
	is_tailored,
	channel_limit,
	subscription_limit,
	bundled_static,
	bundled_custom,
	bundled_limitless,
	title,
	add_static_price,
	add_static_units,
	add_custom_price,
	add_custom_units,
	add_limitless_price,
	add_limitless_units,
	price,
	price_currency,
	static_edit_token_price,
	rules,
	time_created, 
	time_updated) VALUES (
	'Seed for global plan',
	'Speak to the world.',
	true,
	false,
	0, -- channel limit
	0, -- subscription limit
	8000000, -- static bundled
	100000, -- custom bundled,
	100000, -- limitless bundled,
	'Global',
	4, -- static price
	100000, -- static units
	4, -- custom price
	10000, -- custom units
	8, -- limitless price
	10000, -- limitless units
	50000, -- price
	'USD',
	10000,
	'', -- rules
	1635357669,
	1635357669);

-- pay as you go
INSERT INTO plans (  -- id=5
	admin_notes,
	description,
	is_available,
	is_tailored,
	channel_limit,
	subscription_limit,
	bundled_static,
	bundled_custom,
	bundled_limitless,
	title,
	add_static_price,
	add_static_units,
	add_custom_price,
	add_custom_units,
	add_limitless_price,
	add_limitless_units,
	monthly_free_custom,
	monthly_free_limitless,
	monthly_free_static,
	monthly_free_static_edit_tokens,
	price,
	price_currency,
	static_edit_token_price,
	rules,
	time_created, 
	time_updated) VALUES (
	'Pay as you go',
	'Speak to the world.',
	true,
	false,
	0, -- channel limit
	0, -- subscription limit
	8000000, -- static bundled
	100000, -- custom bundled,
	100000, -- limitless bundled,
	'Pay as You Go',
	1000, -- static price
	100000, -- static units
	1000, -- custom price
	10000, -- custom units
	2000, -- limitless price
	10000, -- limitless units
	3, -- monthly_free_custom # 50
	3, -- monthly_free_limitless # 50
	3, -- monthly_free_static # 1000
	1, -- monthly_free_edit_tokens
	0, -- price
	'USD',
	10000,
	'', -- rules
	1635357669,
	1635357669);
