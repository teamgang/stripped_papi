INSERT INTO projects (
	project_id, 
	creator_id, 
	first_period,
	is_closed, 
	name,
	notes, 
	plan_id,
	plan_id_next,
	limitless_bought,
	time_created, 
	time_updated) VALUES (
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'22222222-2222-2222-2222-222222222222',
	202201,
	FALSE,
	'BigGuy',
	'Bigguy is a cool site.',
	5, -- plan_id
	5, -- plan_id_next
	0,
	1641413370,
	1641413370);

INSERT INTO projects (
	project_id, 
	creator_id, 
	first_period,
	is_closed, 
	name,
	notes, 
	plan_id,
	plan_id_next,
	limitless_bought,
	time_created, 
	time_updated) VALUES (
	'aaaaaaaa-bbbb-4fbe-bf2e-ca7d334d0d50',
	'22222222-2222-2222-2222-222222222222',
	202201,
	FALSE,
	'Podcasting',
	'Bigguy is a cool site.',
	5, -- plan_id
	5, -- plan_id_next
	0,
	1595657212,
	1595657212);

INSERT INTO project_has_developers (
	developer_id, 
	project_id, 
	privilege, 
	time_created, 
	time_updated) VALUES (
	'22222222-2222-2222-2222-222222222222',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'creator',
	1635357669,
	1635357669);

INSERT INTO project_has_developers (
	developer_id, 
	project_id, 
	privilege, 
	time_created, 
	time_updated) VALUES (
	'22222222-2222-2222-2222-222222222222',
	'aaaaaaaa-bbbb-4fbe-bf2e-ca7d334d0d50',
	'creator',
	1595657212,
	1595657212);

INSERT INTO networks (
	network_id, 
	abbrev,
	color_primary,
	color_secondary,
	description,
	name,
	notes,
	project_id, 
	route, 
	route_lower, 
	thumb_selected,
	time_created, 
	time_updated) VALUES (
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Th',
	'255,0,0',
	'128,255,0',
	'Here is a description for the network. It is really long so that we can makre sure overflow is working correctly. Wow, we need a lot of text for this test', -- description
	'The biggest network on the face of the planet. You should really think about subscribing',
	'Notes',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'big', -- route
	'big', -- route_lower
	'abbrev',
	1635357669,
	1635357669);


INSERT INTO network_tokens (
	network_id,
	project_id,
	token,
	time_created,
	time_updated) VALUES (
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'net_WTJw8pNeAHY8zbmy',
	1636499080,
	1636499080);


INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-1111-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SV',
	'255,0,0',
	'128,255,0',
	'News, world update.',  -- static_body
	'abbrev',
	'Simba',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba goes live.', -- description
	'Simba News Livestreams', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every weekday at 6 PM EST.', -- notes
	'userGoesLive', -- route
	'usergoeslive', -- route_lower
	1635357669,
	1635357669);

INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-2222-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SG',
	'255,0,0',
	'128,255,0',
	'Games that made me cry as a cub',  -- static_body
	'abbrev',
	'Simba Gaming',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba games', -- description
	'Simba Gaming', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every Tu,Th at 8 PM EST.', -- notes
	'simbaGamingLive', -- route
	'simbagaminglive', -- route_lower
	1635357669,
	1635357669);

INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-3333-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SG',
	'255,0,0',
	'128,255,0',
	'Games that made me cry as a cub',  -- static_body
	'abbrev',
	'Weather',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba games', -- description
	'Simba Gaming', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every Tu,Th at 8 PM EST.', -- notes
	'simbaGamingLive', -- route
	'simbagaminglive', -- route_lower
	1635357669,
	1635357669);

INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-4444-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SG',
	'255,0,0',
	'128,255,0',
	'Games that made me cry as a cub',  -- static_body
	'abbrev',
	'Real News',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba games', -- description
	'Simba Gaming', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every Tu,Th at 8 PM EST.', -- notes
	'simbaGamingLive', -- route
	'simbagaminglive', -- route_lower
	1635357669,
	1635357669);

INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-5555-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SG',
	'255,0,0',
	'128,255,0',
	'Games that made me cry as a cub',  -- static_body
	'chan',
	'Real News',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba games', -- description
	'Simba Gaming', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every Tu,Th at 8 PM EST.', -- notes
	'simbaGamingLive', -- route
	'simbagaminglive', -- route_lower
	1635357669,
	1635357669);

INSERT INTO channels (
	channel_id,
	project_id,
	abbrev,
	color_primary,
	color_secondary,
	static_body,
	static_thumb_selected,
	static_title,
	static_url,
	description,
	name,
	network_id,
	notes,
	route, 
	route_lower, 
	time_created,
	time_updated) VALUES (
	'cccccccc-6666-4fbe-bf2e-ca7d334d0d50',
	'aaaaaaaa-088e-4fbe-bf2e-ca7d334d0d50',
	'SG',
	'255,0,0',
	'128,255,0',
	'Games that made me cry as a cub',  -- static_body
	'abbrev',
	'Channel 6',  -- static_title
	'https://yahoo.com',  -- static_url
	'Get notified when Simba games', -- description
	'Simba Gaming', -- name
	'bbbbbbbb-088e-4fbe-bf2e-ca7d334d0d50',
	'Our API should trigger this channel event every Tu,Th at 8 PM EST.', -- notes
	'simbaGamingLive', -- route
	'simbagaminglive', -- route_lower
	1635357669,
	1635357669);

