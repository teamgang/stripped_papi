sudo mkdir -p /papi/hosted/.well-known
sudo mkdir -p /papi/hosted/img/{boi,chan,chanabbrev,chanstatic,net,netabbrev,stat,thumb}
sudo mkdir -p /papi/{certs,rabbit,tmp,web}
sudo chown -R ubuntu:ubuntu /papi
sudo apt install -y git nginx python3 python3-pip npm neovim opendkim
sudo apt install -y python3-pil libwebp-dev  # image libs
sudo apt install -y libpq-dev  # psycopg2

npm config set prefix '~/.npm-global'
npm install -g yarn

pip install virtualenvwrapper --user

# install certbot
# https://eff-certbot.readthedocs.io/en/stable/install.html
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot

sudo certbot --nginx -d www.pushboi.io,pushboi.io --email team@pushboi.io
sudo cd /etc/letsencrypt/live/www.pushboi.io
sudo openssl dhparam -out /etc/nginx/dhparam.pem 2048

update crontab to automatically renew certificates if needed every day
sudo bash -c 'echo "$((RANDOM%60)) $((RANDOM%24)) * * * root certbot -q renew" >> /etc/crontab'


# docker
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

 sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin


# mail
#sudo apt install mailutils
	#- select Internet Site
	#- System mail name is FQDN (pushboi.io)
	#- to relaunch configuration, execute
	#$ sudo dpkg-reconfigure postfix

#cat /etc/letsencrypt/live/www.pushboi.io/cert.pem | sudo tee -a /etc/postfix/cacert.pem


## some instructions from https://www.linuxbabe.com/linux-server/create-dns-records-in-namecheap

#in namecheap add MX Record:
#@ pushboi.io. 200
#change SPF record to:
#v=spf1 include:zoho.eu include:pushboi.io ~all

#PTR record:
#in lunanode, set rDNS to pushboi.io.

#DKIM:
#add postfix to opendkim group
#https://wiki.archlinux.org/title/OpenDKIM
#opendkim-genkey -r -s papi -d pushboi.io

#Domain      pushboi.io
#Selector    papi
#KeyFile   /etc/dkimkeys/papi.private

#set the correct socket path:
#https://unix.stackexchange.com/questions/74477/postfix-smtpd-warning-connect-to-milter-service-unix-var-run-opendkim-opendki/74491#74491


#sudo apt install opendmarc
#password: this is a really long password for the t34m

#service opendkim restart
#service postfix restart
