#!/bin/sh
sudo mkdir -p /papi/img
sudo mkdir -p /papi/tmp

imageName=$(cat IMAGENAME)
containerName=$(cat CONTAINERNAME)
sudo docker rm $containerName
sudo docker build \
	-t $imageName \
	-f ./dev.dockerfile ./

sudo docker run \
	--name $containerName \
	--mount type=bind,source=/papi,target=/papi \
	-p 80:80 \
	$imageName
