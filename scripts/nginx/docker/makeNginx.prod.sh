#!/bin/sh
imageName=$(cat IMAGENAME)
containerName=$(cat CONTAINERNAME)
sudo docker rm $containerName
sudo docker build \
	-t $imageName \
	-f ./prod.dockerfile ./

sudo docker run \
	--name $containerName \
	$imageName
