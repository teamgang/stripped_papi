FROM python:3.10-alpine3.15
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN apk add openssl-dev libffi-dev
COPY ./requirements.txt .
RUN pip install virtualenv
RUN virtualenv venv
RUN source venv/bin/activate
RUN pip install -r requirements.txt
#ENV POSTGRES_USER postgres
#ENV POSTGRES_PASSWORD postgres
#ENV POSTGRES_DB papi
#ADD 00-db-init.sql /docker-entrypoint-initdb.d/
#ADD 11-db-characters.pgsql /docker-entrypoint-initdb.d/
EXPOSE 5432
