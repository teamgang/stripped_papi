#!/bin/sh
imageName=$(cat IMAGENAME)
containerName=$(cat CONTAINERNAME)
sudo docker rm $containerName

sudo docker build \
	-t $imageName \
	-f ./rabbitmq.dockerfile ./

sudo docker run \
	--mount type=bind,source=/papi/rabbit,target=/var/lib/rabbitmq \
	--mount type=bind,source=/papi/certs,target=/papi/certs \
	--name $containerName \
	--hostname papi-rabbit \
	-p 5671:5671 \
	-p 5672:5672 \
	-p 15672:15672 \
	-e RABBITMQ_DEFAULT_USER=papi \
	-e RABBITMQ_DEFAULT_PASS=1234 \
	$imageName
