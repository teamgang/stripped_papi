FROM rabbitmq:3.9-management-alpine
COPY 20-connection.conf /etc/rabbitmq/conf.d/
RUN mkdir -p /papi/certs
