"""Define project metadata
"""

__package_name__ = 'papi'
__description__ = 'Pushing for everyone'
__url__ = 'https://unipush.com'
__version__ = '0.1.0'
__author__ = 'Taylor Gronka'
__email__ = 'mr.gronka@gmail.com'
__company__ = 'Unipush'
__license__ = 'GNU 2.0'
