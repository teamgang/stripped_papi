import firebase_admin
import os
from passlib.context import CryptContext
from sqlalchemy.engine.url import URL
import toml
from typing import List

DB_CHOICE = os.getenv('DB_CHOICE')
PAPI_ENV = os.getenv('PAPI_ENV')
PAPI_ROOT = os.getenv('PAPI_ROOT')

if not DB_CHOICE:
    raise RuntimeError('DB_CHOICE must be "postgresql" or "sqlite"')

if not PAPI_ENV:
    raise RuntimeError('PAPI_ENV must be "dev" or "local" or "prod"')

if not PAPI_ROOT:
    raise RuntimeError('PAPI_ROOT dir must be set')


class LegalFile:
    def __init__(self, filename: str, legal_dir: str):
        file_root = ''.join(filename.split('.')[:-1])
        fields = file_root.split('+')
        self.audience: str = fields[0]
        self.policy: str = fields[1]
        self.dated: str = fields[2]
        self.filename = filename
        self.fullpath = f'{legal_dir}/{self.filename}'
        if self.audience != 'init':
            with open(self.fullpath, 'r')  as lf:
                self.fulltext = lf.read()

    @property
    def as_json(self):
        return {
            'audience': self.audience,
            'policy': self.policy,
            'dated': self.dated,
            'fulltext': self.fulltext,
        }

def get_latest_legal(audience: str, policy: str, legal_dir: str):
    legal_files = os.listdir(legal_dir)
    latest = LegalFile('init+a+0000-00-00.md', legal_dir)
    for filename in legal_files:
        lf = LegalFile(filename, legal_dir)
        if lf.audience == audience and lf.policy == policy:
            if lf.dated > latest.dated:
                latest = lf
    return latest


class LogLevel:
    VERBOSE = 10
    DEBUG = 20
    INFO = 30
    WARNING = 40
    ERROR = 50
    WTF = 60


class Conf:
    base_url: str = ''
    image_url: str = ''
    hosted_path: str = '/papi/hosted'
    jwt_algorithm: str = 'HS256'
    jwt_algorithms: List[str] = ['HS256']
    jwt_lifetime_minutes: int = 15
    jwt_session_lifetime_days: int = 365
    log_level: int = LogLevel.VERBOSE
    papi_env: str = PAPI_ENV
    papi_root: str = PAPI_ROOT

    rab_exchange = ''
    rab_host = 'localhost'
    rab_user = 'papi'
    rab_password = '1234'

    smtp_host = ''
    smtp_user = ''
    email_errors = ''
    email_noreply = ''

    pwds= CryptContext(schemes=['bcrypt'], deprecated='auto')

    def __init__(self):
        with open('/papi/certs/creds.toml', 'r')  as lf:
            pat = toml.loads(lf.read())
            self.smtp_host = pat['smtp']['host']
            self.smtp_pass = pat['smtp']['pass']
            self.smtp_user = pat['smtp']['user']
            from_domain = pat['smtp']['from_domain']
            self.email_errors = f'PushBoi <errors@{from_domain}>'
            self.email_noreply = f'PushBoi <no-reply@{from_domain}>'

            #TODO: use starletto secrets to make this more secrete
            self.postgres_password = pat['database']['password']
            self.jwt_secret_key = pat['jwt']['secret']
            self.jwt_session_secret_key = pat['jwt']['session_secret']
            # get password key with 'openssl rand -hex 32'
            self.password_key = pat['auth']['password_key']

            self.google_client_id = pat['auth']['google_client_id']
            self.google_client_secret = pat['auth']['google_client_secret']
            self.google_auth_token_key = pat['auth']['google_auth_token_key']
            self.google_auth_state_key = pat['auth']['google_auth_state_key']

            self.apple_team_id = pat['apple']['team_id']
            # apple_service_id = 'io.pusbhoi.ios'  # for web flows
            self.apple_bundle_id = pat['apple']['bundle_id']  # for apps
            self.apple_sign_in_key_id = pat['apple']['sign_in_key_id']
            self.apple_sign_in_key_contents = pat['apple']['sign_in_key_contents']
            self.apple_package_iden = pat['apple']['package_iden']

            self.stripe_api_key = pat['stripe']['api_key']


        self.project_name: str = 'Unipush'
        self.api_name: str = 'Unipush API'
        if self.is_dev():
            self.base_url = 'http://localhost/'
            self.image_url = 'http://localhost/'
        if self.is_local():
            self.base_url = 'http://localhost/'
            self.image_url = 'http://localhost/'
        if self.is_prod():
            self.base_url = 'https://pushboi.io/'
            self.image_url = self.base_url

        self.postgresql_dsn = URL.create(
            drivername='postgresql',
            username='postgres',
            password=self.postgres_password,
            host='localhost',
            port=None,
            database='papi',
        )

        self.sqlite_dsn = 'sqlite:///./sql_app.db'

        with open('/papi/certs/AuthKey_4T7B7L3666_signInWithApple.p8', 'r') \
                as fil:
            self.apple_sign_in_p8 = fil.read()

        self.legal_dir: str = f'{self.papi_root}/src/papi/legal'
        self.read_developer_privacy()
        self.read_developer_terms()
        self.read_surfer_privacy()

        self.google_auth_redirect_uri = f'{self.base_url}v1/google/auth'
        self.google_auth_base_uri = f'{self.base_url}'

        if not self.developer_privacy or \
                not self.developer_terms or \
                not self.surfer_privacy:
            raise RuntimeError('Failed to read latest policy')

        #NOTE: error that JWT must be short-lived means clock is desynced
        cred = firebase_admin.credentials.Certificate('/papi/certs/firebase-cred.json')
        firebase_admin.initialize_app(cred)

    def read_developer_privacy(self):
        self.developer_privacy = get_latest_legal('developer',
                                                  'privacy',
                                                  self.legal_dir)

    def read_developer_terms(self):
        self.developer_terms = get_latest_legal('developer',
                                                'terms',
                                                self.legal_dir)
    def read_surfer_privacy(self):
        self.surfer_privacy = get_latest_legal('surfer',
                                               'privacy',
                                               self.legal_dir)

    def is_dev(self):
        if PAPI_ENV == 'dev':
            return True
        return False

    def is_local(self):
        if PAPI_ENV == 'local':
            return True
        return False

    def is_prod(self):
        if PAPI_ENV == 'prod':
            return True
        return False

    def is_using_postgresql(self):
        if DB_CHOICE == 'postgresql':
            return True
        return False

    def is_using_sqlite(self):
        if DB_CHOICE == 'sqlite':
            return True
        return False

    def make_url(self, path: str):
        return f'{self.base_url}{path}'

    def make_image_url(self, path: str):
        return f'{self.image_url}{path}'


conf = Conf()

print('Configuration loaded')
print(f'PAPI_ENV: {conf.papi_env}')
print(f'PAPI_ROOT: {conf.papi_root}')
print(f'base url: {conf.base_url}')
