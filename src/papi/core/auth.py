from datetime import datetime, timedelta, timezone
from jose import jwt
from typing import Optional, Union
import uuid

from papi.conf import conf
from papi.core.dates import nowstamp

Puid = Union[str, uuid.UUID]  # redefined due to collisions

def create_jwt(to_encode: dict, expires_delta: Optional[timedelta] = None):
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + \
            timedelta(minutes=conf.jwt_lifetime_minutes)
    to_encode.update({'exp': expire.timestamp()})
    return jwt.encode(to_encode,
                      conf.jwt_secret_key,
                      algorithm=conf.jwt_algorithm)


def create_session_jwt(to_encode: dict):
    expire = datetime.now(timezone.utc) + \
        timedelta(days=conf.jwt_session_lifetime_days)
    to_encode.update({'exp': expire.timestamp()})
    return jwt.encode(to_encode,
                      conf.jwt_session_secret_key,
                      algorithm=conf.jwt_algorithm)


def decode_session_jwt(session_jwt: str):
    return jwt.decode(session_jwt,
                      conf.jwt_session_secret_key,
                      algorithms=conf.jwt_algorithms)


def is_jwt_expired(exp: int) -> bool:
    return nowstamp() > exp


def create_developer_jwt(developer_id: Puid) -> str:
    to_encode = { 'developer_id': str(developer_id) }
    return create_jwt(to_encode, expires_delta=timedelta(days=1))


def create_surfer_jwt(surfer_id: Puid) -> str:
    to_encode = { 'surfer_id': str(surfer_id) }
    return create_jwt(to_encode)


def create_surfer_session_jwt(surfer_id: Puid, instance_id: str) -> str:
    to_encode = {
        'surfer_id': str(surfer_id),
        'instance_id': instance_id,
    }
    return create_session_jwt(to_encode)
