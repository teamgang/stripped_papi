from random import randint

PRIMARY_COLORS = [
    '0,0,0',
    '220,20,60',
    '255,0,0',
    '255,99,71',
    '0,0,255',
    '192,192,192',
    '128,128,128',
    '0,128,0',
    '255,215,0',
    '255,140,0',
    '255,165,0',
    '34,139,34',
    '102,205,170',
    '0,191,255',
    '65,105,225',
    '138,43,226',
    '139,0,139',
    '210,105,30',
    '205,133,63',
    '255,20,147',
    '148,0,211',
]

SECONDARY_COLORS = [
    '176,196,222',
    '255,250,240',
    '255,228,181',
    '255,228,196',
    '255,192,203',
    '255,0,255',
    '175,238,238',
    '0,255,255',
    '127,255,0',
    '255,160,122',
    '255,215,0',
]


def get_random_primary_color() -> str:
    pick = randint(0, len(PRIMARY_COLORS))
    return PRIMARY_COLORS[pick]


def get_random_secondary_color() -> str:
    pick = randint(0, len(SECONDARY_COLORS))
    return SECONDARY_COLORS[pick]
