from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
from typing import Iterator

from papi.conf import conf
from .generics import DatabaseError
from .plog import plog


if conf.is_using_postgresql():
    engine = create_engine(conf.postgresql_dsn)
elif conf.is_using_sqlite():
    engine = create_engine(conf.sqlite_dsn,
                           connect_args={'check_same_thread': False})
else:
    raise RuntimeError()

#NOTE: all arguments below come from FastApi docs at
# https://fastapi.tiangolo.com/tutorial/sql-databases/#create-the-database-models
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db() -> Iterator[Session]:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def db_commit(db: Session, response_builder):
    try:
        db.commit()
    except Exception as exc:
        plog.exception('db_commit error', exc)
        response_builder.add_error(DatabaseError)
        raise exc
