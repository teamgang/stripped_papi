from papi.core import ApiError


LoginToChangeEmailError = ApiError(
    code=3000,
    msg='You must be logged in to change your email.')

TokenExpiredError = ApiError(
    code=3001,
    msg='Token expired.')

TokenAlreadyAppliedError = ApiError(
    code=3002,
    msg='Token already applied.')

##
# project errors
##

NameTooShortError = ApiError(
    code=4520,
    msg='Name is too short.')

NameTooLongError = ApiError(
    code=4521,
    msg='Name is too long.')

NotesTooLongError = ApiError(
    code=4523,
    msg='Notes is too long.')

NetworkRouteTakenError = ApiError(
    code=4500,
    msg='Network route is taken.')

ChannelRouteTakenError = ApiError(
    code=4501,
    msg='Channel route is taken.')

RouteTooShortError = ApiError(
    code=4510,
    msg='Route is too short.')

RouteTooLongError = ApiError(
    code=4511,
    msg='Route is too long.')

RouteNotAlnumError = ApiError(
    code=4512,
    msg='Route is not alphanumeric.')

def makeNameBadWordError(word: str):
    return ApiError(
        code=4513,
        msg=f'Name contains the restriced word {word}.')

def makeRouteBadWordError(word: str):
    return ApiError(
        code=4514,
        msg=f'Route contains the restriced word {word}.')

##
# account errors
##

EmailRegisteredError = ApiError(
    code=1001,
    msg='Email is already registered.')

LoginFailedError = ApiError(
    code=1002,
    msg='Login failed.')

AccountNotFoundError = ApiError(
    code=1003,
    msg='Account not found.')

EmailNotVerifiedError = ApiError(
    code=1004,
    msg=('This e-mail address is not verified. Please check your e-mail and '
         'click the link to verify'))

FailedToValidateWithGoogleError = ApiError(
    code=1005,
    msg='Failed to validate your account with Google.')

FailedToValidateWithAppleError = ApiError(
    code=1006,
    msg='Failed to validate your account with Apple.')

##
# boi errors
##
ImageOver300KbError = ApiError(
    code=5006,
    msg='Image must be less than 300 KB for iOS notifications.')
