import base64
from io import BytesIO
from PIL import Image, ImageOps
import subprocess
import os


def delete_file(filepath: str):
    if os.path.exists(filepath):
        # os.remove does not work with no errors. why?
        subprocess.run(['rm', filepath])


def save_thumb_b64(filepath: str, image_b64: str):
    img_bytes = base64.b64decode(image_b64)
    img = Image.open(BytesIO(img_bytes))
    img2 = resize_with_padding(img, (300, 300))
    img2 = img2.convert('RGB')
    img2.save(filepath)
    img.close()


def padding(img, expected_size):
    desired_size = expected_size
    delta_width = desired_size - img.size[0]
    delta_height = desired_size - img.size[1]
    pad_width = delta_width // 2
    pad_height = delta_height // 2
    padding = (pad_width, pad_height, delta_width - pad_width, delta_height - pad_height)
    return ImageOps.expand(img, padding)


def resize_with_padding(img, expected_size):
    img.thumbnail((expected_size[0], expected_size[1]))
    # print(img.size)
    delta_width = expected_size[0] - img.size[0]
    delta_height = expected_size[1] - img.size[1]
    pad_width = delta_width // 2
    pad_height = delta_height // 2
    padding = (pad_width, pad_height, delta_width - pad_width, delta_height - pad_height)
    return ImageOps.expand(img, padding)
