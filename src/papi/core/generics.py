from pydantic import BaseModel
from typing import List, Optional, Union
import uuid


Puid = Union[str, uuid.UUID]


ZEROS_UUID_STR = '00000000-0000-0000-0000-000000000000'


class ApiSchema(BaseModel): pass


class EmptySchema(ApiSchema): pass


class StatusSchema(ApiSchema):
    status: str
    status_code: int


class CollectionSchema(ApiSchema):
    collection: List[Optional[str]]


class ApiError:
    def __init__(self, code: int, msg: str):
        self._code = code
        self._msg = msg

    def append_to_msg(self, more: str):
        self._msg = f'{self._msg} {more}'

    def to_dict(self):
        return {'code': self._code, 'msg': self._msg}

    def copy(self):
        return ApiError(self._code, self._msg)


DatabaseError = ApiError(
    code=1999,
    msg='Error writing to database.')


class AlertIdIn(ApiSchema):
    alert_id: Puid

class BillIdIn(ApiSchema):
    bill_id: Puid

class BoiIdIn(ApiSchema):
    boi_id: Puid

class ChannelIdIn(ApiSchema):
    channel_id: Puid

class ChargeIdIn(ApiSchema):
    charge_id: Puid

class DeveloperIdIn(ApiSchema):
    developer_id: Puid

class EmailIn(ApiSchema):
    email: str

class InstanceIdIn(ApiSchema):
    instance_id: str

class JwtRefreshOut(ApiSchema):
    new_jwt: str

class NetworkIdIn(ApiSchema):
    network_id: Puid

class PaymentIdIn(ApiSchema):
    payment_id: Puid

class PlanIdIn(ApiSchema):
    plan_id: int

class ProjectIdIn(ApiSchema):
    project_id: Puid

class SessionJwtIn(ApiSchema):
    session_jwt: str

class SurferIdIn(ApiSchema):
    surfer_id: Puid

class TokenIn(ApiSchema):
    token: str

class RequestChangeSchema(ApiSchema):
    developer_id: Puid = ''
    surfer_id: Puid = ''
    is_consumed: bool
    time_expires: int
