from fastapi import Request
from jose import exceptions, jwt
from typing import Mapping

from papi.conf import conf
from papi.core.exceptions import jwt_expired_exception
# from papi.core import ZEROS_UUID_STR


class Gibs:
    def __init__(self, request: Request, skip_processing = False):
        self.bearer_payload: Mapping = {}
        self.bearer_token: str = ''
        self.developer_id: str = ''
        self.has_bearer: bool = False
        self.surfer_id: str = ''
        self.is_jwt_valid: bool = False

        if not skip_processing:
            self._process_jwt(request)

    def _process_jwt(self, request: Request) -> None:
        jwt_token = request.headers.get('JWT')
        if jwt_token:
            self.bearer_token = jwt_token
            self.has_bearer = True
        else:
            return

        try:
            self.bearer_payload = jwt.decode(
                self.bearer_token,
                conf.jwt_secret_key,
                algorithms=conf.jwt_algorithms)
        except exceptions.ExpiredSignatureError:
            raise jwt_expired_exception

        self.developer_id = str(self.bearer_payload.get('developer_id', ''))
        self.surfer_id = str(self.bearer_payload.get('surfer_id', ''))

        if self.surfer_id != '' or self.developer_id != '':
            self.is_jwt_valid = True

    def is_signed_in(self) -> bool:
        if self.todd_id():
            return True
        else:
            return False

    def todd_id(self) -> str:
        if self.developer_id:
            return self.developer_id
        elif self.surfer_id:
            return self.surfer_id
        else:
            return ''
