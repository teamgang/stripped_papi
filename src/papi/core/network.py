from urllib import request as ulreq


def get_image_size(url: str):
    return get_content_length(url)


def get_content_length(url: str):
    req = ulreq.urlopen(url)
    size = req.headers.get("content-length")
    try:
        size = int(size)
    except Exception:
        return -1
    return size
