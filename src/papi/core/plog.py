from email.message import EmailMessage
from smtplib import SMTP, SMTPException
import ssl
import traceback

from papi.conf import conf, LogLevel


MAIL_CONTEXT = ssl.create_default_context()


class PapiLoggerMailer:
    def __init__(self):
        try:
            self.mailcon = SMTP(conf.smtp_host, 587)
            self.mailcon.starttls(context=MAIL_CONTEXT)
            self.mailcon.login(conf.smtp_user, conf.smtp_pass)
        except ConnectionRefusedError:
            plog.debug(f'failed to connect to smtp at {conf.smtp_host}')

    def send_email(self, subject: str, body: str):
        msg = EmailMessage()
        msg['From'] = conf.email_errors
        msg['To'] = conf.email_errors
        msg['Subject'] = subject
        # msg['Importance'] = 'high'
        msg.set_content(body)

        if self.mailcon:
            try:
                self.mailcon.send_message(msg)
            except SMTPException:
                plog.exception(f'unable to send {subject}')
        else:
            plog.debug(f'could not send {subject}')


class PapiLogger:
    def exception(self, msg, err: Exception | None = None):
        msg = f'PAPI_EXCEPTION: {msg}'
        if err:
            msg = f'{msg}\n{traceback.format_exc()}'
            mailer = PapiLoggerMailer()
            mailer.send_email('PAPI_EXCEPTION', msg)

    def v(self, msg):
        if LogLevel.VERBOSE >= conf.log_level:
            print(f'PAPI_VERBOSE: {msg}')

    def verbose(self, msg):
        self.v(msg)

    def d(self, msg):
        if LogLevel.DEBUG >= conf.log_level:
            print(f'PAPI_DEBUG: {msg}')

    def debug(self, msg):
        self.d(msg)

    def i(self, msg):
        if LogLevel.INFO >= conf.log_level:
            print(f'PAPI_INFO: {msg}')

    def info(self, msg):
        self.i(msg)

    def w(self, msg):
        if LogLevel.WARNING >= conf.log_level:
            print(f'PAPI_WARNING: {msg}')

    def warning(self, msg):
        self.w(msg)

    def e(self, msg):
        msg = f'PAPI ERROR: {msg}'
        if LogLevel.ERROR >= conf.log_level:
            mailer = PapiLoggerMailer()
            mailer.send_email('PAPI_ERROR', msg)

    def error(self, msg):
        self.e(msg)

    def wtf(self, msg):
        msg = f'PAPI WTF: {msg}'
        if LogLevel.WTF >= conf.log_level:
            mailer = PapiLoggerMailer()
            mailer.send_email('PAPI_WTF', msg)

    def critical(self, msg):
        self.wtf(msg)


plog = PapiLogger()
