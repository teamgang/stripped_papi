from sqlalchemy.orm import Session
from typing import List

from papi.core import Gibs, Puid
from papi.models import (
    Alert,
    Channel,
    Developer,
    Network,
    NetworkSub,
    ProjectHasDeveloper,
    Surfer,
)


class PolicyResult:
    '''Records whether a policy allowed user access or revoked it.

    (A long term goal of this class might be to roll up and unroll the results
    of all applied policies, but we don't have a need for that yet.)
    '''
    def __init__(self, allowed: bool):
        self.allowed: bool = allowed


class PolicyChain:
    '''The PolicyChain class houses static methods that return PolicyResult
    objects. We are able to apply complex Policy inquiries by applying
    combinations of PoliyChain methods and examining the returned PolicyResult.
    '''
    @classmethod
    def at_least_one_policy_must_allow(
        cls,
        policy_results: List[PolicyResult],
    ) -> PolicyResult:
        for policy_result in policy_results:
            if policy_result.allowed:
                return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    def all_policies_must_allow(
        cls,
        policy_results: List[PolicyResult],
    ) -> PolicyResult:
        for policy_result in policy_results:
            if not policy_result.allowed:
                return PolicyResult(allowed=False)
        return PolicyResult(allowed=True)


class Policies:
    '''The Policies class houses static methods that return PolicyResult
    objects.
    '''
    @classmethod
    async def public(cls) -> PolicyResult:
        return PolicyResult(allowed=True)

    @classmethod
    async def no_one(cls) -> PolicyResult:
        return PolicyResult(allowed=False)

    @classmethod
    async def is_user_super_admin(cls, todd_id: str) -> PolicyResult:
        admins = ['9@9.9']
        if todd_id in admins:
            return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def is_user_signed_in(cls, gibs: Gibs) -> PolicyResult:
        if gibs.is_signed_in():
            return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def is_user_a_developer(cls, gibs: Gibs) -> PolicyResult:
        if gibs.developer_id:
            return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def is_user_this_developer(
            cls,
            gibs: Gibs,
            developer: Developer) -> PolicyResult:
        if gibs.developer_id == developer.developer_id:
            return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def is_user_this_surfer(
            cls,
            gibs: Gibs,
            surfer: Surfer) -> PolicyResult:
        if gibs.surfer_id == surfer.surfer_id:
            return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def user_owns_alert(
            cls,
            gibs: Gibs,
            alert: Alert) -> PolicyResult:
        if gibs.developer_id == alert.developer_id:
            return PolicyResult(allowed=True)
        elif gibs.surfer_id == alert.surfer_id:
            return PolicyResult(allowed=True)
        else:
            return PolicyResult(allowed=False)

    @classmethod
    async def user_admins_project_id(
            cls,
            gibs: Gibs,
            project_id: Puid,
            db: Session) -> PolicyResult:
        developers = db.query(ProjectHasDeveloper).filter(
            ProjectHasDeveloper.project_id == project_id).all()  # type: ignore

        for developer in developers:
            if developer.developer_id == gibs.developer_id:
                return PolicyResult(allowed=True)
        return PolicyResult(allowed=False)

    @classmethod
    async def user_admins_channel_id(
            cls,
            gibs: Gibs,
            channel_id: Puid,
            db: Session) -> PolicyResult:
        channel: Channel = db.query(Channel).filter(
            Channel.channel_id == channel_id).first()  # type: ignore

        return await cls.user_admins_project_id(gibs, channel.project_id, db)

    @classmethod
    async def user_admins_network_id(
            cls,
            gibs: Gibs,
            network_id: Puid,
            db: Session) -> PolicyResult:
        network: Network = db.query(Network).filter(
            Network.network_id == network_id).first()  # type: ignore

        return await cls.user_admins_project_id(gibs, network.project_id, db)

    @classmethod
    async def surfer_owns_network_sub(
            cls,
            gibs: Gibs,
            network_sub: NetworkSub) -> PolicyResult:
        if gibs.surfer_id == network_sub.surfer_or_instance_id:
            return PolicyResult(allowed=True)
        else:
            return PolicyResult(allowed=False)


class PolicyRunner:
    @classmethod
    async def anon_or_surfer_owns_network_sub_id(
            cls,
            gibs: Gibs,
            network_sub_id: Puid,
            db: Session,
            rb) -> NetworkSub:

        network_sub: NetworkSub = db.query(NetworkSub).filter( #type:ignore
            NetworkSub.network_sub_id == network_sub_id).first()

        if network_sub.is_linked_to_surfer:
            rb.exit_if_policy_fails(
                await Policies.surfer_owns_network_sub(gibs, network_sub))
        else:
            rb.exit_if_policy_fails(await Policies.public())

        return network_sub
