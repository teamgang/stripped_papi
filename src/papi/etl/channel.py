from sqlalchemy.orm import Session

from papi.models import Channel, ChannelSub


def channels_from_network_id(network_id: str, db: Session):
    channels = db.query(
        Channel.channel_id,
        Channel.abbrev,
        Channel.color_primary,
        Channel.color_secondary,
        Channel.description,
        Channel.name,
        Channel.network_id,
        Channel.notes,
        Channel.page1,
        Channel.route,
        Channel.static_abbrev,
        Channel.static_body,
        Channel.static_thumb_selected,
        Channel.static_title,
        Channel.static_url,
        Channel.thumb_selected,
        Channel.time_created,
        Channel.time_updated,
    ).filter( #type:ignore
             Channel.network_id == network_id).all()

    return [dict(d) for d in channels]


def channel_subs_to_channel_subs_dict(channel_subs: ChannelSub):
    out = {}
    for sub in channel_subs:
        out[sub.channel_id] = sub
    return out
