# Terms of Use

Last updated: May 08, 2022

Thank you for your interest in PushBoi.

__PLEASE READ THESE TERMS OF USE CAREFULLY AND COMPLETELY BEFORE USING, 
REGISTERING FOR, OR SUBMITTING CONTENT TO THE SERVICES (DEFINED BELOW).  BY 
USING, REGISTERING FOR, OR SUBMITTING CONTENT TO THE SERVICES, OR OTHERWISE 
INDICATING YOUR ACCEPTANCE OF THESE TERMS OF USE, YOU AGREE TO THESE TERMS OF 
USE. IF YOU DO NOT 
WISH TO AGREE TO THESE TERMS OF USE, YOU MAY NOT USE, REGISTER FOR, OR SUBMIT 
CONTENT TO, THE SERVICES.__

## 1.  Services Covered
__1.1 Use of the Service is Subject to these Terms.__ These Terms of Use are 
between PushBoi LLC (hereafter referred to as "PB") and you.  The services 
covered by these Terms of Use ("Services") include all versions of PB's website 
(the "Site"), the PB API, and other Internet enabled or wireless means by which 
PB provides content to you or receives content from you, including, without 
limitation, downloadable, preloaded or remotely accessed software applications 
(including, without limitation, PushBoi® mobile applications, tablet applications, 
personal computer applications, interactive/smart TV applications, and virtual/chat/voice 
assistant applications), content submission services, chat rooms, message 
boards, text/SMS messaging services, email messaging services, alert products, 
and delivery of PB content to you at your request.

__1.2 Changes to the Terms.__ PB reserves the right to make changes to the 
Terms from time to time. When these changes are made, PB will make a new copy of the 
Terms available at https://pushboi.io/developer/terms.latest (or such other URL 
as PB may provide). You understand and agree that if you use the Service after 
the date on which the Terms have changed, PB will treat your use as acceptance of 
the updated Terms. If a modification is unacceptable to you, you may terminate 
this agreement by ceasing use of the PB API(s) and website.

## 2. Accepting the Terms.

__2.1 Clicking to Accept or Using the PB website or API(s).__ In order to use 
the PB API(s), you must agree to the Terms by:

  1. clicking to accept the Terms, where this option is made available to you 
  by PB in the Services user interface; or
  2. using the PB API(s). You understand and agree that PB will treat your use 
  of the PB API(s) as acceptance of the Terms from that point onwards. 

__2.2 U.S. Law Restrictions.__ You may not use the PB API(s) and may not accept 
the Terms if you are a person barred from using the Service under United States 
law.

__2.3 Authority to Accept the Terms.__ You represent that you have full power, 
capacity, and authority to accept these Terms. If you are accepting on behalf of 
your employer or another entity, you represent that you have full legal 
authority to bind your employer or such entity to these Terms. If you don't 
have the legal authority to bind, please ensure that an authorized person from 
your entity consents to and accepts these Terms.

## 3. Privacy and Personal Information.

__3.1 PBs Privacy Policy.__ For information about PB's data protection practice
s, please read PB's Privacy Policy (https://pushboi.io/developer/privacy.latest). 
This policy explains how PB treats your personal information and protects your 
privacy when you use the Service.

__3.2 Use of Your Data under PBs Privacy Policy.__ You agree to the use of your 
data in accordance with PB's Privacy Policy.

__3.3 Applicable Privacy Laws.__ You will comply with all applicable laws 
relating to the collection of information from subscribers to your PB Network.

__3.4 No Personally Identifiable Information or Personal Data.__ You must not 
provide to PB: (a) any personally identifiable information or device identifiers; 
or (b) any European persons Personal Data (where "European" means "European 
Economic Area or Switzerland" and "Personal Data" has the meaning provided 
in the General Data Protection Regulation (EU) 2016/679 of the European 
Parliament and of the Council of April 27, 2016). Users of your PB website and 
API Implementation may provide information directly to PB through your PB API 
Implementation, as needed.

## 4. Ownership

The Services are owned and operated by PB, but the Services may include 
elements licensed from or provided by third parties. PB (along with its third 
party licensors) retains all right, title and interest in and to the Services, 
including all intellectual property rights in them. The Services contain 
content (including, without limitation, data, text, software, images, video, 
graphics, music and sound) which is protected by United States and worldwide 
copyright, trademark, trade secret, patent and other intellectual property 
laws, and with the exception of content in the public domain, the rights to 
the content of the Services under such laws are owned or controlled by PB or 
licensed from third parties.

## 5. Limited License

Subject to all of the terms and conditions of these Terms of Use, PB grants you 
a limited, personal, non-exclusive, non-sublicensable, non-transferable license 
to access and use the Services for your personal, non-commercial use only. 
While you may access, view, use and display the Services for your personal, 
non-commercial use, you may not modify, reproduce, distribute,  publicly 
display, publicly perform, rent, lease, participate in the transfer or sale, 
create derivative works, or in any way exploit, any of the content, in whole 
or in part, except as expressly indicated by PB in the Services, as expressly 
permitted under intellectual property laws, or with the express written 
permission of PB and any relevant third party owners of intellectual property 
rights in the content. In the event of any permitted copying, redistribution 
or publication of protected material, you will not change or delete any author 
attribution, trademark legend or copyright notice, and no ownership rights 
will be transferred.

## 6. License Requirements.

__6.1 Lawful use.__  You agree to use the Services for lawful purposes only. 
You agree not to post or transmit through the Services any material which 
violates or infringes in any way upon the rights of others, which is unlawful, 
threatening, abusive, defamatory, invasive of privacy or publicity rights, 
vulgar, obscene, profane or otherwise objectionable, which encourages conduct 
that would constitute a criminal offense, gives rise to civil liability or 
otherwise violates any applicable laws, rules, or regulations, whether federal, state, local, foreign 
or international. You agree not to use the Services to advertise or perform any 
commercial solicitation, including, without limitation, the solicitation of 
users to become subscribers of other services competitive with the Services. 
You agree not to (i) impersonate any person or entity, or otherwise 
misrepresent your affiliation; (ii) manipulate, forge, or otherwise modify 
information or identifiers in a manner which may hide or disguise the origin 
of any information; (iii) alter or delete information not provided by you, 
interfere with the operation of the Services, or upload, post, email, transmit, 
link to or otherwise make available any content that contains any virus, worm, 
computer code, file, or other material intended to cause the interruption, 
destruction, theft or loss of functionality of any Services, software, data, 
equipment, or means of communications; (iv) attempt to gain access to 
confidential information to which you are not entitled; (v) modify, reverse 
engineer, reverse assemble, decompile or hack into any of the systems or 
software used by any Services; (vi) monitor any information in or related to 
the Services for any unauthorized or commercial purpose; or (vii) access, 
monitor or copy, or permit another person or entity to access, monitor 
or copy, the Services using so-called bots, spiders, scrapers or other 
automated means or manual processes without PBs express written permission.  
Any conduct by you that in PB's discretion restricts or inhibits any other user 
from using or enjoying the Services is not permitted.

__6.2 Maintenance.__ You are solely responsible for obtaining and maintaining 
all devices, wired or wireless communications means, telephone, computer 
software, computer hardware and other equipment needed for access to and use of 
the Services and all charges related thereto. For certain Services which are 
software applications, from time to time PB may require the update of the 
Services on your computer or other device when a new version of the Services 
becomes available.  Updates may occur automatically or through other means, or 
you may be required to download updates before continuing to use the Services. 
Notwithstanding the foregoing, PB has no obligation to make available to you 
any updates or new versions of the Services.

__6.3 Communication.__ If you sign up for or request to receive alerts, forecast
s, notifications or other communications from PB through the Services, you 
expressly consent to receiving such communications in the form of telephone 
calls, text messages, push alerts, in-app messages, pop-up messages, and 
e-mails, as applicable, to the contact points identified through the Services, 
and in the case of telephone calls and text messages, you expressly consent to 
receiving calls that use an automatic telephone dialing system and 
pre-recorded or artificial voice messages. You acknowledge and agree that 
certain communications are advertising-supported services, and you consent to 
receive advertising in such communications.

__6.4 Registration Limitations.__  Some of the Services may provide you with the
 opportunity to register by creating a user account in order to enable certain 
 features or receive certain information. You must be at least 18 years of age to 
register. By registering to use the Services, you represent that you are of the 
minimum registration age or older, and if you are under the age of majority in 
your country, state or territory, you represent that you either are an 
emancipated minor or you have obtained the legal consent of your parent or legal guardian 
to enter into these Terms of Use, use the Services, submit material, and fulfill
 the obligations stated in these Terms of Use, which form a binding contract 
 between you and PB.  You authorize PB to collect, use, share and store the 
 information you provide upon registration and thereafter in accordance with 
 PB's Privacy Policy.

__6.5 Registration Representation.__ By registering, you represent and 
warrant that all information that you choose to provide is accurate. You agree 
to update such information as necessary. If the registration process requires 
you to create and use log-in credentials such as an email address or user name 
along with a password, you acknowledge and agree that PB may rely on the 
subsequent use of your log-in credentials to provide access to your account and 
the information you have provided. You are responsible for all use of your 
account, regardless of whether you authorized such access or use, and for 
ensuring that all use of your account complies fully with these Terms of Use.

__6.6 Changes to the Service; Deprecation Policy.__ The following is the 
Services "Deprecation Policy":

  1 PB will announce if it intends to remove major features from, or 
  discontinue, an API or the Service.
  2 PB will make an effort to continue to support old API versions, unless 
  doing so could create a security risk or substantial economic or material 
  technical burden. 

## 7. PushBoi App Subscriptions; Subscriber Agreement

This Section 8 applies if you use PB's PushBoi app:

Users purchase subscriptions for PB's apps through the Apple App Store, the 
Google Play store, or other platform stores, and such purchases are governed by 
the terms and conditions provided to you by the parties operating your mobile 
platform and platform store ("platform operators"). Please contact those 
platform operators in connection with any issues regarding payments, renewals 
and refunds, but contact PB Customer Support regarding the content, features 
and functionality provided under your subscription, including any technical 
support issues. If you are unable to resolve any issues regarding payments, 
renewals and refunds with your platform operator, please contact PB Customer 
Support and we will attempt to assist you.

## 8. PushBoi.io Pricing and Payment Terms

This Section 8 applies if you purchase usage capacity (beyond the Services 
transaction limits) through the PB API pricing plan:

__8.1 Free Quota.__ Certain parts of the Service are provided to you without 
charge up to the transaction limits described on the PB Pricing page.

__8.2 Online Billing.__ PB will issue an electronic bill to you for all charges 
accrued above the transaction limits based on your use of the Service during 
the previous month. You will pay all fees specified in the invoice, including 
the invoices specified currency and payment terms. PB's measurement of your 
use of the Service is final.

__8.3 Taxes.__ In association with your purchase of PB API usage, you are 
responsible for all applicable government-imposed taxes, except for taxes based 
on PB's net income, net worth, employment, and assets (including personal and 
real property) ("Taxes"), and you will pay PB for the Service without any 
reduction for Taxes. If PB is obligated to collect or pay Taxes, the Taxes will 
be invoiced to you, unless you provide PB with a timely and valid tax exemption 
certificate authorized by the appropriate taxing authority. In some states the 
sales tax is due on the total purchase price at the time of sale and must be 
invoiced and collected at the time of the sale.

__8.4 Invoice Disputes & Refunds.__ To the fullest extent permitted by law, you 
waive all claims relating to fees unless claimed within sixty days after 
charged (this does not affect any of your rights with your credit card issuer). 
Refunds (if any) are at PBs discretion and will only be in the form of credit 
for the Service. Nothing in these Terms obligates PB to extend credit to any party.

__8.5 Delinquent Payments.__ Late payments may bear interest at the rate of 
1.5% per month (or the highest rate permitted by law, if less). PB reserves 
the right to suspend your access to the Service for any late payments.

## 9. Terminating this Agreement.

__9.1__ The Terms will continue to apply until terminated by either you or PB 
as described below.

__9.2__ You may terminate your legal agreement with PB by removing the PB 
API(s) code from your PB API Implementation and discontinuing your use of the 
Service at any time. You do not need to specifically inform PB when you stop 
using the Service.

__9.3__ PB reserves the right to terminate these Terms or discontinue the 
Service, or any portion or feature of the Service, for any reason and at any 
time without liability or other obligation to you, except as described under 
Section 4.4 (Changes to the Service; Deprecation Policy).

__9.4__ Nothing in this Section 9 will affect PBs rights under Section 4 
(Provision of Service by PB).

__9.5__ When this legal agreement comes to an end, those Terms that by their 
nature are intended to continue indefinitely will continue to apply, 
including Sections 3.4 (European Data Protection Terms); 9 (Terminating this 
Agreement); 10 (Exclusion of Warranties); 11 (Limitations of Liability); 12 
(Indemnities); and 15 (General Legal Terms).

## 10. EXCLUSION OF WARRANTIES.

__10.1__ NOTHING IN THESE TERMS, INCLUDING SECTIONS 10 AND 11, WILL EXCLUDE OR 
LIMIT PBS WARRANTY OR LIABILITY FOR LOSSES THAT MAY NOT BE LAWFULLY EXCLUDED OR 
LIMITED BY APPLICABLE LAW. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF 
CERTAIN WARRANTIES OR CONDITIONS OR THE LIMITATION OR EXCLUSION OF LIABILITY 
FOR CERTAIN TYPES OF LOSS OR DAMAGES. ACCORDINGLY, ONLY THE LIMITATIONS THAT 
ARE LAWFUL IN YOUR JURISDICTION WILL APPLY TO YOU, AND PBS LIABILITY WILL BE 
LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.

__10.2__ YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR USE OF THE SERVICE AND 
THE CONTENT IS AT YOUR SOLE RISK AND THAT THE SERVICE AND THE CONTENT ARE 
PROVIDED "AS IS" AND "AS AVAILABLE." IN PARTICULAR, PB, ITS SUBSIDIARIES AND 
AFFILIATES, AND ITS LICENSORS AND THEIR SUPPLIERS, DO NOT REPRESENT OR WARRANT TO YOU THAT:

1. THE SERVICE WILL MEET YOUR REQUIREMENTS;
2. THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE;
3. THE SERVICE WILL BE ACCURATE OR RELIABLE; AND
4. DEFECTS IN THE OPERATION OR FUNCTIONALITY OF ANY SOFTWARE PROVIDED TO YOU AS 
PART OF THE SERVICE WILL BE CORRECTED. 

__10.3__ ANY CONTENT OBTAINED THROUGH THE PB SERVICES IS AT YOUR OWN DISCRETION 
AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER 
SYSTEM OR OTHER DEVICE, LOSS OF DATA, OR ANY OTHER DAMAGE OR INJURY THAT 
RESULTS FROM DOWNLOADING OR USING ANY SUCH CONTENT.

__10.4__ NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU 
FROM PB, OR THROUGH OR FROM THE SERVICE OR CONTENT, WILL CREATE ANY WARRANTY 
NOT  EXPRESSLY STATED IN THE TERMS.

__10.5__ PB, ITS LICENSORS, AND THEIR SUPPLIERS FURTHER EXPRESSLY DISCLAIM 
ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, 
INCLUDING THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.

## 11. LIMITATIONS OF LIABILITY.

__11.1__ SUBJECT TO SECTION 10.1, YOU EXPRESSLY UNDERSTAND AND AGREE THAT, TO 
THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, PB, ITS SUBSIDIARIES, AND 
AFFILIATES, AND PB'S LICENSORS AND THEIR SUPPLIERS, WILL NOT BE LIABLE TO YOU 
FOR:

1. ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY, OR 
PUNITIVE DAMAGES THAT MAY BE INCURRED BY YOU, HOWEVER CAUSED AND UNDER ANY 
THEORY OF LIABILITY (INCLUDING CONTRACT, TORT, COMMON LAW, OR STATUTORY 
DAMAGES); ANY LOSS OF REVENUES OR PROFIT (WHETHER INCURRED DIRECTLY OR 
INDIRECTLY); ANY LOSS OF GOODWILL OR BUSINESS REPUTATION; ANY LOSS OF DATA; 
ANY COST TO PROCURE SUBSTITUTE GOODS OR SERVICES; OR ANY INTANGIBLE LOSS; OR

2. ANY LOSS OR DAMAGE AS A RESULT OF:
    1. ANY RELIANCE PLACED BY YOU ON THE COMPLETENESS, ACCURACY, OR EXISTENCE 
    OF ANY ADVERTISING, OR AS A RESULT OF ANY RELATIONSHIP OR TRANSACTION 
    BETWEEN YOU AND ANY ADVERTISER OR SPONSOR WHOSE ADVERTISING APPEARS ON PB SERVICES;
    2. ANY CHANGES THAT PB MAY MAKE TO THE SERVICE, OR ANY PERMANENT OR 
    TEMPORARY DISCONTINUATION OF THE SERVICE (OR ANY FEATURES WITHIN THE SERVICE);
    3. THE DELETION OR CORRUPTION OF, OR FAILURE TO STORE, ANY CONTENT AND 
    OTHER DATA MAINTAINED OR TRANSMITTED BY OR THROUGH YOUR USE OF THE SERVICE;
    4. YOUR FAILURE TO PROVIDE PB WITH ACCURATE ACCOUNT INFORMATION; OR
    5. YOUR FAILURE TO KEEP YOUR PASSWORD OR ACCOUNT DETAILS SECURE AND 
    CONFIDENTIAL. 

__11.2__ THE LIMITATIONS ON PBS LIABILITY IN SECTION 11.1 ABOVE WILL APPLY 
WHETHER OR NOT PB, ITS SUBSIDIARIES, AFFILIATES, LICENSORS OR THEIR SUPPLIERS 
HAVE BEEN ADVISED OF OR SHOULD HAVE BEEN AWARE OF THE POSSIBILITY OF ANY SUCH 
LOSSES OR DAMAGES.

## 12. Indemnities.

__12.1__ You will defend and indemnify PB and its affiliates, directors, 
officers, employees, strategic partners, licensors, and their suppliers (the 
"Indemnified Parties") against all liabilities, damages, losses, costs, fees 
(including legal fees), and expenses relating to any allegation or third-party 
legal proceeding to the extent arising from:

1. your use of the Service or the Content in breach of the Terms or applicable 
policies;
2. your Maps API Implementation, including any claim that your Maps API 
Implementation infringes a third party's rights or violates applicable law; or
3. Your Content. 

__12.2__ You will cooperate as fully as reasonably required in the defense of 
any allegation or third-party legal proceeding. PB reserves the right, at its 
own expense, to assume the exclusive control and defense of any indemnified 
matter under this Section 12.

## 13. Copyright Policies; Content Removal; Termination of Repeat Offenders Accounts.

It is PBs policy to respond to notices of alleged copyright infringement that 
comply with applicable international intellectual property law (including, in 
the United States, the Digital Millennium Copyright Act) and to terminate the 
accounts of repeat offenders. Details of PBs policy can be found here.

## 14. Other Content.

__18.1__ The Service may include hyperlinks to other websites or content or 
resources. PB has no control over any websites or resources that are provided 
by companies or persons other than PB. You understand and agree that PB is not 
responsible for the availability of any such external sites or resources, and 
does not endorse any advertising, products, or other materials on, or available 
from, such websites or resources.

__18.2__ You understand and agree that PB is not liable for any loss or damage 
that you may incur as a result of the availability of those external sites or 
resources, or as a result of any reliance by you on the completeness, accuracy, 
or existence of any advertising, products, or other materials on, or 
available from, such websites or resources.

## 15. General Legal Terms.

__15.1 Notices.__ PB may provide you with notices, including those regarding 
changes to the Terms, by email, regular mail, or postings on the Service.

__15.2 Assignment.__ PB may assign any part of this agreement without written 
consent.

__15.3 No Waiver.__ PB will not be treated as having waived any rights by not 
exercising (or delaying the exercise of) any rights under these Terms. A 
waiver will be effective only if PB expressly states in a writing signed by an 
authorized representative that PB is waiving a specified Term.

__15.4 Third-Party Beneficiaries.__ PBs affiliates and the Indemnified Parties 
are third-party beneficiaries to the Terms and are entitled to directly 
enforce, and rely on, any Terms that confer a right or benefit to them. There 
are no other third-party beneficiaries to the Terms.

__15.5 Entire Agreement.__ These Terms set out all terms agreed between the 
parties and supersede all other agreements between the parties relating to its 
subject matter.

__15.6 Severability.__ If any term (or part of a term) of these Terms is 
invalid, illegal or unenforceable, the rest of the Terms will remain in effect.

__15.7 Equitable Relief.__ You understand and agree that damages for improper 
use of the Maps API(s) may be irreparable; therefore, PB is entitled to seek 
equitable relief, including injunctions in any jurisdiction, in addition to 
all other remedies it may have.

__15.8 Conflicting Languages.__ If these Terms are translated into any other 
language, and there is a discrepancy between the English text and the 
translated text, the English text will govern.

__15.9 Governing Law.__ ALL CLAIMS ARISING OUT OF OR RELATING TO THESE TERMS OR 
ANY RELATED PB SERVICES WILL BE GOVERNED BY CALIFORNIA LAW, EXCLUDING 
CALIFORNIA'S CONFLICT OF LAWS RULES, AND WILL BE LITIGATED EXCLUSIVELY IN THE 
FEDERAL OR STATE COURTS OF SANTA CLARA COUNTY, CALIFORNIA, USA; THE PARTIES 
CONSENT TO PERSONAL JURISDICTION IN THOSE COURTS. 
