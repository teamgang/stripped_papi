This project has to manage an interesting disconnect between a user and 
a user's device. Therefore, we manage 'subscriptions' which are tied
to basically anonymous user accounts that we generate. These user
accounts might never be used. But, one day, they might be used.

Put another way:
* Each device has a subscription
* Each subscription is tied to a use
  * a user can have multiple devices and a device can have multiple users
  * Some projects might want users to be able to manage accounts on pushboy
