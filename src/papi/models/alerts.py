from sqlalchemy import Boolean, Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin


class Alert(Base, TimesMixin):
    __tablename__ = 'alerts'
    alert_id = cast(Puid, Column(UUID(as_uuid=True),
                                 nullable=False,
                                 primary_key=True,
                                 unique=True,
                                 ))
    developer_id = cast(Puid, Column(UUID(as_uuid=True)))
    surfer_id = cast(Puid, Column(UUID(as_uuid=True)))
    instance_id = cast(str, Column(NonNullString))
    project_id = cast(Puid, Column(UUID(as_uuid=True)))
    network_id = cast(Puid, Column(UUID(as_uuid=True)))
    channel_id = cast(Puid, Column(UUID(as_uuid=True)))
    msg = cast(str, Column(NonNullString, nullable=False))
    subject = cast(str, Column(NonNullString, nullable=False))
    was_viewed = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'alert_id': self.alert_id,
            'developer_id': self.developer_id,
            'surfer_id': self.surfer_id,
            'instance_id': self.instance_id,
            'project_id': self.project_id,
            'network_id': self.network_id,
            'channel_id': self.channel_id,
            'msg': self.msg,
            'subject': self.subject,
            'was_viewed': self.was_viewed,
        }


class Email(Base, TimesMixin):
    __tablename__ = 'emails'
    email_id = cast(Puid, Column(UUID(as_uuid=True),
                                 nullable=False,
                                 primary_key=True,
                                 unique=True,
                                 ))
    body = cast(str, Column(NonNullString, nullable=False))
    developer_id = cast(Puid, Column(UUID(as_uuid=True),
                                     ForeignKey('developers.developer_id')))
    from_ = cast(str, Column(NonNullString, nullable=False))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id')))
    subject = cast(str, Column(NonNullString, nullable=False))
    to_ = cast(str, Column(NonNullString, nullable=False))
    was_allow_email_bills_enabled = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'email_id': self.email_id,
            'body': self.body,
            'developer_id': self.developer_id,
            'from_': self.from_,
            'project_id': self.project_id,
            'subject': self.subject,
            'to_': self.to_,
            'was_allow_email_bills_enabled': self.was_allow_email_bills_enabled,
        }
