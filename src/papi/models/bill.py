import json
from sqlalchemy import Boolean, Column, ForeignKey, Integer, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin

BILL_TYPE_MONTHLY = 'monthly'

BILL_STATUS_CREATED = 'created'
BILL_STATUS_PAID = 'paid'
BILL_STATUS_PROCESSING = 'processing'
BILL_STATUS_TRY_AGAIN = 'try_again'


class Bill(Base, TimesMixin):
    __tablename__ = 'bills'
    bill_id = cast(Puid, Column(UUID(as_uuid=True),
                                nullable=False,
                                primary_key=True,
                                server_default=text("uuid_generate_v4()"),
                                unique=True,
                                ))
    admin_notes = cast(str, Column(NonNullString))
    bill_type = cast(str, Column(NonNullString, nullable=False))
    charge_ids = cast(str, Column(NonNullString, nullable=False))
    credit_applied = cast(int, Column(Integer, default=0))
    credit_overflow = cast(int, Column(Integer, default=0))
    currency = cast(str, Column(NonNullString, nullable=False))
    notes = cast(str, Column(NonNullString, nullable=False))
    period = cast(int, Column(Integer, nullable=False))
    price = cast(int, Column(Integer, nullable=False))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False))
    status = cast(str, Column(NonNullString, nullable=False))
    was_autopay_used = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'bill_id': self.bill_id,
            'admin_notes': self.admin_notes,
            'bill_type': self.bill_type,
            'charge_ids': json.loads(self.charge_ids),
            'credit_applied': self.credit_applied,
            'credit_overflow': self.credit_overflow,
            'currency': self.currency,
            'notes': self.notes,
            'period': self.period,
            'price': self.price,
            'project_id': self.project_id,
            'status': self.status,
            'was_autopay_used': self.was_autopay_used,
        }
