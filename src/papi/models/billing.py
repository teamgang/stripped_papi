from sqlalchemy import Boolean, Column, ForeignKey, Integer, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin

ITEM_TYPE_NOTIFICATION = 0

PAYMENT_PLATFORM_STRIPE = 0
PAYMENT_PLATFORM_BTCPAY = 1

SILO_ADD_CREDIT = 'add_credit'
SILO_BASKET = 'basket'
SILO_BILL_PAY = 'bill_pay'
SILO_PLAN = 'plan'

ATTEMPT_STATUS_CREATED = 5
ATTEMPT_STATUS_FAILED = 10
ATTEMPT_STATUS_WAITING_ON_STRIPE = 30
ATTEMPT_STATUS_WAITING_TO_PROCESS = 50
ATTEMPT_STATUS_PROCESSING = 60
ATTEMPT_STATUS_PROCESSED = 70

BASKET_ITEM_TYPE_ADD_CREDIT = 'add_credit'
BASKET_ITEM_TYPE_BILL_PAY = 'bill_pay'
BASKET_ITEM_TYPE_NONE = 'none'
BASKET_ITEM_TYPE_PLAN = 'plan'

CHARGE_TYPE_PLAN = 1
CHARGE_TYPE_BOIS_CUSTOM = 2
CHARGE_TYPE_BOIS_LIMITLESS = 3
CHARGE_TYPE_BOIS_STATIC = 4
CHARGE_TYPE_STAT_EDIT_TOKEN = 5

RULES_NO_NOTIFICATION_PURCHASES = 'no_notification_purchases'


class Plan(Base, TimesMixin):
    __tablename__ = 'plans'
    plan_id = cast(int, Column(Integer,
                               nullable=False,
                               primary_key=True,
                               unique=True,
                               ))
    add_custom_price = cast(int, Column(Integer, nullable=False))
    add_custom_units = cast(int, Column(Integer, nullable=False))
    add_limitless_price = cast(int, Column(Integer, nullable=False))
    add_limitless_units = cast(int, Column(Integer, nullable=False))
    add_static_price = cast(int, Column(Integer, nullable=False))
    add_static_units = cast(int, Column(Integer, nullable=False))
    bundled_static = cast(int, Column(Integer, nullable=False))
    bundled_custom = cast(int, Column(Integer, nullable=False))
    bundled_limitless = cast(int, Column(Integer, nullable=False))
    monthly_credit = cast(int, Column(Integer, nullable=False))
    monthly_free_custom = cast(int, Column(Integer, nullable=False))
    monthly_free_limitless = cast(int, Column(Integer, nullable=False))
    monthly_free_static = cast(int, Column(Integer, nullable=False))
    monthly_free_static_edit_tokens = cast(int, Column(Integer, nullable=False))

    months= cast(int, Column(Integer, nullable=False))
    price = cast(int, Column(Integer, nullable=False))
    price_currency = cast(str, Column(NonNullString, nullable=False))
    static_edit_token_price = cast(int, Column(Integer, nullable=False))

    admin_notes = cast(str, Column(NonNullString, nullable=False))
    channel_limit = cast(int, Column(Integer, nullable=False))
    description = cast(str, Column(NonNullString, nullable=False))
    is_available = cast(bool, Column(Boolean, nullable=False))
    is_tailored = cast(bool, Column(Boolean, nullable=False))
    rules = cast(str, Column(NonNullString, nullable=False))
    subscription_limit = cast(int, Column(Integer, nullable=False))
    title = cast(str, Column(NonNullString, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'plan_id': self.plan_id,
            'add_custom_price': self.add_custom_price,
            'add_custom_units': self.add_custom_units,
            'add_limitless_price': self.add_limitless_price,
            'add_limitless_units': self.add_limitless_units,
            'add_static_price': self.add_static_price,
            'add_static_units': self.add_static_units,
            'bundled_static': self.bundled_static,
            'bundled_custom': self.bundled_custom,
            'bundled_limitless': self.bundled_limitless,
            'monthly_credit': self.monthly_credit,
            'monthly_free_custom': self.monthly_free_custom,
            'monthly_free_limitless': self.monthly_free_limitless,
            'monthly_free_static': self.monthly_free_static,
            'monthly_free_static_edit_tokens':
                self.monthly_free_static_edit_tokens,
            'months': self.months,
            'price': self.price,
            'price_currency': self.price_currency,
            'static_edit_token_price': self.static_edit_token_price,

            'admin_notes': self.admin_notes,
            'channel_limit': self.channel_limit,
            'description': self.description,
            'is_available': self.is_available,
            'is_tailored': self.is_tailored,
            'rules': self.rules,
            'subscription_limit': self.subscription_limit,
            'title': self.title,
        }

    def can_purchase_bois(self) -> bool:
        return not RULES_NO_NOTIFICATION_PURCHASES in self.rules #type:ignore


class PlanChangeRequest(Base, TimesMixin):
    __tablename__ = 'plan_change_requests'
    plan_change_id = cast(Puid, Column(UUID(as_uuid=True),
                                       nullable=False,
                                       primary_key=True,
                                       server_default=text("uuid_generate_v4()"),
                                       unique=True,
                                       ))
    developer_id = cast(Puid, Column(UUID(as_uuid=True),
                                     ForeignKey('developers.developer_id'),
                                     nullable=False,
                                     primary_key=True,
                                     server_default=text("uuid_generate_v4()"),
                                     ))
    plan_id_next = cast(int, Column(Integer,
                                    ForeignKey('plans.plan_id'),
                                    nullable=False))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   primary_key=True,
                                   server_default=text("uuid_generate_v4()"),
                                   ))
    terms_accepted_version = cast(str, Column(NonNullString))

    def is_agreement_accepted(self):
        return len(self.terms_accepted_version) > 0

    def as_dict(self):
        return {
            'plan_change_id': self.plan_change_id,
            'developer_id': self.developer_id,
            'plan_id_next': self.plan_id_next,
            'project_id': self.project_id,
            'terms_accepted_version': self.terms_accepted_version,
        }


class Charge(Base, TimesMixin):
    __tablename__ = 'charges'
    charge_id = cast(Puid, Column(UUID(as_uuid=True),
                                  nullable=False,
                                  primary_key=True,
                                  server_default=text("uuid_generate_v4()"),
                                  unique=True,
                                  ))
    boi_id = cast(Puid, Column(UUID(as_uuid=True),
                               ForeignKey('bois.boi_id'),
                               ))
    charge_type = cast(int, Column(Integer, nullable=False))
    currency = cast(str, Column(NonNullString))
    period = cast(int, Column(Integer, nullable=False))
    plan_id = cast(int, Column(Integer,
                               ForeignKey('plans.plan_id'),
                               nullable=False))
    price = cast(int, Column(Integer, nullable=False))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    units = cast(int, Column(Integer, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'charge_id': self.charge_id,
            'boi_id': self.boi_id,
            'charge_type': self.charge_type,
            'currency': self.currency,
            'period': self.period,
            'plan_id': self.plan_id,
            'price': self.price,
            'project_id': self.project_id,
            'units': self.units,
        }


class Payment(Base, TimesMixin):
    __tablename__ = 'payments'
    payment_id = cast(Puid, Column(UUID(as_uuid=True),
                                   nullable=False,
                                   primary_key=True,
                                   server_default=text("uuid_generate_v4()"),
                                   unique=True,
                                   ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    bill_id = cast(Puid, Column(UUID(as_uuid=True)))
    btcpay_invoice_id = cast(str, Column(NonNullString))
    payment_intent_id = cast(str, Column(NonNullString))
    payment_platform = cast(int, Column(Integer, nullable=False))
    stripe_invoice_id = cast(str, Column(NonNullString))

    admin_notes = cast(str, Column(NonNullString))
    basket = cast(str, Column(NonNullString, nullable=False))
    credit_applied = cast(int, Column(Integer, nullable=False))
    currency = cast(str, Column(NonNullString, nullable=False))
    developer_notes = cast(str, Column(NonNullString))
    is_autopay_selected = cast(bool, Column(Boolean, nullable=False))
    plastic_id = cast(str, Column(NonNullString))
    silo = cast(str, Column(NonNullString, nullable=False))
    status = cast(int, Column(Integer, nullable=False))
    terms_accepted_version = cast(str, Column(NonNullString))
    total_price = cast(int, Column(Integer, nullable=False))
    total_price_after_credit = cast(int, Column(Integer, nullable=False))

    def is_agreement_accepted(self):
        return len(self.terms_accepted_version) > 0

    def as_dict(self):
        return {
            **self.time_dict(),
            'payment_id': self.payment_id,
            'project_id': self.project_id,
            'bill_id': self.bill_id or '',
            'btcpay_invoice_id': self.btcpay_invoice_id,
            'payment_intent_id': self.payment_intent_id,
            'payment_platform': self.payment_platform,
            'stripe_invoice_id': self.stripe_invoice_id,

            'admin_notes': self.admin_notes,
            'basket': self.basket,
            'credit_applied': self.credit_applied,
            'currency': self.currency,
            'developer_notes': self.developer_notes,
            'is_autopay_selected': self.is_autopay_selected,
            'plastic_id': self.plastic_id,
            'silo': self.silo,
            'status': self.status,
            'terms_accepted_version': self.terms_accepted_version,
            'total_price': self.total_price,
            'total_price_after_credit': self.total_price_after_credit,
        }


class StripePaymentAttempt(Base, TimesMixin):
    __tablename__ = 'stripe_payment_attempts'
    attempt_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             nullable=False,
                             primary_key=True,
                             server_default=text("uuid_generate_v4()"),
                             unique=True,
                             ))
    payment_intent_id = cast(str, Column(NonNullString))
    admin_notes = cast(str, Column(NonNullString))
    basket = cast(str, Column(NonNullString, nullable=False))
    bill_id = cast(Puid, Column(UUID(as_uuid=True)))
    credit_applied = cast(int, Column(Integer, nullable=False))
    currency = cast(str, Column(NonNullString, nullable=False))
    is_autopay_selected = cast(bool, Column(Boolean, nullable=False))
    plan_id = cast(int, Column(Integer, nullable=False))
    plastic_id = cast(str, Column(NonNullString, nullable=False))
    project_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             ForeignKey('projects.project_id'),
                             nullable=False,
                             ))
    silo = cast(str, Column(NonNullString, nullable=False))
    status = cast(int, Column(Integer, nullable=False))
    terms_accepted_version = cast(str, Column(NonNullString))
    total_price = cast(int, Column(Integer, nullable=False))
    total_price_after_credit = cast(int, Column(Integer, nullable=False))

    def is_agreement_accepted(self):
        return len(self.terms_accepted_version) > 0

    def as_dict(self):
        return {
            **self.time_dict(),
            'attempt_id': self.attempt_id,
            'admin_notes': self.admin_notes,
            'basket': self.basket,
            'bill_id': self.bill_id or '',
            'credit_applied': self.credit_applied,
            'currency': self.currency,
            'is_autopay_selected': self.is_autopay_selected,
            'plan_id': self.plan_id,
            'plastic_id': self.plastic_id,
            'payment_intent_id': self.payment_intent_id,
            'project_id': self.project_id,
            'silo': self.silo,
            'status': self.status,
            'terms_accepted_version': self.terms_accepted_version,
            'total_price': self.total_price,
            'total_price_after_credit': self.total_price_after_credit,
        }


class AgreementAcceptedLogs(Base, TimesMixin):
    __tablename__ = 'agreement_accepted_logs'
    devleoper_id = cast(Puid, Column(UUID(as_uuid=True),
                                     nullable=False,
                                     primary_key=True,
                                     server_default=text("uuid_generate_v4()"),
                                     ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    is_accepted = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'developer_id': self.developer_id,
            'project_id': self.project_id,
            'is_accepted': self.is_accepted,
        }
