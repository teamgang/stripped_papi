from sqlalchemy import Boolean, Column, ForeignKey, Integer, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.conf import conf
from papi.core import Puid
from papi.models.channels import Channel
from papi.models.generics import Base, NonNullString, TimesMixin

AS_USER = 'asUser'

PLASTICITY_CUSTOM = 'custom'
PLASTICITY_LIMITLESS = 'limitless'
PLASTICITY_STATIC = 'static'

BOI_STATUS_DONE = 'done'
BOI_STATUS_ERROR_CHARGE = 'error_charge'
BOI_STATUS_ERROR_SHIP = 'error_ship'
BOI_STATUS_FOR_CHARGE = 'for_charge'
BOI_STATUS_FOR_SHIP = 'for_ship'

BOI_LAYOUT_BIG_PICTURE = 'bigPicture'
BOI_LAYOUT_BIG_TEXT = 'bigText'
BOI_LAYOUT_DEFAULT = 'default'


class Boi(Base, TimesMixin):
    __tablename__ = 'bois'
    boi_id = cast(Puid, Column(UUID(as_uuid=True),
                               nullable=False,
                               primary_key=True,
                               server_default=text("uuid_generate_v4()"),
                               unique=True,
                               ))
    channel_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('channels.channel_id'),
                                   nullable=False,
                                   ))
    network_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('networks.network_id'),
                                   nullable=False,
                                   ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))

    body = cast(str, Column(NonNullString, nullable=False))

    # cm = count_metric
    cm_created = cast(int, Column(Integer, default=0))
    cm_dismissed = cast(int, Column(Integer, default=0))
    cm_displayed = cast(int, Column(Integer, default=0))
    cm_tapped = cast(int, Column(Integer, default=0))
    cm_viewed_in_app = cast(int, Column(Integer, default=0))
    count_failure = cast(int, Column(Integer, nullable=False, default=0))
    count_success = cast(int, Column(Integer, nullable=False, default=0))
    count_recipient_instance_ids = cast(int, Column(Integer, default=0))

    error = cast(str, Column(NonNullString,
                                       nullable=False,
                                       default=''))
    is_hidden = cast(bool, Column(Boolean, nullable=False))
    layout = cast(str, Column(NonNullString, nullable=False))
    notes = cast(str, Column(NonNullString, nullable=False))
    period = cast(int, Column(Integer, nullable=False))
    plan_id = cast(int, Column(Integer, nullable=False))
    plasticity = cast(str, Column(NonNullString, nullable=False))
    status = cast(str, Column(NonNullString, nullable=False))
    title = cast(str, Column(NonNullString, nullable=False))
    thumb_selected = cast(str, Column(NonNullString, nullable=False))
    token = cast(str, Column(NonNullString, nullable=False))
    url = cast(str, Column(NonNullString, nullable=False))

    def as_dict(self, channel: Channel):
        return {
            **self.time_dict(),
            'boi_id': str(self.boi_id),
            'channel_id': str(self.channel_id),
            'network_id': str(self.network_id),
            'body': self.body,

            'cm_created': self.cm_created,
            'cm_dismissed': self.cm_dismissed,
            'cm_displayed': self.cm_displayed,
            'cm_tapped': self.cm_tapped,
            'cm_viewed_in_app': self.cm_viewed_in_app,
            'count_failure': self.count_failure,
            'count_success': self.count_success,
            'count_recipient_instance_ids': self.count_recipient_instance_ids,

            'error': self.error,
            'is_hidden': self.is_hidden,
            'layout': self.layout,
            'notes': self.notes,
            'period': self.period,
            'plan_id': self.plan_id,
            'plasticity': self.plasticity,
            'project_id': str(self.project_id),
            'status': self.status,
            'thumb_selected': self.thumb_selected,
            'thumb_selected_url': self.get_thumb_selected_url(channel),
            'title': self.title,
            'token': self.get_token_stripped(),
            'url': self.url,

            'channel_abbrev': channel.abbrev,
            'channel_name': channel.name,
            'channel_thumb_abbrev': channel.thumb_abbrev,
            'channel_thumb_channel': channel.thumb_channel,
            'channel_thumb_selected': channel.thumb_selected,
            'color_primary': channel.color_primary,
            'color_secondary': channel.color_secondary,
        }

    def get_token_stripped(self):
        if self.token == AS_USER:
            return self.token

        stripped = self.token.split('_') #type:ignore
        return f'{stripped[0]}_*****{self.token[-4:]}' #type:ignore

    def is_layout_default(self):
        return self.layout == BOI_LAYOUT_DEFAULT

    def is_layout_big_picture(self):
        return self.layout == BOI_LAYOUT_BIG_PICTURE

    def is_layout_big_text(self):
        return self.layout == BOI_LAYOUT_BIG_TEXT

    def is_plasticity_custom(self):
        return self.plasticity == PLASTICITY_CUSTOM

    def is_plasticity_limitless(self):
        return self.plasticity == PLASTICITY_LIMITLESS

    def is_plasticity_static(self):
        return self.plasticity == PLASTICITY_STATIC

    @property
    def channel_thumb_abbrev(self):
        return conf.make_image_url(f'img/chanabbrev/{self.channel_id}.webp');

    @property
    def channel_thumb_channel(self):
        return conf.make_image_url(f'img/chan/{self.channel_id}.webp');

    def get_thumb_selected_url(self, channel: Channel):
        if self.thumb_selected == 'abbrev':
            return channel.thumb_abbrev
        if self.thumb_selected == 'chan' or self.thumb_selected == '':
            return channel.get_thumb_selected_url
        if self.thumb_selected == 'static':
            return channel.get_static_thumb_selected_url
        else:
            #external and uploaded image case
            return self.thumb_selected


class BoiRecipient(Base, TimesMixin):
    __tablename__ = 'boi_recipients'
    boi_recipient_id = cast(Puid,
                            Column(UUID(as_uuid=True),
                                   nullable=False,
                                   primary_key=True,
                                   server_default=text("uuid_generate_v4()"),
                                   ))
    boi_id = cast(Puid, Column(UUID(as_uuid=True),
                               ForeignKey('bois.boi_id'),
                               nullable=False,
                               ))
    instance_id = cast(str, Column(NonNullString))
    network_sub_id = cast(Puid, Column(UUID(as_uuid=True)))
    is_silenced = cast(bool, Column(Boolean))
    surfer_sound = cast(str, Column(NonNullString))
    use_surfer_sound = cast(bool, Column(Boolean))

    # m = metric
    m_created = cast(bool, Column(Integer, default=False))
    m_dismissed = cast(bool, Column(Integer, default=False))
    m_displayed = cast(bool, Column(Integer, default=False))
    m_tapped = cast(bool, Column(Integer, default=False))
    m_viewed_in_app = cast(bool, Column(Integer, default=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'instance_id': self.instance_id,
            'network_sub_id': self.network_sub_id,
            'is_silenced': self.is_silenced,
            'surfer_sound': self.surfer_sound,
            'use_surfer_sound': self.use_surfer_sound,

            'm_created': self.m_created,
            'm_dismissed': self.m_dismissed,
            'm_displayed': self.m_displayed,
            'm_tapped': self.m_tapped,
            'm_viewed_in_app': self.m_viewed_in_app,
        }
