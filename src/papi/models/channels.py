from sqlalchemy import BigInteger, Column, ForeignKey, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.conf import conf
from papi.core import Puid
from papi.core.files import delete_file, save_thumb_b64
from papi.models.generics import Base, NonNullString, TimesMixin


class Channel(Base, TimesMixin):
    __tablename__ = 'channels'
    channel_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             nullable=False,
                             primary_key=True,
                             server_default=text("uuid_generate_v4()"),
                             unique=True,
                             ))
    network_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('networks.network_id'),
                                   index=True,
                                   nullable=False,
                                   ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   index=True,
                                   nullable=False,
                                   ))
    abbrev = cast(str, Column(NonNullString, nullable=False))
    color_primary = cast(str, Column(NonNullString, nullable=False))
    color_secondary = cast(str, Column(NonNullString, nullable=False))
    description = cast(str, Column(NonNullString, nullable=False))
    name = cast(str, Column(NonNullString, nullable=False))
    notes = cast(str, Column(NonNullString, nullable=False))
    page1 = cast(str, Column(NonNullString, nullable=False))
    route = cast(str, Column(NonNullString, nullable=False))
    route_lower = cast(str, Column(NonNullString, nullable=False))
    static_body = cast(str, Column(NonNullString, nullable=False))
    static_edit_expires = cast(int, Column(BigInteger, nullable=False))
    static_thumb_selected = cast(str, Column(NonNullString, nullable=False))
    static_title = cast(str, Column(NonNullString, nullable=False))
    static_url = cast(str, Column(NonNullString, nullable=False))
    thumb_selected = cast(str, Column(NonNullString, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'channel_id': self.channel_id,
            'network_id': self.network_id,
            'project_id': self.project_id,
            'abbrev': self.abbrev,
            'color_primary': self.color_primary,
            'color_secondary': self.color_secondary,
            'description': self.description,
            'name': self.name,
            'notes': self.notes,
            'page1': self.page1,
            'route': self.route,

            'static_body': self.static_body,
            'static_edit_expires': self.static_edit_expires,
            'static_thumb_selected': self.static_thumb_selected,
            'static_title': self.static_title,
            'static_url': self.static_url,

            'thumb_abbrev': self.thumb_abbrev,
            'thumb_channel': self.thumb_channel,
            'thumb_selected': self.thumb_selected,
        }

    def save_abbrev(self, thumb_b64: str):
        pathpart: str = f'/img/chanabbrev/{self.channel_id}.webp'
        filepath: str = f'{conf.hosted_path}{pathpart}'
        delete_file(filepath)
        save_thumb_b64(filepath, thumb_b64)

    @property
    def thumb_abbrev(self) -> str:
        return conf.make_image_url(f'img/chanabbrev/{self.channel_id}.webp');

    @property
    def thumb_channel(self) -> str:
        return conf.make_image_url(f'img/chan/{self.channel_id}.webp');

    @property
    def get_thumb_selected_url(self) -> str:
        if self.thumb_selected == 'abbrev':
            return self.thumb_abbrev
        elif self.thumb_selected == 'chan':
            return self.thumb_channel
        else:
            return self.thumb_selected

    @property
    def get_static_thumb_selected_url(self) -> str:
        if self.static_thumb_selected == 'abbrev':
            return self.thumb_abbrev
        elif self.static_thumb_selected == 'chan':
            return self.get_thumb_selected_url
        else:
            #external and uploaded link case
            return self.static_thumb_selected
