from sqlalchemy import BigInteger, Boolean, Column, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin


class Developer(Base, TimesMixin):
    __tablename__ = 'developers'
    developer_id = cast(Puid, Column(UUID(as_uuid=True),
                                     nullable=False,
                                     primary_key=True,
                                     server_default=text("uuid_generate_v4()"),
                                     unique=True,
                                     ))
    email = cast(str, Column(NonNullString,
                             nullable=False,
                             unique=True,
                             ))
    allow_email_bills = cast(bool, Column(Boolean, nullable=False))
    is_email_verified = cast(bool, Column(Boolean, nullable=False))
    is_project_owner = cast(bool, Column(Boolean, nullable=False))
    password = cast(str, Column(NonNullString, nullable=False))
    phone = cast(str, Column(NonNullString, nullable=False))
    stripe_customer_id = cast(str, Column(NonNullString))

    def as_dict(self):
        return {
            **self.time_dict(),
            'developer_id': self.developer_id,
            'allow_email_bills': self.allow_email_bills,
            'email': self.email,
            'phone': self.phone,
        }


class DevChangeEmailRequest(Base, TimesMixin):
    __tablename__ = 'dev_change_email_requests'
    token = cast(str, Column(NonNullString,
                             nullable=False,
                             primary_key=True,
                             ))
    developer_id = cast(Puid, Column(UUID(as_uuid=True), nullable=False))
    is_applied = cast(bool, Column(Boolean, nullable=False))
    is_consumed = cast(bool, Column(Boolean, nullable=False))
    new_email = cast(str, Column(NonNullString))
    old_email = cast(str, Column(NonNullString))
    time_expires = cast(int, Column(BigInteger, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'developer_id': self.developer_id,
            'is_applied': self.is_applied,
            'is_consumed': self.is_consumed,
            'new_email': self.new_email,
            'old_email': self.old_email,
            'token': self.token,
            'time_expires': self.time_expires,
        }


class DevChangePasswordRequest(Base, TimesMixin):
    __tablename__ = 'dev_change_password_requests'
    token = cast(str, Column(NonNullString,
                                       nullable=False,
                                       primary_key=True,
                                       ))
    developer_id = cast(Puid, Column(UUID(as_uuid=True), nullable=False))
    is_consumed = cast(bool, Column(Boolean, nullable=False))
    time_expires = cast(int, Column(BigInteger, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'developer_id': self.developer_id,
            'is_consumed': self.is_consumed,
            'token': self.token,
            'time_expires': self.time_expires,
        }
