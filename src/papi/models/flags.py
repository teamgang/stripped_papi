from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin


class Flag(Base, TimesMixin):
    __tablename__ = 'flags'
    flag_id = cast(Puid, Column(UUID(as_uuid=True),
                                nullable=False,
                                primary_key=True,
                                unique=True,
                                ))
    surfer_id = cast(Puid, Column(UUID(as_uuid=True)))
    instance_id = cast(str, Column(NonNullString))
    msg = cast(str, Column(NonNullString, nullable=False))
    category = cast(str, Column(NonNullString, nullable=False))

    boi_id = cast(Puid, Column(UUID(as_uuid=True)))

    def as_dict(self):
        return {
            **self.time_dict(),
            'flag_id': self.flag_id,
            'surfer_id': self.surfer_id,
            'instance_id': self.instance_id,
            'category': self.category,
            'msg': self.msg,

            'boi_id': self.boi_id,
        }
