from datetime import datetime, timezone
from sqlalchemy import BigInteger, Column, types
from sqlalchemy.ext.declarative import declarative_base
from typing import cast

from papi.core import nowstamp


Base = declarative_base()

class NonNullString(types.TypeDecorator):
    cache_ok = True
    impl = types.Unicode
    def process_result_value(self, value, dialect):
        return value if value else ''


class TimesMixin(object):
    time_created = cast(int, Column(BigInteger,
                                    nullable=False,
                                    default=nowstamp,
                                    ))
    time_updated = cast(int, Column(BigInteger,
                                    nullable=False,
                                    default=nowstamp,
                                    onupdate=nowstamp,
                                    ))

    def time_dict(self):
        return  {
            'time_created': self.time_created,
            'time_updated': self.time_updated,
        }

    def datetime_created(self):
        return datetime.utcfromtimestamp(self.time_created)

    def datetime_updated(self):
        return datetime.utcfromtimestamp(self.time_updated)

    def month_created(self):
        return self.datetime_created.strftime('%m')

    def day_created(self):
        return self.datetime_created.strftime('%d')

    def update_time_updated(self):
        self.time_created = datetime.now(timezone.utc).timestamp()
