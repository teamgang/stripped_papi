from sqlalchemy import Column, Integer
from sqlalchemy.dialects.postgresql import UUID
from typing import cast, Optional

from papi.core import Puid
from papi.models.generics import Base, NonNullString, TimesMixin

# platforms:
# android
# ios
# linux
# macos
# web
# windows

# user types
# developer
# surfer

# levels
# verbose
# debug
# info
# warning
# error
# wtf


class Plog(Base, TimesMixin):
    __tablename__ = 'plogs'
    plog_id = cast(Puid, Column(UUID(as_uuid=True),
                                nullable=False,
                                primary_key=True))
    channel_id = cast(Optional[Puid], Column(UUID(as_uuid=True)))
    developer_id = cast(Optional[Puid], Column(UUID(as_uuid=True)))
    instance_id = cast(Optional[str], Column(NonNullString))
    network_id = cast(Optional[Puid], Column(UUID(as_uuid=True)))
    project_id = cast(Optional[Puid], Column(UUID(as_uuid=True)))
    surfer_id = cast(Optional[Puid], Column(UUID(as_uuid=True)))

    category = cast(str, Column(Integer))
    code = cast(int, Column(Integer))
    level = cast(str, Column(Integer))
    meta = cast(str, Column(NonNullString))
    msg = cast(str, Column(NonNullString))
    platform = cast(str, Column(Integer))
    version = cast(str, Column(NonNullString))

    def as_dict(self):
        return {
            **self.time_dict(),
            'plog_id': self.plog_id,
            'channel_id': self.channel_id,
            'developer_id': self.developer_id,
            'instance_id': self.instance_id,
            'network_id': self.network_id,
            'project_id': self.project_id,
            'surfer_id': self.surfer_id,

            'category': self.category,
            'code': self.code,
            'meta': self.meta,
            'msg': self.msg,
            'platform': self.platform,
            'version': self.version,
        }
