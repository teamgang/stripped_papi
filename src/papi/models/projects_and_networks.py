from sqlalchemy import BigInteger, Boolean, Column, ForeignKey, Integer, text
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.conf import conf
from papi.core import Puid
from papi.core.files import delete_file, save_thumb_b64
from papi.models.generics import Base, NonNullString, TimesMixin


def make_thumb_network_abbrev(network_id: Puid):
    return conf.make_image_url(f'img/netabbrev/{network_id}.webp');


def make_thumb_network(network_id: Puid):
    return conf.make_image_url(f'img/net/{network_id}.webp');


class Project(Base, TimesMixin):
    __tablename__ = 'projects'
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   nullable=False,
                                   primary_key=True,
                                   server_default=text("uuid_generate_v4()"),
                                   unique=True,
                                   ))
    plan_id = cast(int, Column(Integer,
                               ForeignKey('plans.plan_id'),
                               nullable=False))
    plan_id_next = cast(int, Column(Integer,
                                    ForeignKey('plans.plan_id'),
                                    nullable=False))
    creator_id = cast(Puid, Column(UUID(as_uuid=True), nullable=False))
    credit = cast(int, Column(Integer, nullable=False))
    debt = cast(int, Column(Integer, nullable=False))
    default_plastic_id = cast(str, Column(NonNullString, nullable=False))
    first_period = cast(int, Column(BigInteger, nullable=False))
    is_autopay_enabled = cast(bool, Column(Boolean, nullable=False))
    is_bill_overdue = cast(bool, Column(Boolean, nullable=False))
    is_closed = cast(bool, Column(Boolean, nullable=False))
    is_in_arrears = cast(bool, Column(Boolean, nullable=False))
    name = cast(str, Column(NonNullString, nullable=False))
    notes = cast(str, Column(NonNullString, nullable=False))
    time_last_bill_issued = cast(int, Column(BigInteger, nullable=False))
    time_last_paid_off = cast(int, Column(BigInteger, nullable=False))
    time_plan_expires = cast(int, Column(BigInteger, nullable=False))

    custom_bought = cast(int, Column(Integer, nullable=False))
    custom_sent = cast(int, Column(Integer, nullable=False))
    limitless_bought = cast(int, Column(Integer, nullable=False))
    limitless_sent = cast(int, Column(Integer, nullable=False))
    static_bought = cast(int, Column(Integer, nullable=False))
    static_sent = cast(int, Column(Integer, nullable=False))
    subscribers = cast(int, Column(Integer, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'creator_id': self.creator_id,
            'credit': self.credit,
            'debt': self.debt,
            'default_plastic_id': self.default_plastic_id,
            'first_period': self.first_period,
            'is_autopay_enabled': self.is_autopay_enabled,
            'is_bill_overdue': self.is_bill_overdue,
            'is_closed': self.is_closed,
            'is_in_arrears': self.is_in_arrears,
            'name': self.name,
            'notes': self.notes,
            'plan_id': self.plan_id,
            'plan_id_next': self.plan_id_next,
            'project_id': self.project_id,
            'time_last_paid_off': self.time_last_paid_off,
            'time_plan_expires': self.time_plan_expires,

            'custom_bought': self.custom_bought,
            'custom_sent': self.custom_sent,
            'limitless_bought': self.limitless_bought,
            'limitless_sent': self.limitless_sent,
            'static_bought': self.static_bought,
            'static_sent': self.static_sent,
            'subscribers': self.subscribers,
        }

    # def is_in_arrears(self) -> bool:
        # # only check if marked overdue
        # if self.is_bill_overdue:
            # # only check if a payoff has not been attempted
            # if self.time_last_bill_issued > self.time_last_paid_off: #type:ignore
                # # give seven days grace period
                # seven_days = 1000 * 60 * 60 * 24 * 7
                # if (nowstamp() - self.time_last_bill_issued) > seven_days: #type:ignore
                    # return True
        # return False

    def custom_remaining(self) -> int:
        return self.custom_bought - self.custom_sent

    def limitless_remaining(self) -> int:
        return self.limitless_bought - self.limitless_sent

    def static_remaining(self) -> int:
        return self.static_bought - self.static_sent

    def max_credit_to_apply(self, price: int) -> int:
        if self.credit > price:
            return price
        else:
            return self.credit


class ProjectHasDeveloper(Base, TimesMixin):
    __tablename__ = 'project_has_developers'
    developer_id = cast(Puid,
                        Column(UUID(as_uuid=True),
                               ForeignKey('developers.developer_id'),
                               nullable=False,
                               primary_key=True,
                               server_default=text("uuid_generate_v4()"),
                               ))
    project_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             ForeignKey('projects.project_id'),
                             nullable=False,
                             primary_key=True,
                             server_default=text("uuid_generate_v4()"),
                             ))
    privilege = cast(str, Column(NonNullString, nullable=False))


class Network(Base, TimesMixin):
    __tablename__ = 'networks'
    network_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             nullable=False,
                             primary_key=True,
                             server_default=text("uuid_generate_v4()"),
                             unique=True,
                             ))
    abbrev = cast(str, Column(NonNullString, nullable=False))
    color_primary = cast(str, Column(NonNullString, nullable=False))
    color_secondary = cast(str, Column(NonNullString, nullable=False))
    description = cast(str, Column(NonNullString, nullable=False))
    name = cast(str, Column(NonNullString, nullable=False))
    notes = cast(str, Column(NonNullString, nullable=False))
    page1 = cast(str, Column(NonNullString, nullable=False))
    page2 = cast(str, Column(NonNullString, nullable=False))
    project_id = cast(Puid,
                      Column(UUID(as_uuid=True),
                             ForeignKey('projects.project_id'),
                             index=True,
                             nullable=False,
                             server_default=text("uuid_generate_v4()"),
                             ))
    route = cast(str, Column(NonNullString, nullable=False))
    route_lower = cast(str, Column(NonNullString, nullable=False))
    thumb_selected = cast(str, Column(NonNullString, nullable=False))
    subscriber_count = cast(int, Column(Integer, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'abbrev': self.abbrev,
            'color_primary': self.color_primary,
            'color_secondary': self.color_secondary,
            'description': self.description,
            'name': self.name,
            'network_id': str(self.network_id),
            'notes': self.notes,
            'page1': self.page1,
            'page2': self.page2,
            'project_id': str(self.project_id),
            'route': self.route,
            'thumb_abbrev': self.thumb_abbrev,
            'thumb_network': self.thumb_network,
            'thumb_selected': self.thumb_selected,
        }

    def save_abbrev(self, thumb_b64: str):
        pathpart: str = f'/img/netabbrev/{self.network_id}.webp'
        filepath: str = f'{conf.hosted_path}{pathpart}'
        delete_file(filepath)
        save_thumb_b64(filepath, thumb_b64)

    @property
    def thumb_abbrev(self) -> str:
        return make_thumb_network_abbrev(self.network_id)

    @property
    def thumb_network(self) -> str:
        return make_thumb_network(self.network_id)

    # this is also done on front-end - not good
    @property
    def thumb_selected_url(self) -> str:
        if self.thumb_selected == 'abbrev':
            return self.thumb_abbrev
        elif self.thumb_selected == 'net':
            return self.thumb_network
        else:
            return self.thumb_selected
