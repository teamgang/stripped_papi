from sqlalchemy import BigInteger, Column
from typing import cast

from papi.core import nowstamp
from papi.models.generics import Base, NonNullString, TimesMixin


class Instance(Base, TimesMixin):
    __tablename__ = 'instances'
    instance_id = cast(str, Column(NonNullString,
                                   nullable=False,
                                   primary_key=True))
    platform = cast(str, Column(NonNullString))
    time_last_used = cast(int, Column(BigInteger,
                                      nullable=False,
                                      default=nowstamp,
                                      ))

    def as_dict(self):
        return {
            **self.time_dict(),
            'instance_id': self.instance_id,
            'platform': self.platform,
            'time_last_used': self.time_last_used,
        }
