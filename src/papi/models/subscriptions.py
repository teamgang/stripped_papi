from sqlalchemy import and_, Boolean, Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Session
from typing import cast, List

from papi.core import Puid
from papi.models.channels import Channel
from papi.models.generics import Base, NonNullString, TimesMixin


def is_id_a_uuid(id):
    if not id:
        RuntimeError(f'missing s_or_i_id for ${id}')
    return (len(str(id).split('-')) == 5 and len(str(id)) < 40)


class NetworkSub(Base, TimesMixin):
    __tablename__ = 'network_subscriptions'
    network_sub_id = cast(Puid, Column(UUID(as_uuid=True),
                                       nullable=False,
                                       primary_key=True,
                                       ))
    network_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('networks.network_id'),
                                   nullable=False,
                                   ))
    surfer_or_instance_id = cast(str, Column(NonNullString, nullable=False))
    is_silenced = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'network_sub_id': self.network_sub_id,
            'network_id': self.network_id,
            'surfer_or_instance_id': self.surfer_or_instance_id,
            'is_silenced': self.is_silenced,
        }

    @property
    def is_linked_to_instance(self):
        return not is_id_a_uuid(self.surfer_or_instance_id)

    @property
    def is_linked_to_surfer(self):
        return is_id_a_uuid(self.surfer_or_instance_id)

    @property
    def s_or_i_id(self):
        return self.surfer_or_instance_id


class ChannelSub(Base, TimesMixin):
    __tablename__ = 'channel_subscriptions'
    network_sub_id = cast(Puid, Column(UUID(as_uuid=True),
                                       nullable=False,
                                       primary_key=True,
                                       ))
    channel_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('channels.channel_id'),
                                   nullable=False,
                                   primary_key=True,
                                   ))
    network_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('networks.network_id'),
                                   nullable=False,
                                   ))
    is_silenced = cast(bool, Column(Boolean, nullable=False))
    is_subscribed = cast(bool, Column(Boolean, nullable=False))
    surfer_sound = cast(str, Column(NonNullString, nullable=False))
    use_surfer_sound = cast(bool, Column(Boolean, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'channel_id': self.channel_id,
            'network_id': self.network_id,
            'network_sub_id': self.network_sub_id,
            'is_silenced': self.is_silenced,
            'is_subscribed': self.is_subscribed,
            'surfer_sound': self.surfer_sound,
            'use_surfer_sound': self.use_surfer_sound,
        }


def get_network_sub_with_ids(network_id: Puid,
                             instance_id: str,
                             surfer_id: str) -> NetworkSub:
    network_sub: NetworkSub = db.query(NetworkSub).filter( #type:ignore
        and_(NetworkSub.network_id == network_id,
            NetworkSub.surfer_or_instance_id == instance_id)).first()

    if not network_sub and surfer_id:
        network_sub: NetworkSub = db.query(NetworkSub).filter( #type:ignore
            and_(NetworkSub.network_id == network_id,
                 NetworkSub.surfer_of_instance_id == surfer_id)).first()
    return network_sub


def get_and_fix_channel_subs_of_network_with_ids(network_sub: NetworkSub,
                                                 db: Session) -> List:
    channel_subs: ChannelSub = db.query(ChannelSub).filter( #type:ignore
        ChannelSub.network_sub_id == network_sub.network_sub_id).all()

    all_channels = db.query(Channel).filter( #type:ignore
        Channel.network_id == network_sub.network_id).all()

    channel_map = {}
    for chan in all_channels:
        channel_map[chan.channel_id] = chan

    def make_packaged(sub: ChannelSub):
        return {
            'channel_id': sub.channel_id,
            'network_id': sub.network_id,
            'network_sub_id': sub.network_sub_id,

            'is_silenced': sub.is_silenced,
            'is_subscribed': sub.is_subscribed,
            'surfer_sound': sub.surfer_sound,
            'use_surfer_sound': sub.use_surfer_sound,
            'channel': channel_map[sub.channel_id].as_dict(),
        }

    collection = []
    for sub in channel_subs:
        collection.append(make_packaged(sub))
        if sub.channel_id in channel_map:
            del channel_map[sub.channel_id]
        else:
            # this channel no longer exists, so we can delete the subscription
            db.delete(sub)

    #Here we create any channels which the user did not already have
    for remaining_channel in channel_map.values():
        new_sub = make_default_channel_sub(
            network_sub.network_sub_id,
            remaining_channel.channel_id,
            remaining_channel.network_id)
        db.add(new_sub)
        collection.append(make_packaged(new_sub))

    return collection


def make_default_channel_sub(network_sub_id: Puid,
                             channel_id: Puid,
                             network_id: Puid) -> ChannelSub:
    channel_sub = ChannelSub()
    channel_sub.channel_id = channel_id
    channel_sub.network_id = network_id
    channel_sub.network_sub_id = network_sub_id
    channel_sub.is_silenced = False
    channel_sub.is_subscribed = False
    channel_sub.surfer_sound = ''
    channel_sub.use_surfer_sound = False
    return channel_sub
