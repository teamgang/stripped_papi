from sqlalchemy import BigInteger, Boolean, Column
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import in_x_days, nowstamp, Puid
from papi.models.generics import Base, NonNullString, TimesMixin


class Surfer(Base, TimesMixin):
    __tablename__ = 'surfers'
    surfer_id = cast(Puid, Column(UUID(as_uuid=True),
                                  primary_key=True,
                                  nullable=False,
                                  unique=True,
                                  ))
    email = cast(str, Column(NonNullString, nullable=False))
    google_id = cast(str, Column(NonNullString, default=''))
    is_email_verified = cast(bool, Column(Boolean, nullable=False))
    password = cast(str, Column(NonNullString, nullable=False))
    stripe_customer_id = cast(str, Column(NonNullString))

    def as_dict(self):
        return {
            **self.time_dict(),
            'surfer_id': self.surfer_id,
            'email': self.email,
        }


class SurferSession(Base, TimesMixin):
    __tablename__ = 'surfer_sessions'
    instance_id = cast(str, Column(NonNullString,
                                   nullable=False,
                                   primary_key=True))
    surfer_id = cast(Puid, Column(UUID(as_uuid=True),
                                  nullable=False,
                                  ))
    is_apple_session = cast(bool, Column(Boolean, nullable=False))
    is_google_session = cast(bool, Column(Boolean, nullable=False))
    session_jwt = cast(str, Column(NonNullString, nullable=False))
    platform = cast(str, Column(NonNullString, nullable=False))
    refresh_token_apple = cast(str, Column(NonNullString, nullable=False))

    time_expires = cast(int, Column(BigInteger,
                                    nullable=False,
                                    default=nowstamp,
                                    ))
    time_last_used = cast(int, Column(BigInteger,
                                      nullable=False,
                                      default=nowstamp,
                                      ))
    time_next_24h_check = cast(int, Column(BigInteger,
                                           nullable=False,
                                           default=nowstamp,
                                           ))

    def as_dict(self):
        return {
            **self.time_dict(),
            'instance_id': self.instance_id,
            'surfer_id': self.surfer_id,
            'platform': self.platform,
            'time_expires': self.time_expires,
            'time_last_used': self.time_last_used,
        }

    def needs_24_check(self):
        if (nowstamp() - self.time_next_24h_check) > 0:
            return True
        return False

    def set_next_24h_check(self):
        self.time_next_24h_check = in_x_days(1).timestamp()

    def get_client_id(self):
        if self.is_google_session == 'io.pushboi.and':
            return 'io.pushboi.and'
        elif self.is_apple_session == 'io.pushboi.ios':
            return 'io.pushboi.ios'
        else:
            return ''


class SurferChangeEmailRequest(Base, TimesMixin):
    __tablename__ = 'surfer_change_email_requests'
    token = cast(str, Column(NonNullString,
                             nullable=False,
                             primary_key=True,
                             ))
    surfer_id = cast(Puid, Column(UUID(as_uuid=True), nullable=False))
    is_applied = cast(bool, Column(Boolean, nullable=False))
    is_consumed = cast(bool, Column(Boolean, nullable=False))
    new_email = cast(str, Column(NonNullString))
    old_email = cast(str, Column(NonNullString))
    time_expires = cast(int, Column(BigInteger, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'surfer_id': self.surfer_id,
            'is_applied': self.is_applied,
            'is_consumed': self.is_consumed,
            'new_email': self.new_email,
            'old_email': self.old_email,
            'token': self.token,
            'time_expires': self.time_expires,
        }


class SurferChangePasswordRequest(Base, TimesMixin):
    __tablename__ = 'surfer_change_password_requests'
    token = cast(str, Column(NonNullString,
                                       nullable=False,
                                       primary_key=True,
                                       ))
    surfer_id = cast(Puid, Column(UUID(as_uuid=True), nullable=False))
    is_consumed = cast(bool, Column(Boolean, nullable=False))
    time_expires = cast(int, Column(BigInteger, nullable=False))

    def as_dict(self):
        return {
            **self.time_dict(),
            'surfer_id': self.surfer_id,
            'is_consumed': self.is_consumed,
            'token': self.token,
            'time_expires': self.time_expires,
        }
