from sqlalchemy import BigInteger, Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from typing import cast

from papi.core import get_period_now, period_from_ym, Puid
from papi.models.generics import Base, NonNullString, TimesMixin

STAT_EDIT_STATUS_CONSUMED = 'consumed'
STAT_EDIT_STATUS_FREE = 'free'
STAT_EDIT_STATUS_PURCHASED = 'purchased'


class NetworkToken(Base, TimesMixin):
    __tablename__ = 'network_tokens'
    network_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('networks.network_id'),
                                   nullable=False,
                                   primary_key=True,
                                   ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    token = cast(str, Column(NonNullString,
                             nullable=False,
                             primary_key=True,
                             ))

    def as_dict(self):
        return {
            **self.time_dict(),
            'network_id': self.network_id,
            'token': self.token,
        }


class ChannelToken(Base, TimesMixin):
    __tablename__ = 'channel_tokens'
    channel_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('channels.channel_id'),
                                   nullable=False,
                                   primary_key=True,
                                   ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    token = cast(str, Column(NonNullString,
                                       nullable=False,
                                       primary_key=True,
                                       ))

    def as_dict(self):
        return {
            **self.time_dict(),
            'channel_id': self.channel_id,
            'token': self.token,
        }


class StatEditToken(Base, TimesMixin):
    __tablename__ = 'stat_edit_tokens'
    stat_edit_token_id = cast(Puid, Column(UUID(as_uuid=True),
                                           nullable=False,
                                           primary_key=True,
                                           ))
    project_id = cast(Puid, Column(UUID(as_uuid=True),
                                   ForeignKey('projects.project_id'),
                                   nullable=False,
                                   ))
    channel_id = cast(Puid, Column(UUID(as_uuid=True)))
    status = cast(str, Column(NonNullString,
                              nullable=False,
                              ))
    time_activated = cast(int, Column(BigInteger,
                                      nullable=False,
                                      default=0,
                                      ))
    time_expires = cast(int, Column(BigInteger,
                                    nullable=False,
                                    default=0,
                                    ))

    def as_dict(self):
        return {
            **self.time_dict(),
            'stat_edit_token_id': self.stat_edit_token_id,
            'project_id': self.project_id,
            'channel_id': self.channel_id,
            'status': self.status,
            'time_activated': self.time_activated,
            'time_expires': self.time_expires,
        }

    def is_activated_in_period(self, year: int, month: int) -> bool:
        return get_period_now() == period_from_ym(year, month)
