from .alert import router as alert_router
from .boi import router as boi_router
from .bill import router as bill_router

from .change_boi import router as change_boi_router
from .change_channel import router as change_channel_router
from .change_developer import router as change_developer_router
from .change_network import router as change_network_router
from .change_project import router as change_project_router
from .change_stat import router as change_stat_router
from .change_surfer import router as change_surfer_router

from .charge import router as charge_router
from .channel import router as channel_router
from .developer import router as developer_router
from .network import router as network_router
from .index import router as index_router
from .legal import router as legal_router
from .metric_boi import router as metric_boi_router

from .notifications import router as notifications_router
from .payment import router as payment_router
from .plan import router as plan_router
from .project import router as project_router

from .stripe import router as stripe_router
from .sub_add import router as sub_add_router
from .sub_channel import router as sub_channel_router
from .sub_network import router as sub_network_router
from .surfer import router as surfer_router
from .surfer_apple import router as surfer_apple_router
from .surfer_google import router as surfer_google_router

from .tokens_channel import router as tokens_channel_router
from .tokens_network import router as tokens_network_router
from .tokens_static_edit import router as tokens_static_edit_router
from .zroutes import router as zroutes_router
from papi.conf import conf


def attach_routes(app):
    if conf.papi_env == 'dev' or conf.papi_env == 'local':
        app.include_router(zroutes_router, prefix='/v1')

    app.include_router(index_router)

    app.include_router(alert_router, prefix='/v1')
    app.include_router(boi_router, prefix='/v1')
    app.include_router(bill_router, prefix='/v1')

    app.include_router(charge_router, prefix='/v1')
    app.include_router(change_boi_router, prefix='/v1')
    app.include_router(change_channel_router, prefix='/v1')
    app.include_router(change_developer_router, prefix='/v1')
    app.include_router(change_network_router, prefix='/v1')
    app.include_router(change_project_router, prefix='/v1')
    app.include_router(change_stat_router, prefix='/v1')
    app.include_router(change_surfer_router, prefix='/v1')

    app.include_router(channel_router, prefix='/v1')
    app.include_router(developer_router, prefix='/v1')
    app.include_router(legal_router, prefix='/v1')
    app.include_router(metric_boi_router, prefix='/v1')

    app.include_router(network_router, prefix='/v1')
    app.include_router(notifications_router, prefix='/v1')
    app.include_router(payment_router, prefix='/v1')
    app.include_router(plan_router, prefix='/v1')
    app.include_router(project_router, prefix='/v1')

    app.include_router(stripe_router, prefix='/v1')
    app.include_router(sub_add_router, prefix='/v1')
    app.include_router(sub_channel_router, prefix='/v1')
    app.include_router(sub_network_router, prefix='/v1')
    app.include_router(surfer_router, prefix='/v1')
    app.include_router(surfer_apple_router, prefix='/v1')
    app.include_router(surfer_google_router, prefix='/v1')

    app.include_router(tokens_channel_router, prefix='/v1')
    app.include_router(tokens_network_router, prefix='/v1')
    app.include_router(tokens_static_edit_router, prefix='/v1')

    # app.include_router(admins, prefix='/v1')
