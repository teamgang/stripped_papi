from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.core import (
    AlertIdIn,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.models import Alert

router = APIRouter()


@router.post('/alert/markViewed', response_model=EmptySchema)
async def alert_mark_viewed(request: Request,
                            jin: AlertIdIn,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    alert: Alert = db.query(Alert).filter( #type:ignore
        Alert.alert_id == jin.alert_id).first()
    rb.exit_if_policy_fails(await Policies.user_owns_alert(gibs, alert))

    alert.was_viewed = True
    db.add(alert)
    db_commit(db, rb)
    return rb.build_response()
