from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiSchema,
    BillIdIn,
    db_commit,
    get_db,
    Gibs,
    Policies,
    ProjectIdIn,
    Puid,
    ResponseBuilder,
)
from papi.models import (
    ATTEMPT_STATUS_PROCESSED,
    Bill,
    BILL_STATUS_PAID,
    Payment,
)

router = APIRouter()


class BillSchema(ApiSchema):
    bill_id: Puid
    bill_type: str
    charge_ids: List[str]
    credit_applied: int
    credit_overflow: int
    currency: str
    notes: str
    period: int
    price: int
    project_id: Puid
    status: str
    time_created: int
    was_autopay_used: bool

class BillCollectionSchema(ApiSchema):
    collection: List[Optional[BillSchema]]

@router.post('/bill/get.byId', response_model=BillSchema)
async def bill_get_by_id(request: Request,
                         jin: BillIdIn,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    bill = db.query(Bill).filter(
        Bill.bill_id == jin.bill_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, bill.project_id, db))

    rb.set_fields_with_dict(bill.as_dict())
    return rb.build_response()


@router.post('/bills/get.byProjectId', response_model=BillCollectionSchema)
async def bills_get_by_project_id(request: Request,
                                  jin: ProjectIdIn,
                                  db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    bills = db.query(Bill).filter(Bill.project_id == jin.project_id
        ).order_by(Bill.period.desc()).all() #type:ignore

    collection = []
    for bill in bills:
        if bill.status != BILL_STATUS_PAID:
            payment = db.query(Payment).filter(
                Payment.bill_id == bill.bill_id).first()  # type: ignore
            if payment.status == ATTEMPT_STATUS_PROCESSED:
                bill.status = BILL_STATUS_PAID
                db.add(bill)
                db_commit(db, rb)

        collection.append(bill.as_dict())

    rb.set_field('collection', collection)
    return rb.build_response()
