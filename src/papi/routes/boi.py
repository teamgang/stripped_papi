from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List, Optional, Union
import uuid

from papi.conf import conf
from papi.core import (
    ApiSchema,
    BoiIdIn,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder,
    timestamps_from_ym,
)
from papi.core.files import save_thumb_b64
from papi.models import Boi, Channel

router = APIRouter()


class BoiSchema(ApiSchema):
    boi_id: Union[str, uuid.UUID]
    channel_id: Union[str, uuid.UUID]
    network_id: Union[str, uuid.UUID]
    project_id: Union[str, uuid.UUID]
    body: str

    cm_created: int
    cm_dismissed: int
    cm_displayed: int
    cm_tapped: int
    cm_viewed_in_app: int
    count_failure: int
    count_success: int
    count_recipient_instance_ids: int

    error: str
    is_hidden: bool
    layout: str
    notes: str
    period: int
    plan_id: int
    plasticity: str
    status: str
    thumb_selected: str
    title: str
    token: str
    url: str

    # from channel
    channel_abbrev: str
    channel_name: str
    channel_thumb_abbrev: str
    channel_thumb_channel: str
    channel_thumb_selected: str
    color_primary: str
    color_secondary: str
    time_created: int
    time_updated: int

class BoiCollectionSchema(ApiSchema):
    collection: List[Optional[BoiSchema]]

@router.post('/boi/get.byId', response_model=BoiSchema)
async def boi_get_by_id(jin: BoiIdIn,
                        db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    boi: Boi = db.query(Boi).filter(
        Boi.boi_id == jin.boi_id).first()  # type: ignore

    channel: Channel = db.query(Channel).filter(
        Channel.channel_id == boi.channel_id).first()  # type: ignore
    item = boi.as_dict(channel)

    rb.set_fields_with_dict(item)
    return rb.build_response()


class BoiUploadThumbIn(ApiSchema):
    thumb_b64: str

class BoiUploadThumbOut(ApiSchema):
    thumb_selected: str

@router.post('/boi/upload.thumb', response_model=BoiUploadThumbOut)
async def boi_upload_thumb(request: Request,
                           jin: BoiUploadThumbIn):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_a_developer(gibs))

    new_uuid = uuid.uuid4()

    pathpart: str = f'img/boi/{new_uuid}.webp'
    filepath: str = f'{conf.hosted_path}/{pathpart}'
    save_thumb_b64(filepath, jin.thumb_b64)

    rb.set_field('thumb_selected', conf.make_image_url(pathpart))
    return rb.build_response()


class ChannelIdSchema(ApiSchema):
    channel_id: str

@router.post('/bois/get.byChannelId', response_model=BoiCollectionSchema)
async def bois_get_by_channel_id(jin: ChannelIdSchema,
                                 db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    bois = db.query(Boi).filter(
        Boi.channel_id == jin.channel_id).all()  # type: ignore

    collection = []
    channel:Channel = db.query(Channel).filter(
        Channel.channel_id == jin.channel_id).first()  # type: ignore
    for boi in bois:
        item = boi.as_dict(channel)
        collection.append(item)

    rb.set_field('collection', collection)
    return rb.build_response()


class NetworkIdSchema(ApiSchema):
    network_id: str

@router.post('/bois/get.byNetworkId', response_model=BoiCollectionSchema)
async def bois_get_by_network_id(jin: NetworkIdSchema,
                                 db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    bois = db.query(Boi).filter(
        Boi.network_id == jin.network_id).all()  # type: ignore

    channel_cache = {}
    collection = []

    for boi in bois:
        if boi.channel_id in channel_cache:
            channel = channel_cache[boi.channel_id]
        else:
            channel: Channel = db.query(Channel).filter(
                Channel.channel_id == boi.channel_id).first()  # type: ignore
            channel_cache[boi.channel_id] = channel

        item = boi.as_dict(channel)
        collection.append(item)

    rb.set_field('collection', collection)
    return rb.build_response()


class NetworkIdPeriodIn(ApiSchema):
    month: int
    network_id: str
    year: int

@router.post('/bois/get.byNetworkIdAndPeriod',
             response_model=BoiCollectionSchema)
async def bois_get_by_network_id_and_period(jin: NetworkIdPeriodIn,
                                            db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    start, end = timestamps_from_ym(jin.year, jin.month)

    bois = db.query(Boi).filter( #type: ignore
            and_(Boi.network_id == jin.network_id,
                 Boi.time_created.between(start, end))).all()

    channel_cache = {}
    collection = []

    for boi in bois:
        if boi.channel_id in channel_cache:
            channel = channel_cache[boi.channel_id]
        else:
            channel: Channel = db.query(Channel).filter(
                Channel.channel_id == boi.channel_id).first()  # type: ignore
            channel_cache[boi.channel_id] = channel

        item = boi.as_dict(channel)
        collection.append(item)

    rb.set_field('collection', collection)
    return rb.build_response()


class ProjectIdPeriodIn(ApiSchema):
    month: int
    project_id: str
    year: int

@router.post('/bois/get.byProjectIdAndPeriod',
             response_model=BoiCollectionSchema)
async def bois_get_by_project_id_and_period(request: Request,
                                            jin: ProjectIdPeriodIn,
                                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    if jin.year == 0 or jin.month == 0:
        rb.set_field('collection', [])
        return rb.build_response()

    start, end = timestamps_from_ym(jin.year, jin.month)

    bois = db.query(Boi).filter( #type: ignore
            and_(Boi.project_id == jin.project_id,
                 Boi.time_created.between(start, end))).all()

    channel_cache = {}
    collection = []

    for boi in bois:
        if boi.channel_id in channel_cache:
            channel = channel_cache[boi.channel_id]
        else:
            channel: Channel = db.query(Channel).filter(
                Channel.channel_id == boi.channel_id).first()  # type: ignore
            channel_cache[boi.channel_id] = channel

        item = boi.as_dict(channel)
        collection.append(item)

    rb.set_field('collection', collection)
    return rb.build_response()
