from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    Puid,
    ResponseBuilder)
from papi.models import Boi

router = APIRouter()


class BoiHideIn(ApiSchema):
    boi_id: Puid
    is_hidden: bool

@router.post('/boi/hide', response_model=EmptySchema)
async def boi_hide(request: Request,
                   jin: BoiHideIn,
                   db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    boi: Boi = db.query(Boi).filter( #type:ignore
        Boi.boi_id == jin.boi_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, boi.project_id, db))

    boi.is_hidden = jin.is_hidden
    db.add(boi)
    db_commit(db, rb)
    return rb.build_response()
