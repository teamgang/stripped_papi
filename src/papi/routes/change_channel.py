from fastapi import APIRouter, Depends, Request
import os
from sqlalchemy.orm import Session

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.core.errors import ImageOver300KbError
from papi.core.files import save_thumb_b64
from papi.core.network import get_image_size
from papi.models import Channel
from papi.validation import ValidateChannel

router = APIRouter()


class ChannelChangeAbbrevIn(ApiSchema):
    abbrev: str
    channel_id: str
    thumb_b64: str

@router.post('/channel/change.abbrev', response_model=EmptySchema)
async def channel_change_abbrev(request: Request,
                                jin: ChannelChangeAbbrevIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.abbrev = jin.abbrev
    db.add(channel)
    db_commit(db, rb)
    channel.save_abbrev(jin.thumb_b64)
    return rb.build_response()


class ChannelChangeDescriptionIn(ApiSchema):
    description: str
    channel_id: str

@router.post('/channel/change.description', response_model=EmptySchema)
async def channel_change_description(request: Request,
                                     jin: ChannelChangeDescriptionIn,
                                     db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.description = jin.description
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class ChannelChangeNameIn(ApiSchema):
    name: str
    channel_id: str

@router.post('/channel/change.name', response_model=EmptySchema)
async def channel_change_name(request: Request,
                              jin: ChannelChangeNameIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    validation = ValidateChannel.check_name(jin.name)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    channel.name = jin.name
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class ChannelChangeNotesIn(ApiSchema):
    notes: str
    channel_id: str

@router.post('/channel/change.notes', response_model=EmptySchema)
async def channel_change_notes(request: Request,
                               jin: ChannelChangeNotesIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.notes = jin.notes
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class ChannelChangeRouteIn(ApiSchema):
    route: str
    channel_id: str

@router.post('/channel/change.route', response_model=EmptySchema)
async def channel_change_route(request: Request,
                               jin: ChannelChangeRouteIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    validation = ValidateChannel.check_route(jin.route, channel.project_id, db)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    channel.route = jin.route
    channel.route_lower = jin.route.lower()
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class ChannelChangeColorIn(ApiSchema):
    color: str
    channel_id: str
    thumb_b64: str

@router.post('/channel/change.colorPrimary', response_model=EmptySchema)
async def channel_change_color_primary(request: Request,
                                       jin: ChannelChangeColorIn,
                                       db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.color_primary = jin.color
    db.add(channel)
    db_commit(db, rb)
    channel.save_abbrev(jin.thumb_b64)
    return rb.build_response()


@router.post('/channel/change.colorSecondary', response_model=EmptySchema)
async def channel_change_color_secondary(request: Request,
                                         jin: ChannelChangeColorIn,
                                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.color_secondary = jin.color
    db.add(channel)
    db_commit(db, rb)
    channel.save_abbrev(jin.thumb_b64)
    return rb.build_response()


class ChannelChangeThumbIn(ApiSchema):
    channel_id: str
    is_abbrev: bool
    is_external: bool
    is_upload: bool
    thumb_b64: str
    thumb_selected: str

@router.post('/channel/change.thumb', response_model=EmptySchema)
async def channel_change_thumb(request: Request,
                               jin: ChannelChangeThumbIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    size_error = ImageOver300KbError.copy()

    if jin.is_abbrev:
        channel.thumb_selected = 'abbrev'
    elif jin.is_external:
        size = int(get_image_size(jin.thumb_selected) / 1024)
        print(f'external image size: {size}')
        if size > 299:
            size_error.append_to_msg(f'This image is {size} KB.')
            rb.add_error(size_error)
            return rb.build_response()
        channel.thumb_selected = jin.thumb_selected
    elif jin.is_upload:
        pathpart: str = f'img/chan/{channel.channel_id}.webp'
        filepath: str = f'{conf.hosted_path}/{pathpart}'
        save_thumb_b64(filepath, jin.thumb_b64)
        size = int(os.path.getsize(filepath) / 1024)
        print(f'upload size: {size}')
        print(filepath)
        if size > 299:
            size_error.append_to_msg(f'This image is {size} KB.')
            rb.add_error(size_error)
            return rb.build_response()
        channel.thumb_selected = conf.make_image_url(pathpart)

    print(channel.thumb_selected)
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()
