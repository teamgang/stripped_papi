from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    EmailIn,
    EmptySchema,
    get_db,
    Gibs,
    in_x_minutes,
    nowstamp,
    plog,
    Policies,
    RequestChangeSchema,
    ResponseBuilder,
    TokenIn,
)
from papi.core.errors import (
    AccountNotFoundError,
    LoginToChangeEmailError,
    TokenAlreadyAppliedError,
    TokenExpiredError,
)

from papi.models import (
    Developer,
    DevChangeEmailRequest,
    DevChangePasswordRequest,
)
from papi.utils import (
    generate_token,
    send_email_simple,
    send_change_email_verification,
)

router = APIRouter()


@router.post('/developer/change.email.request', response_model=EmptySchema)
async def developer_change_email_request(request: Request,
                                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    developer: Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == gibs.developer_id).first()

    token = generate_token()
    url = conf.make_url(f'developer/change.email.link?token={token}')

    plog.info(f'email change request for {developer.developer_id}, url:\n{url}')

    change_req = DevChangeEmailRequest()
    change_req.token = token
    change_req.is_applied = False
    change_req.is_consumed = False
    change_req.developer_id = developer.developer_id
    change_req.time_expires = int(in_x_minutes(15).timestamp())
    db.add(change_req)
    db_commit(db, rb)

    subject = 'Email Change Requested'
    msg = ('Click the link below to change your email for your developer '
           'PushBoi account. You must be logged in for the link to work.'
           f'\n\n{url}')
    send_email_simple(conf.email_noreply, developer.email, subject, msg)

    return rb.build_response()


@router.post('/developer/change.email.request.validate',
             response_model=RequestChangeSchema)
async def developer_change_email_request_validate(
        request: Request,
        jin: TokenIn,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    developer: Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == gibs.developer_id).first()

    change_req: DevChangeEmailRequest = db.query(DevChangeEmailRequest).filter( #type:ignore
            DevChangeEmailRequest.token == jin.token).first()

    if str(developer.developer_id) != gibs.developer_id:
        rb.add_error(LoginToChangeEmailError)
        return rb.build_response()

    rb.set_field('is_consumed', change_req.is_consumed)
    rb.set_field('developer_id', change_req.developer_id)
    rb.set_field('time_expires', change_req.time_expires)
    return rb.build_response()


class ChangeEmailIn(ApiSchema):
    email: str
    token: str

@router.post('/developer/change.email', response_model=EmptySchema)
async def developer_change_email(request: Request,
                                jin: ChangeEmailIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    developer:Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == gibs.developer_id).first()

    change_req: DevChangeEmailRequest = db.query(DevChangeEmailRequest).filter( #type:ignore
        DevChangeEmailRequest.token == jin.token).first()

    if str(developer.developer_id) != gibs.developer_id:
        rb.add_error(LoginToChangeEmailError)
        return rb.build_response()

    if str(change_req.developer_id) != gibs.developer_id:
        rb.add_error(LoginToChangeEmailError)
        return rb.build_response()

    if change_req.is_consumed \
            or nowstamp() > change_req.time_expires:
        rb.add_error(TokenExpiredError)
        return rb.build_response()

    change_req.is_consumed = True
    change_req.new_email = jin.email
    change_req.old_email = developer.email

    send_change_email_verification(jin.email, jin.token, 'developer')

    db.add(change_req)
    db.add(developer)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/developer/change.email.validate.new.link',
             response_model=EmptySchema)
async def developer_change_email_validate_new_link(
        request: Request,
        jin: TokenIn,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    change_req: DevChangeEmailRequest = db.query(DevChangeEmailRequest).filter( #type:ignore
        DevChangeEmailRequest.token == jin.token).first()

    if change_req.is_applied:
        rb.add_error(TokenAlreadyAppliedError)
        return rb.build_response()

    developer: Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == change_req.developer_id).first()

    change_req.is_applied = True
    developer.email = change_req.new_email

    db.add(change_req)
    db.add(developer)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/developer/change.password.request', response_model=EmptySchema)
async def developer_change_password_request(jin: EmailIn,
                                            db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    developer = db.query(Developer).filter( #type:ignore
        Developer.email == jin.email).first()

    if not developer:
        rb.add_error(AccountNotFoundError)
        return rb.build_response()

    token = generate_token()
    url = conf.make_url(f'developer/change.password.link?token={token}')

    plog.info(f'password change request for {developer.developer_id}, url:\n{url}')

    change_req = DevChangePasswordRequest()
    change_req.token = token
    change_req.is_consumed = False
    change_req.developer_id = developer.developer_id
    change_req.time_expires = int(in_x_minutes(15).timestamp())

    db.add(change_req)
    db_commit(db, rb)

    subject = 'Password Change Requested'
    msg = ('Click the link below to change your password for your developer '
           'PushBoi account. You must be logged in for the link to work.'
           f'\n\n{url}')
    send_email_simple(conf.email_noreply, developer.email, subject, msg)

    return rb.build_response()


@router.post('/developer/change.password.request.validate',
             response_model=RequestChangeSchema)
async def developer_change_password_request_validate(
        jin: TokenIn,
        db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    change_req: DevChangePasswordRequest = db.query(
        DevChangePasswordRequest).filter( #type:ignore
            DevChangePasswordRequest.token == jin.token).first()

    rb.set_field('is_consumed', change_req.is_consumed)
    rb.set_field('developer_id', change_req.developer_id)
    rb.set_field('time_expires', change_req.time_expires)
    return rb.build_response()


class ChangePasswordIn(ApiSchema):
    password: str
    token: str

@router.post('/developer/change.password', response_model=EmptySchema)
async def developer_change_password(request: Request,
                                    jin: ChangePasswordIn,
                                    db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    developer: Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == gibs.developer_id).first()

    change_req: DevChangePasswordRequest = db.query(
        DevChangePasswordRequest).filter( #type:ignore
            DevChangePasswordRequest.token == jin.token).first()

    if change_req.is_consumed \
            or nowstamp() > change_req.time_expires:
        rb.add_error(TokenExpiredError)
        return rb.build_response()

    change_req.is_consumed = True
    developer.password = conf.pwds.hash(jin.password)

    db.add(change_req)
    db.add(developer)
    db_commit(db, rb)
    return rb.build_response()


class ChangePhoneIn(ApiSchema):
    phone: str

@router.post('/developer/change.phone', response_model=EmptySchema)
async def developer_change_phone(request: Request,
                                 jin: ChangePhoneIn,
                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    developer: Developer = db.query(Developer).filter( #type:ignore
        Developer.developer_id == gibs.developer_id).first()

    developer.phone = jin.phone
    db.add(developer)
    db_commit(db, rb)
    return rb.build_response()
