from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.core.files import save_thumb_b64
from papi.models import Network
from papi.validation import ValidateNetwork

router = APIRouter()


class NetworkChangeAbbrevIn(ApiSchema):
    abbrev: str
    network_id: str
    thumb_b64: str

@router.post('/network/change.abbrev', response_model=EmptySchema)
async def network_change_abbrev(request: Request,
                                jin: NetworkChangeAbbrevIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    network.abbrev = jin.abbrev
    db.add(network)
    db_commit(db, rb)
    network.save_abbrev(jin.thumb_b64)
    return rb.build_response()


class NetworkChangeDescriptionIn(ApiSchema):
    description: str
    network_id: str

@router.post('/network/change.description', response_model=EmptySchema)
async def network_change_description(request: Request,
                                     jin: NetworkChangeDescriptionIn,
                                     db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    network.description = jin.description
    db.add(network)
    db_commit(db, rb)
    return rb.build_response()


class NetworkChangeNameIn(ApiSchema):
    name: str
    network_id: str

@router.post('/network/change.name', response_model=EmptySchema)
async def network_change_name(request: Request,
                              jin: NetworkChangeNameIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    validation = ValidateNetwork.check_name(jin.name)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    network.name = jin.name
    db.add(network)
    db_commit(db, rb)
    return rb.build_response()


class NetworkChangeNotesIn(ApiSchema):
    notes: str
    network_id: str

@router.post('/network/change.notes', response_model=EmptySchema)
async def network_change_notes(request: Request,
                               jin: NetworkChangeNotesIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    network.notes = jin.notes
    db.add(network)
    db_commit(db, rb)
    return rb.build_response()


class NetworkChangeRouteIn(ApiSchema):
    route: str
    network_id: str

@router.post('/network/change.route', response_model=EmptySchema)
async def network_change_route(request: Request,
                               jin: NetworkChangeRouteIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    validation = ValidateNetwork.check_route(jin.route, db)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    network.route = jin.route
    network.route_lower = jin.route.lower()
    db.add(network)
    db_commit(db, rb)
    return rb.build_response()


class NetworkChangeColorIn(ApiSchema):
    color: str
    network_id: str
    thumb_b64: str

@router.post('/network/change.colorPrimary', response_model=EmptySchema)
async def network_change_color_primary(request: Request,
                                       jin: NetworkChangeColorIn,
                                       db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    network.color_primary = jin.color
    db.add(network)
    db_commit(db, rb)
    network.save_abbrev(jin.thumb_b64)
    return rb.build_response()


@router.post('/network/change.colorSecondary', response_model=EmptySchema)
async def network_change_color_secondary(request: Request,
                                         jin: NetworkChangeColorIn,
                                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    network.color_secondary = jin.color
    db.add(network)
    db_commit(db, rb)
    network.save_abbrev(jin.thumb_b64)
    return rb.build_response()


class NetworkChangeThumbIn(ApiSchema):
    network_id: str
    is_abbrev: bool
    is_external: bool
    is_upload: bool
    thumb_b64: str
    thumb_selected: str

@router.post('/network/change.thumb', response_model=EmptySchema)
async def network_change_thumb(request: Request,
                               jin: NetworkChangeThumbIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    #NOTE: network image is not sent to notifications, so no errors
    if jin.is_abbrev:
        network.thumb_selected = 'abbrev'
    elif jin.is_external:
        network.thumb_selected = jin.thumb_selected
    elif jin.is_upload:
        pathpart: str = f'img/net/{network.network_id}.webp'
        filepath: str = f'{conf.hosted_path}/{pathpart}'
        network.thumb_selected = conf.make_image_url(pathpart)
        save_thumb_b64(filepath, jin.thumb_b64)

    db.add(network)
    db_commit(db, rb)
    return rb.build_response()
