from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse
from sqlalchemy.orm import Session

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    plog,
    Policies,
    ResponseBuilder)
from papi.models import Developer, Project
from papi.payments import stripe
from papi.routes.stripe import AgreementMustBeAcceptedError

router = APIRouter()


class ProjectChangeDefaultPlasticIdIn(ApiSchema):
    default_plastic_id: str
    project_id: str
    terms_accepted_version: str

#NOTE: this route sets the autopay method
@router.post('/project/change.defaultPlasticId', response_model=EmptySchema)
async def project_change_default_plastic_id(
        request: Request,
        jin: ProjectChangeDefaultPlasticIdIn,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    if not jin.terms_accepted_version:
        rb.add_error(AgreementMustBeAcceptedError)
        return rb.build_response()

    project = db.query(Project).filter( #type:ignore
        Project.project_id == jin.project_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, project.project_id, db))

    #TODO: check that plastic id exists for developer on stripe
    # developer = db.query(Developer).filter(
        # Developer.developer_id == gibs.developer_id).first()  # type: ignore

    project.default_plastic_id = jin.default_plastic_id
    project.is_autopay_enabled = True
    db.add(project)
    db_commit(db, rb)
    return rb.build_response()


class ProjectAddPlasticOut(ApiSchema):
    stripe_url: str

@router.post('/project/plastic.add', response_model=ProjectAddPlasticOut)
async def project_plastic_add(request: Request,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    developer = db.query(Developer).filter(
        Developer.developer_id == gibs.developer_id).first()  # type: ignore

    pre = f'{request.base_url}v1/project/change.defaultCard'
    success_url = f'{pre}.success'
    cancel_url = f'{pre}.cancel'

    session = stripe.checkout.Session.create(
        customer=developer.stripe_customer_id,
        payment_method_types=['card'],
        mode='setup',
        success_url=success_url,
        cancel_url=cancel_url,
    )

    rb.set_field('stripe_url', session.url)
    return rb.build_response()


class ProjectPlasticDeleteIn(ApiSchema):
    plastic_id: str
    project_id: str

@router.post('/project/plastic.delete', response_model=EmptySchema)
async def project_plastic_delete(request: Request,
                                 jin: ProjectPlasticDeleteIn,
                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    project = db.query(Project).filter(
        Project.project_id == jin.project_id).first()  # type: ignore

    if project.default_plastic_id == jin.plastic_id:
        project.default_plastic_id = ''
        project.is_autopay_enabled = False
        db.add(project)
        db_commit(db, rb)

    return rb.build_response()


@router.get('/project/change.defaultCard.success', response_class=HTMLResponse)
async def payment_method_change_success():
    msg = 'Your card has been saved and can now be used with PushBoi.'
    return HTMLResponse(msg)


@router.get('/project/change.defaultCard.cancel', response_class=HTMLResponse)
async def payment_method_change_cancel():
    msg = 'Your card was not saved.'
    return HTMLResponse(msg)


class ProjectChangeNameIn(ApiSchema):
    name: str
    project_id: str

@router.post('/project/change.name', response_model=EmptySchema)
async def project_change_name(request: Request,
                              jin: ProjectChangeNameIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    project = db.query(Project).filter( #type:ignore
        Project.project_id == jin.project_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, project.project_id, db))

    project.name = jin.name
    db.add(project)
    db_commit(db, rb)
    return rb.build_response()


class ProjectChangeNotesIn(ApiSchema):
    notes: str
    project_id: str

@router.post('/project/change.notes', response_model=EmptySchema)
async def project_change_notes(request: Request,
                               jin: ProjectChangeNotesIn,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    project = db.query(Project).filter( #type:ignore
        Project.project_id == jin.project_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, project.project_id, db))

    project.notes = jin.notes
    db.add(project)
    db_commit(db, rb)
    return rb.build_response()


class ProjectChangeAutopayIn(ApiSchema):
    enable_autopay: bool
    project_id: str

@router.post('/project/change.autopay', response_model=EmptySchema)
async def project_change_autopay(request: Request,
                                 jin: ProjectChangeAutopayIn,
                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    project = db.query(Project).filter( #type:ignore
        Project.project_id == jin.project_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, project.project_id, db))

    project.is_autopay_enabled = jin.enable_autopay
    db.add(project)
    db_commit(db, rb)
    return rb.build_response()
