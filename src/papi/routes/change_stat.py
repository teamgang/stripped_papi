from fastapi import APIRouter, Depends, Request
import os
from sqlalchemy.orm import Session

from papi.conf import conf
from papi.core import (
    ApiError,
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    nowstamp,
    Policies,
    ResponseBuilder)
from papi.core.errors import ImageOver300KbError
from papi.core.files import save_thumb_b64
from papi.core.network import get_image_size
from papi.models import Channel

router = APIRouter()


class StatChangeAbbrevIn(ApiSchema):
    channel_id: str
    static_abbrev: str

NoTokenSpentError = ApiError(
    code=4405,
    msg='A token must be spent')

@router.post('/stat/change.abbrev', response_model=EmptySchema)
async def stat_change_abbrev(request: Request,
                             jin: StatChangeAbbrevIn,
                             db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if nowstamp() >  channel.static_edit_expires:
        rb.add_error(NoTokenSpentError)
        return rb.build_response()

    channel.static_abbrev = jin.static_abbrev
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class StatChangeBodyIn(ApiSchema):
    channel_id: str
    static_body: str

@router.post('/stat/change.body', response_model=EmptySchema)
async def stat_change_body(request: Request,
                           jin: StatChangeBodyIn,
                           db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if nowstamp() >  channel.static_edit_expires:
        rb.add_error(NoTokenSpentError)
        return rb.build_response()

    channel.static_body = jin.static_body
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class StatChangeUrlIn(ApiSchema):
    channel_id: str
    static_url: str

@router.post('/stat/change.url', response_model=EmptySchema)
async def stat_change_url(request: Request,
                          jin: StatChangeUrlIn,
                          db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if nowstamp() >  channel.static_edit_expires:
        rb.add_error(NoTokenSpentError)
        return rb.build_response()

    channel.static_url = jin.static_url
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class StatChangeTitleIn(ApiSchema):
    channel_id: str
    static_title: str

@router.post('/stat/change.title', response_model=EmptySchema)
async def stat_change_title(request: Request,
                            jin: StatChangeTitleIn,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if nowstamp() >  channel.static_edit_expires:
        rb.add_error(NoTokenSpentError)
        return rb.build_response()

    channel.static_title = jin.static_title
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class StatChangeThumbUrlIn(ApiSchema):
    channel_id: str
    is_abbrev: bool
    is_using_chan: bool
    is_external: bool
    is_upload: bool
    thumb_b64: str
    thumb_selected: str

@router.post('/stat/change.thumbUrl', response_model=EmptySchema)
async def stat_change_thumb_selected(request: Request,
                                jin: StatChangeThumbUrlIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if nowstamp() >  channel.static_edit_expires:
        rb.add_error(NoTokenSpentError)
        return rb.build_response()

    size_error = ImageOver300KbError.copy()

    if jin.is_abbrev:
        channel.static_thumb_selected = 'abbrev'
    if jin.is_using_chan:
        channel.static_thumb_selected = 'chan'
    elif jin.is_external:
        size = int(get_image_size(jin.thumb_selected) / 1024)
        print(f'external image size: {size}')
        if size > 299:
            size_error.append_to_msg(f'This image is {size} KB.')
            rb.add_error(size_error)
            return rb.build_response()
        channel.static_thumb_selected = jin.thumb_selected
    elif jin.is_upload:
        pathpart: str = f'img/chanstatic/{channel.channel_id}.webp'
        filepath: str = f'{conf.hosted_path}/{pathpart}'
        save_thumb_b64(filepath, jin.thumb_b64)
        size = int(os.path.getsize(filepath) / 1024)
        if size > 299:
            size_error.append_to_msg(f'This image is {size} KB.')
            rb.add_error(size_error)
            return rb.build_response()
        channel.static_thumb_selected = conf.make_image_url(pathpart)

    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()
