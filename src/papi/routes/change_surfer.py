from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    EmailIn,
    EmptySchema,
    get_db,
    Gibs,
    in_x_minutes,
    nowstamp,
    plog,
    Policies,
    RequestChangeSchema,
    ResponseBuilder,
    TokenIn,
)
from papi.core.errors import (
    AccountNotFoundError,
    TokenAlreadyAppliedError,
    TokenExpiredError,
)

from papi.models import (
    Surfer,
    SurferChangeEmailRequest,
    SurferChangePasswordRequest,
    SurferSession,
)
from papi.utils import (
    generate_token,
    send_email_simple,
    send_change_email_verification,
)

router = APIRouter()


@router.post('/surfer/change.email.request', response_model=EmptySchema)
async def surfer_change_email_request(request: Request,
                                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    surfer: Surfer = db.query(Surfer).filter( #type:ignore
        Surfer.surfer_id == gibs.surfer_id).first()

    token = generate_token()
    url = conf.make_url(f'surfer/change.email.link?token={token}')

    plog.info(f'email change request for {surfer.surfer_id}, url:\n{url}')

    change_req = SurferChangeEmailRequest()
    change_req.token = token
    change_req.is_applied = False
    change_req.is_consumed = False
    change_req.surfer_id = surfer.surfer_id
    change_req.time_expires = int(in_x_minutes(15).timestamp())
    db.add(change_req)
    db_commit(db, rb)

    subject = 'Email Change Requested'
    msg = ('Click the link below to change your email for your account '
           'PushBoi account. You must be logged in for the link to work.'
           f'\n\n{url}')
    send_email_simple(conf.email_noreply, surfer.email, subject, msg)

    return rb.build_response()


@router.post('/surfer/change.email.request.validate',
             response_model=RequestChangeSchema)
async def surfer_change_email_request_validate(
        jin: TokenIn,
        db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    change_req: SurferChangeEmailRequest = db.query(
        SurferChangeEmailRequest).filter( #type:ignore
            SurferChangeEmailRequest.token == jin.token).first()

    rb.set_field('is_consumed', change_req.is_consumed)
    rb.set_field('surfer_id', change_req.surfer_id)
    rb.set_field('time_expires', change_req.time_expires)
    return rb.build_response()


class ChangeEmailIn(ApiSchema):
    email: str
    token: str

@router.post('/surfer/change.email', response_model=EmptySchema)
async def surfer_change_email(request: Request,
                              jin: ChangeEmailIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    change_req: SurferChangeEmailRequest = db.query(
        SurferChangeEmailRequest).filter( #type:ignore
            SurferChangeEmailRequest.token == jin.token).first()

    surfer: Surfer = db.query(Surfer).filter( #type:ignore
        Surfer.surfer_id == change_req.surfer_id).first()

    if change_req.is_consumed \
            or nowstamp() > change_req.time_expires:
        rb.add_error(TokenExpiredError)
        return rb.build_response()

    change_req.is_consumed = True
    change_req.new_email = jin.email
    change_req.old_email = surfer.email

    send_change_email_verification(jin.email, jin.token, 'surfer')

    db.add(change_req)
    db.add(surfer)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/surfer/change.email.validate.new.link',
             response_model=EmptySchema)
async def surfer_change_email_validate_new_link(jin: TokenIn,
                                                db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    change_req: SurferChangeEmailRequest = db.query(
        SurferChangeEmailRequest).filter( #type:ignore
            SurferChangeEmailRequest.token == jin.token).first()

    if change_req.is_applied:
        rb.add_error(TokenAlreadyAppliedError)
        return rb.build_response()

    surfer: Surfer = db.query(Surfer).filter( #type:ignore
        Surfer.surfer_id == change_req.surfer_id).first()

    change_req.is_applied = True
    surfer.email = change_req.new_email

    db.add(change_req)
    db.add(surfer)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/surfer/change.password.request', response_model=EmptySchema)
async def surfer_change_password_request(jin: EmailIn,
                                         db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    surfer: Surfer = db.query(Surfer).filter( #type:ignore
        Surfer.email == jin.email).first()

    if not surfer:
        rb.add_error(AccountNotFoundError)
        return rb.build_response()

    token = generate_token()
    url = conf.make_url(f'surfer/change.password.link?token={token}')

    plog.info(f'password change request for {surfer.surfer_id}, url:\n{url}')

    change_req = SurferChangePasswordRequest()
    change_req.token = token
    change_req.is_consumed = False
    change_req.surfer_id = surfer.surfer_id
    change_req.time_expires = int(in_x_minutes(15).timestamp())

    db.add(change_req)
    db_commit(db, rb)

    subject = 'Password Change Requested'
    msg = ('Click the link below to change your password for your account '
           'PushBoi account. You must be logged in for the link to work.'
           f'\n\n{url}')
    send_email_simple(conf.email_noreply, surfer.email, subject, msg)

    return rb.build_response()


@router.post('/surfer/change.password.request.validate',
             response_model=RequestChangeSchema)
async def surfer_change_password_request_validate(
        jin: TokenIn,
        db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    change_req: SurferChangePasswordRequest = db.query(
        SurferChangePasswordRequest).filter( #type:ignore
            SurferChangePasswordRequest.token == jin.token).first()

    rb.set_field('is_consumed', change_req.is_consumed)
    rb.set_field('surfer_id', change_req.surfer_id)
    rb.set_field('time_expires', change_req.time_expires)
    return rb.build_response()


class ChangePasswordIn(ApiSchema):
    password: str
    token: str

@router.post('/surfer/change.password', response_model=EmptySchema)
async def surfer_change_password(request: Request,
                                    jin: ChangePasswordIn,
                                    db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    surfer: Surfer = db.query(Surfer).filter( #type:ignore
        Surfer.surfer_id == gibs.surfer_id).first()

    change_req: SurferChangePasswordRequest = db.query(SurferChangePasswordRequest).filter( #type:ignore
        SurferChangePasswordRequest.token == jin.token).first()

    if change_req.is_consumed \
            or nowstamp() > change_req.time_expires:
        rb.add_error(TokenExpiredError)
        return rb.build_response()

    change_req.is_consumed = True
    surfer.password = conf.pwds.hash(jin.password)

    sessions = db.query(SurferSession).filter(
        SurferSession.surfer_id == surfer.surfer_id).all()  # type: ignore

    for session in sessions:
        db.delete(session)
    db_commit(db, rb)

    db.add(change_req)
    db.add(surfer)
    db_commit(db, rb)
    return rb.build_response()
