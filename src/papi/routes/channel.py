from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List, Optional
import uuid

from papi.core import (
    ApiSchema,
    ChannelIdIn,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    NetworkIdIn,
    Policies,
    Puid,
    ResponseBuilder)
from papi.models import (
    Channel,
    Network,
    TASK_META_NEW_CHANNEL,
    TASK_META_CHANNEL_DELETED,
)
from papi.queues import publish_task, Task
from papi.validation import ValidateChannel

router = APIRouter()


def createEmptyChannelOut():
    return ChannelOut(
        abbrev='',
        channel_id='',
        color_primary='',
        color_secondary='',
        description='',
        name='',
        network_id='',
        notes='',
        page1='',
        route='',
        static_body='',
        static_edit_expires=0,
        static_thumb_selected='',
        static_title='',
        static_url='',
        thumb_abbrev='',
        thumb_channel='',
        thumb_selected='',
        time_created=0,
        time_updated=0,
    )

class ChannelOut(ApiSchema):
    channel_id: Puid
    abbrev: str
    color_primary: str
    color_secondary: str
    description: str
    name: str
    network_id: Puid
    notes: str
    page1: str
    route: str
    static_body: str
    static_edit_expires: int
    static_thumb_selected: str
    static_title: str
    static_url: str
    thumb_abbrev: str
    thumb_channel: str
    thumb_selected: str
    time_created: int
    time_updated: int

class ChannelCollectionSchema(ApiSchema):
    collection: List[Optional[ChannelOut]]

@router.post('/channels/get.byNetworkId',
             response_model=ChannelCollectionSchema)
async def channels_get_by_network_id(jin: NetworkIdIn,
                                     db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    channels = db.query(Channel).filter( #type:ignore
            Channel.network_id == jin.network_id).all()
    collection = [c.as_dict() for c in channels]

    rb.set_field('collection', collection)
    return rb.build_response()


class ChannelCreateSchema(ApiSchema):
    abbrev: str
    color_primary: str
    color_secondary: str
    description: str
    name: str
    network_id: Puid
    notes: str
    route: str
    static_body: str
    static_title: str
    static_url: str
    thumb_b64: str

@router.post('/channel/create', response_model=EmptySchema)
async def channel_create(request: Request,
                         jin: ChannelCreateSchema,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    channel = Channel()
    channel.channel_id = str(uuid.uuid4())
    channel.network_id = jin.network_id
    channel.project_id = network.project_id
    channel.abbrev = jin.abbrev
    channel.color_primary = jin.color_primary
    channel.color_secondary = jin.color_secondary
    channel.description = jin.description
    channel.name = jin.name
    channel.notes = jin.notes
    channel.route = jin.route
    channel.route_lower = jin.route.lower()
    channel.static_body = jin.static_body
    channel.static_title = jin.static_title
    channel.static_url = jin.static_url
    channel.static_edit_expires = 0

    channel.static_thumb_selected = 'abbrev'
    channel.thumb_selected = 'abbrev'

    validation = ValidateChannel.all_fields_obj(channel, db)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    channel.save_abbrev(jin.thumb_b64)

    db.add(channel)
    db_commit(db, rb)

    # send to alerter queue
    task: Task = Task()
    task.channel_id = str(channel.channel_id)
    task.network_id = str(channel.network_id)
    task.meta = TASK_META_NEW_CHANNEL
    publish_task(task, 'for_alerter')
    return rb.build_response()


@router.post('/channel/delete', response_model=EmptySchema)
async def channel_delete(request: Request,
                         jin: ChannelIdIn,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    channel.is_deleted = True
    db.add(channel)
    db_commit(db, rb)

    # send to alerter queue
    task: Task = Task()
    task.channel_id = str(channel.channel_id)
    task.network_id = str(channel.network_id)
    task.meta = TASK_META_CHANNEL_DELETED
    publish_task(task, 'for_alerter')
    return rb.build_response()


@router.post('/channel/get.byId', response_model=ChannelOut)
async def channel_get_by_id(jin: ChannelIdIn,
                            db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    rb.exit_if_missing_requirements([channel])
    rb.set_fields_with_dict(channel.as_dict())
    return rb.build_response()


class ChannelRouteIn(ApiSchema):
    channel_route: str
    network_route: str

@router.post('/channel/get.byRoute', response_model=ChannelOut)
async def channel_get_by_route(jin: ChannelRouteIn,
                               db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    network: Network = db.query(Network).filter( #type:ignore
        Network.route_lower == jin.network_route.lower()).first()

    channel: Channel = db.query(Channel).filter( #type:ignore
        and_(Channel.route_lower == jin.channel_route.lower(),
             Channel.network_id == network.network_id)).first()
    # rb.exit_if_missing_requirements([network])

    if channel:
        rb.set_fields_with_dict(channel.as_dict())
    else:
        no_channel = createEmptyChannelOut()
        rb.set_fields_with_dict(dict(no_channel))

    return rb.build_response()
