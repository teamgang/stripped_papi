from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiSchema,
    BillIdIn,
    ChargeIdIn,
    get_db,
    Gibs,
    Policies,
    Puid,
    ResponseBuilder,
    timestamps_from_period,
)
from papi.models import Bill, Charge

router = APIRouter()


class ChargeSchema(ApiSchema):
    charge_id: Puid
    boi_id: Puid
    charge_type: str
    currency: str
    period: int
    plan_id: int
    price: int
    project_id: Puid
    time_created: int
    units: int

class ChargeCollectionSchema(ApiSchema):
    collection: List[Optional[ChargeSchema]]

@router.post('/charge/get.byId', response_model=ChargeSchema)
async def charge_get_by_id(request: Request,
                         jin: ChargeIdIn,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    charge: Charge = db.query(Charge).filter(
        Charge.charge_id == jin.charge_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, charge.project_id, db))

    rb.set_fields_with_dict(charge.as_dict())
    return rb.build_response()


@router.post('/charges/get.byBillId', response_model=ChargeCollectionSchema)
async def charges_get_by_bill_id(request: Request,
                                 jin: BillIdIn,
                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    bill: Bill = db.query(Bill).filter(
        Bill.bill_id == jin.bill_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, bill.project_id, db))

    start, end = timestamps_from_period(bill.period)
    charges: List[Charge] = db.query(Charge).filter( #type:ignore
        and_(Charge.project_id == bill.project_id,
             Charge.time_created.between(start, end))).all()

    collection = []
    for charge in charges:
        collection.append(charge.as_dict())

    rb.set_field('collection', collection)
    return rb.build_response()
