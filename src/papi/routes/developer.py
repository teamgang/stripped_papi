from fastapi import APIRouter, Depends, Request
from pydantic import Field
from sqlalchemy.orm import Session
import uuid

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    create_developer_jwt,
    DeveloperIdIn,
    EmptySchema,
    get_db,
    Gibs,
    JwtRefreshOut,
    Policies,
    Puid,
    ResponseBuilder,
)
from papi.core.errors import (
    EmailRegisteredError,
    LoginFailedError,
    EmailNotVerifiedError,
)
from papi.models import Developer
from papi.payments import stripe
from papi.utils import send_email_verification

router = APIRouter()


class EmailPasswordSchema(ApiSchema):
    email: str = Field(
        min_length=5,
        max_length=320,
        #TODO: regex
        # regex=,
    )
    password: str = Field(
        min_length=4,
        max_length=320,
    )

class DeveloperRegisterOut(ApiSchema):
    developer_id: Puid
    new_jwt: str

class DeveloperSignInOut(ApiSchema):
    developer_id: Puid
    stripe_customer_id: Puid
    new_jwt: str

@router.post('/developer/register.withEmail', response_model=EmptySchema)
async def developer_register_with_email(jin: EmailPasswordSchema,
                                        db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    check_developer: Developer = db.query(Developer).filter(
        Developer.email == jin.email).first()  # type: ignore

    developer = Developer()
    if check_developer:
        rb.add_error(EmailRegisteredError)
        return rb.build_response()
    else:
        developer.developer_id = uuid.uuid4()
        developer.is_email_verified = False
        developer.email = jin.email
        developer.password = conf.pwds.hash(jin.password)
        developer.phone = ''

    customer = stripe.Customer.create(
        email=developer.email,
        metadata={
            'developer_id': developer.developer_id,
        }
    )

    developer.stripe_customer_id = customer.id
    db.add(developer)
    db_commit(db, rb)
    send_email_verification(developer.email,
                            developer.developer_id,
                            'developer')
    return rb.build_response()


@router.post('/developer/signIn.withEmail', response_model=DeveloperSignInOut)
async def developer_sign_in_with_email(jin: EmailPasswordSchema,
                                       db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    developer: Developer = db.query(Developer).filter(
        Developer.email == jin.email).first()  # type: ignore

    if not developer:
        rb.add_error(LoginFailedError)
        return rb.build_response()
    else:
        if not developer.is_email_verified:
            rb.add_error(EmailNotVerifiedError)
            return rb.build_response()

        if conf.pwds.verify(jin.password, developer.password):
            rb.set_field('new_jwt', create_developer_jwt(
                developer.developer_id))
            rb.set_field('developer_id', str(developer.developer_id))
            rb.set_field('stripe_customer_id', str(developer.stripe_customer_id))
        else:
            rb.add_error(LoginFailedError)
    return rb.build_response()


class DeveloperOut(ApiSchema):
    developer_id: Puid
    email: str
    phone: str

@router.post('/developer/get.byId', response_model=DeveloperOut)
async def developer_get_by_id(request: Request,
                              jin: DeveloperIdIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    developer: Developer = db.query(Developer).filter(
        Developer.developer_id == jin.developer_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.is_user_this_developer(gibs, developer))

    rb.set_fields_with_dict(developer.as_dict())
    return rb.build_response()


@router.post('/developer/signOut', response_model=EmptySchema)
async def developer_sign_out():
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    return rb.build_response()


@router.post('/developer/jwt.refresh', response_model=JwtRefreshOut)
async def developer_jwt_refresh():
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    # NOTE: developers do not have session_jwt, so by setting new_jwt to '' we
    # force on a signout on the app end
    rb.set_field('new_jwt', '')
    return rb.build_response()


@router.post('/developer/email.verify', response_model=EmptySchema)
async def developer_email_verify(jin: DeveloperIdIn,
                                 db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    developer: Developer = db.query(Developer).filter(
        Developer.developer_id == jin.developer_id).first()  # type: ignore

    developer.is_email_verified = True
    db.add(developer)
    db_commit(db, rb)
    return rb.build_response()
