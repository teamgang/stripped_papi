from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.models import Flag

router = APIRouter()


class FlagIn(ApiSchema):
    instance_id: str
    category: str
    msg: str
    boi_id: str

@router.post('/flag', response_model=EmptySchema)
async def flag_content(request: Request,
                       jin: FlagIn,
                       db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    flag = Flag()
    flag.surfer_id = gibs.surfer_id
    flag.instance_id = jin.instance_id
    flag.category = jin.category
    flag.msg = jin.msg
    flag.boi_id = jin.boi_id

    db.add(flag)
    db_commit(db, rb)
    return rb.build_response()
