from fastapi import APIRouter, Request

router = APIRouter()

@router.get('/')
async def hello():
    return {'hello': 'hey'}


@router.get('/health')
async def health_check():
    return {'hello': 'hey'}


@router.get('/app')
def read_main(request: Request):
    return {
        'message': 'Hello World',
        'root_path': request.scope.get('root_path'),
    }
