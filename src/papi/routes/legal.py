from fastapi import APIRouter

from papi.conf import conf
from papi.core import (
    ApiSchema,
    Policies,
    ResponseBuilder,
)

router = APIRouter()


class PolicySchema(ApiSchema):
    audience: str
    policy: str
    dated: str
    fulltext: str

@router.get('/user/privacy.latest', response_model=PolicySchema)
async def surfer_legal_privacy():
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    conf.read_surfer_privacy()
    rb.set_fields_with_dict(conf.surfer_privacy.as_json)
    return rb.build_response()


@router.get('/developer/privacy.latest', response_model=PolicySchema)
async def developer_legal_privacy():
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    conf.read_developer_privacy()
    rb.set_fields_with_dict(conf.developer_privacy.as_json)
    return rb.build_response()


@router.get('/developer/terms.latest', response_model=PolicySchema)
async def developer_legal_terms():
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    conf.read_developer_terms()
    rb.set_fields_with_dict(conf.developer_terms.as_json)
    return rb.build_response()
