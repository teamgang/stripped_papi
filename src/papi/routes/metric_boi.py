from fastapi import APIRouter, Depends
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import Union
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Policies,
    ResponseBuilder)
from papi.models import Boi, BoiRecipient

router = APIRouter()


class BoiMetricIn(ApiSchema):
    boi_id: Union[str, uuid.UUID]
    instance_id: str
    report_name: str

@router.post('/boi/metric.report', response_model=EmptySchema)
async def boi_metric_report(jin: BoiMetricIn,
                            db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    boi: Boi = db.query(Boi).filter(
        Boi.boi_id == jin.boi_id).first()  # type: ignore

    boi_recipient: BoiRecipient = db.query(BoiRecipient).filter(
        and_(BoiRecipient.boi_id == jin.boi_id,
             BoiRecipient.instance_id == jin.instance_id)).first()  # type: ignore

    if not boi:
        return rb.build_response()

    if jin.report_name == 'boiCreated':
        boi.cm_created += 1
        if boi_recipient:
            boi_recipient.m_created

    elif jin.report_name == 'boiDismissed':
        boi.cm_dismissed += 1
        if boi_recipient:
            boi_recipient.m_dismissed

    elif jin.report_name == 'boiDisplayed':
        boi.cm_displayed += 1
        if boi_recipient:
            boi_recipient.m_displayed

    elif jin.report_name == 'boiTapped':
        boi.cm_tapped += 1
        if boi_recipient:
            boi_recipient.m_tapped

    elif jin.report_name == 'boiViewedInApp':
        boi.cm_viewed_in_app += 1
        if boi_recipient:
            boi_recipient.m_views_in_app

    else:
        raise RuntimeError(f'report name {jin.report_name} was not found')

    db.add(boi)
    db.add(boi_recipient)
    db_commit(db, rb)

    return rb.build_response()
