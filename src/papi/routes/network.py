from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional, Union
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    NetworkIdIn,
    Policies,
    ProjectIdIn,
    ResponseBuilder)
from papi.models import Network
from papi.models.projects_and_networks import (
    make_thumb_network_abbrev,
    make_thumb_network,
)
from papi.validation import ValidateNetwork

router = APIRouter()


def createEmptyNetworkOut():
    return NetworkOut(
        abbrev='',
        color_primary='',
        color_secondary='',
        description='',
        name='',
        network_id='',
        notes='',
        page1='',
        page2='',
        project_id='',
        route='',
        thumb_abbrev='',
        thumb_network='',
        thumb_selected='',
        time_created=0,
        time_updated=0,
    )

class NetworkOut(ApiSchema):
    abbrev: str
    color_primary: str
    color_secondary: str
    description: str
    name: str
    network_id: Union[str, uuid.UUID]
    notes: str
    page1: str
    page2: str
    project_id: Union[str, uuid.UUID]
    route: str
    thumb_abbrev: str
    thumb_network: str
    thumb_selected: str
    time_created: int
    time_updated: int

class NetworkCollectionSchema(ApiSchema):
    collection: List[Optional[NetworkOut]]

@router.post('/networks/get.byProjectId',
             response_model=NetworkCollectionSchema)
async def networks_get_by_project_id(request: Request,
                                     jin: ProjectIdIn,
                                     db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    networks = db.query(
        Network.abbrev,
        Network.color_primary,
        Network.color_secondary,
        Network.description,
        Network.name,
        Network.network_id,
        Network.notes,
        Network.page1,
        Network.page2,
        Network.project_id,
        Network.route,
        Network.thumb_selected,
        Network.time_created,
        Network.time_updated,
    ).filter( #type:ignore
             Network.project_id == jin.project_id).all()
    collection = [dict(d) for d in networks]
    for network in collection:
        network['thumb_abbrev'] = make_thumb_network_abbrev(
            network['network_id'])
        network['thumb_network'] = make_thumb_network(network['network_id'])

    rb.set_field('collection', collection)
    return rb.build_response()


class NetworkCreateSchema(ApiSchema):
    abbrev: str
    color_primary: str
    color_secondary: str
    description: str
    name: str
    notes: str
    project_id: str
    route: str
    thumb_b64: str

@router.post('/network/create', response_model=EmptySchema)
async def network_create(request: Request,
                         jin: NetworkCreateSchema,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    network = Network()
    network.description = jin.description
    network.abbrev = jin.abbrev
    network.color_primary = jin.color_primary
    network.color_secondary = jin.color_secondary
    network.name = jin.name
    network.network_id = str(uuid.uuid4())
    network.notes = jin.notes
    network.project_id = jin.project_id
    network.route = jin.route
    network.route_lower = jin.route.lower()
    network.thumb_selected = 'abbrev'
    network.subscriber_count = 0

    validation = ValidateNetwork.all_fields_obj(network, db)
    if validation.is_not_valid:
        rb.read_validation(validation)
        return rb.build_response()

    network.save_abbrev(jin.thumb_b64)

    db.add(network)
    db_commit(db, rb)
    return rb.build_response()



@router.post('/network/get.byId', response_model=NetworkOut)
async def network_get_by_id(jin: NetworkIdIn,
                            db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == jin.network_id).first()
    rb.exit_if_missing_requirements([network])
    rb.set_fields_with_dict(network.as_dict())
    return rb.build_response()


class NetworkRouteIn(ApiSchema):
    route: str

@router.post('/network/get.byRoute', response_model=NetworkOut)
async def network_get_by_route(jin: NetworkRouteIn,
                               db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    network: Network = db.query(Network).filter( #type:ignore
        Network.route_lower == jin.route.lower()).first()

    if network:
        rb.set_fields_with_dict(network.as_dict())
    else:
        no_network = createEmptyNetworkOut()
        rb.set_fields_with_dict(dict(no_network))

    return rb.build_response()
