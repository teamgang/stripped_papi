from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from papi.core import (
    ApiError,
    BoiIdIn,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.models import (
    Channel,
    PLASTICITY_CUSTOM,
    PLASTICITY_LIMITLESS,
    PLASTICITY_STATIC,
)
from papi.utils import BoiSenderProcessor, BoiSenderIn

router = APIRouter()


AccessTokenRejectedError = ApiError(
    code=9050,
    msg='Invalid access token or user account.')

BodyTitleThumbMustUseDefaultsError = ApiError(
    code=9060,
    msg='Static notifications cannot customize the body, title, and '
    'thumb_selected must equal \'static\' or be empty.')

BodyTitleThumbRequiredError = ApiError(
    code=9061,
    msg=('Custom and limitless notifications require the body, title, and '
         'thumb selected fields to be set.'))

InvalidPlasticityError = ApiError(
    code=9062,
    msg='Invalid plasticity received.')

ProjectInArrearsError = ApiError(
    code=9053,
    msg='Project cannot send notifications due to missed payment.')

CannotBuyNotificationsError = ApiError(
    code=9054,
    msg='Unable to add notifications to your account.')

@router.post('/dev/notification/send', response_model=BoiIdIn)
async def notification_send(request: Request,
                            jin: BoiSenderIn,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.route == jin.channel_route).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    if jin.plasticity == PLASTICITY_STATIC:
        if jin.body or jin.title:
            rb.add_error(BodyTitleThumbMustUseDefaultsError)
            return rb.build_response()
        if not (jin.thumb_selected != 'static' or jin.thumb_selected != ''):
            rb.add_error(BodyTitleThumbMustUseDefaultsError)
            return rb.build_response()
    elif jin.plasticity == PLASTICITY_CUSTOM or \
            jin.plasticity == PLASTICITY_LIMITLESS:
        if not jin.body or not jin.title:
            rb.add_error(BodyTitleThumbRequiredError)
            return rb.build_response()
    else:
        rb.add_error(InvalidPlasticityError)
        return rb.build_response()


    bsp: BoiSenderProcessor = BoiSenderProcessor(db, jin, gibs.developer_id)

    if not bsp.is_token_valid_or_developer_authorized():
        rb.add_error(AccessTokenRejectedError)
        return rb.build_response()

    if bsp.is_project_in_arrears():
        rb.add_error(ProjectInArrearsError)
        return rb.build_response()

    bsp.send_to_shipper(db, rb)

    rb.set_field('boi_id', str(bsp.boi.boi_id))
    return rb.build_response()
