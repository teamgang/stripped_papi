from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiSchema,
    get_db,
    Gibs,
    Policies,
    PaymentIdIn,
    ProjectIdIn,
    Puid,
    ResponseBuilder,
)
from papi.models import Payment, StripePaymentAttempt

router = APIRouter()


class PaymentSchema(ApiSchema):
    attempt_id: Puid = ''
    payment_id: Puid = ''
    payment_intent_id: Puid = ''

    admin_notes: str
    basket: str
    bill_id: Puid = ''
    charge_ids: List[str] = []
    credit_applied: int
    currency: str
    developer_notes: str = ''
    is_autopay_selected: bool
    plastic_id: str = ''
    project_id: Puid
    silo: str
    status: str
    time_created: int
    total_price: int
    total_price_after_credit: int

class PaymentCollectionSchema(ApiSchema):
    collection: List[Optional[PaymentSchema]]

@router.post('/payment/get.byId', response_model=PaymentSchema)
async def payment_get_by_id(request: Request,
                            jin: PaymentIdIn,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    payment = db.query(Payment).filter(
        Payment.payment_id == jin.payment_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, payment.project_id, db))

    rb.set_fields_with_dict(payment.as_dict())
    return rb.build_response()


@router.post('/payments/get.successful.byProjectId',
             response_model=PaymentCollectionSchema)
async def payments_get_succesful_by_project_id(request: Request,
                                               jin: ProjectIdIn,
                                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    payments = db.query(Payment).filter(Payment.project_id == jin.project_id
        ).order_by(Payment.time_created.desc()).all() #type:ignore

    collection = []
    for payment in payments:
        collection.append(payment.as_dict())

    rb.set_field('collection', collection)
    return rb.build_response()


@router.post('/payments/get.ccAttempts.byProjectId',
             response_model=PaymentCollectionSchema)
async def payments_get_cc_attempts_by_project_id(request: Request,
                                                 jin: ProjectIdIn,
                                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    payments = db.query(StripePaymentAttempt).filter(
        StripePaymentAttempt.project_id == jin.project_id
            ).order_by(StripePaymentAttempt.time_created.desc()).all() #type:ignore

    collection = []
    for payment in payments:
        collection.append(payment.as_dict())

    rb.set_field('collection', collection)
    return rb.build_response()
