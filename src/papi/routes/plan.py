from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiSchema,
    get_db,
    Gibs,
    PlanIdIn,
    Policies,
    ResponseBuilder)
from papi.models import Plan

router = APIRouter()


class PlanSchema(ApiSchema):
    plan_id: int
    add_custom_price: int
    add_custom_units: int
    add_limitless_price: int
    add_limitless_units: int
    add_static_price: int
    add_static_units: int
    bundled_static: int
    bundled_custom: int
    bundled_limitless: int
    monthly_credit: int
    monthly_free_custom: int
    monthly_free_limitless: int
    monthly_free_static: int
    monthly_free_static_edit_tokens: int
    months: int
    price: int
    price_currency: str
    static_edit_token_price: int

    admin_notes: str
    channel_limit: int
    description: str
    is_available: bool
    is_tailored: bool
    rules: str
    subscription_limit: int
    title: str
    time_created: int
    time_updated: int

@router.post('/plan/get.byId', response_model=PlanSchema)
async def plan_get_by_plan_id(request: Request,
                              jin: PlanIdIn,
                              db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    plan = db.query(Plan).filter( #type:ignore
        Plan.plan_id == jin.plan_id).first()

    rb.set_fields_with_dict(plan.as_dict())
    return rb.build_response()


class PlanIdSchema(ApiSchema):
    plan_id: int

class PlanCollectionSchema(ApiSchema):
    collection: List[Optional[PlanSchema]]

@router.post('/plans/get.choices', response_model=PlanCollectionSchema)
async def plans_get_choices(request: Request,
                            jin: PlanIdSchema,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    planChoicesIndices: List[int] = [1, 2, 3, 4]
    if jin.plan_id not in planChoicesIndices:
        planChoicesIndices.append(jin.plan_id)

    plans = db.query(Plan).filter( #type:ignore
        Plan.plan_id.in_(planChoicesIndices)).all() #type:ignore
    collection = [p.as_dict() for p in plans]

    rb.set_field('collection', collection)
    return rb.build_response()
