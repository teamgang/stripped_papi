from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from typing import Optional

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Policies,
    Puid,
    ResponseBuilder,
    set_optional,
)
from papi.models import Plog

router = APIRouter()


class PlogIn(ApiSchema):
    channel_id: Optional[Puid]
    developer_id: Optional[Puid]
    instance_id: Optional[str]
    network_id: Optional[Puid]
    surfer_id: Optional[Puid]
    project_id: Optional[Puid]
    category: str
    code: int
    level: str
    meta: str
    msg: str
    platform: str
    user_type: str
    version: str

@router.post('/plog', response_model=EmptySchema)
async def surfer_legal_privacy(jin: PlogIn,
                               db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    plog =  Plog()
    plog.channel_id = set_optional(jin.channel_id)
    plog.developer_id = set_optional(jin.developer_id)
    plog.instance_id = set_optional(jin.instance_id)
    plog.network_id = set_optional(jin.network_id)
    plog.project_id = set_optional(jin.project_id)
    plog.surfer_id = set_optional(jin.surfer_id)

    plog.category = jin.category
    plog.code = jin.code
    plog.level = jin.level
    plog.meta = jin.meta
    plog.msg = jin.msg
    plog.platform = jin.platform
    plog.user_type = jin.user_type
    plog.version = jin.version

    db.add(plog)
    db_commit(db, rb)
    return rb.build_response()
