from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional, Union
import uuid

from papi.core import (
    ApiError,
    ApiSchema,
    db_commit,
    EmptySchema,
    DeveloperIdIn,
    get_db,
    get_period_now,
    Gibs,
    Policies,
    ProjectIdIn,
    ResponseBuilder)
from papi.models import (
    Plan,
    PlanChangeRequest,
    Project,
    ProjectHasDeveloper,
)

router = APIRouter()

class ProjectCreateSchema(ApiSchema):
    notes: str
    name: str

@router.post('/project/create', response_model=EmptySchema)
async def project_create(request: Request,
                         jin: ProjectCreateSchema,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    project = Project()
    project.project_id = str(uuid.uuid4())
    project.creator_id = gibs.developer_id
    project.first_period = get_period_now()
    project.is_closed = False
    project.name = jin.name
    project.notes = jin.notes

    project.plan_id = 5  # pay as you go plan number
    project.plan_id_next = 5

    project.custom_bought = 0
    project.custom_sent = 0
    project.limitless_bought = 0
    project.limitless_sent = 0
    project.static_bought = 0
    project.static_sent = 0
    project.subscribers = 0

    project.is_autopay_enabled = False
    project.credit = 0
    project.time_last_bill_issued = 0
    project.time_last_paid_off = 0
    project.time_plan_expires = 0

    project_has_developer = ProjectHasDeveloper()
    project_has_developer.developer_id = project.creator_id
    project_has_developer.project_id = str(project.project_id)
    project_has_developer.privilege = 'creator'

    db.add(project)
    db.add(project_has_developer)
    db_commit(db, rb)
    return rb.build_response()


class ProjectSchema(ApiSchema):
    creator_id: Union[str, uuid.UUID]
    credit: int
    name: str
    default_plastic_id: str
    is_autopay_enabled: bool
    is_closed: bool
    name: str
    notes: str
    plan_id: int
    plan_id_next: int
    project_id: uuid.UUID
    time_plan_expires: int
    time_created: int
    time_updated: int

    custom_bought: int
    custom_sent: int
    limitless_bought: int
    limitless_sent: int
    static_bought: int
    static_sent: int
    subscribers: int


class ProjectWideSchema(ProjectSchema):
    developer_id: uuid.UUID
    privilege: str

class ProjectWideCollectionSchema(ApiSchema):
    collection: List[Optional[ProjectWideSchema]]

@router.post('/projects/get.byDeveloperId',
             response_model=ProjectWideCollectionSchema)
async def project_get_by_developer_id(request: Request,
                                      jin: DeveloperIdIn,
                                      db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    projects = db.query(  # type: ignore
        ProjectHasDeveloper.project_id,
        Project.creator_id,
        Project.credit,
        ProjectHasDeveloper.developer_id,
        Project.default_plastic_id,
        Project.is_autopay_enabled,
        Project.is_closed,
        Project.name,
        Project.notes,
        Project.plan_id,
        Project.plan_id_next,
        ProjectHasDeveloper.privilege,
        Project.time_plan_expires,
        Project.time_created,
        Project.time_updated,

        Project.custom_bought,
        Project.custom_sent,
        Project.limitless_bought,
        Project.limitless_sent,
        Project.static_bought,
        Project.static_sent,
        Project.subscribers,
    ).join(Project).filter(
        ProjectHasDeveloper.developer_id == jin.developer_id).all()
    collection = [dict(p) for p in projects]

    rb.set_field('collection', collection)
    return rb.build_response()


@router.post('/project/get.byId', response_model=ProjectSchema)
async def project_get_by_id(request: Request,
                            jin: ProjectIdIn,
                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    project: Project = db.query(Project).filter(
        Project.project_id == jin.project_id).first()  # type: ignore
    rb.exit_if_missing_requirements([project])
    rb.set_fields_with_dict(project.as_dict())
    return rb.build_response()


PlanChangeError = ApiError(
    code=2222,
    msg='Error changing planIdNext')

class ProjectPlanIdNextChangeIn(ApiSchema):
     project_id: uuid.UUID
     plan_id_next: int
     terms_accepted_version: str

@router.post('/project/planIdNext.change', response_model=EmptySchema)
async def project_plan_id_next_change(request: Request,
                                      jin: ProjectPlanIdNextChangeIn,
                                      db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))
    if not jin.terms_accepted_version:
        rb.add_error(PlanChangeError)
        return rb.build_response()

    project: Project = db.query(Project).filter(
        Project.project_id == jin.project_id).first()  # type: ignore

    plan: Plan = db.query(Plan).filter(
        Plan.plan_id == jin.plan_id_next).first()  # type: ignore

    rb.exit_if_missing_requirements([plan, project])

    project.plan_id_next = jin.plan_id_next
    db.add(project)
    db_commit(db, rb)

    pcr = PlanChangeRequest()
    pcr.plan_change_id = str(uuid.uuid4())
    pcr.is_cancellation = False
    pcr.developer_id = gibs.developer_id
    pcr.plan_id_next = jin.plan_id_next
    pcr.project_id = str(jin.project_id)
    pcr.terms_accepted_version = jin.terms_accepted_version
    db.add(pcr)
    db_commit(db, rb)

    return rb.build_response()


@router.post('/project/planIdNext.cancel', response_model=EmptySchema)
async def project_plan_id_next_cancel(request: Request,
                                      jin: ProjectIdIn,
                                      db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    project: Project = db.query(Project).filter(
        Project.project_id == jin.project_id).first()  # type: ignore

    rb.exit_if_missing_requirements([project])

    project.plan_id_next = project.plan_id
    db.add(project)
    db_commit(db, rb)

    pcr = PlanChangeRequest()
    pcr.plan_change_id = str(uuid.uuid4())
    pcr.is_cancellation = True
    pcr.developer_id = gibs.developer_id
    pcr.plan_id_next = project.plan_id
    pcr.project_id = str(jin.project_id)
    pcr.terms_accepted_version = ''
    db.add(pcr)
    db_commit(db, rb)

    return rb.build_response()
