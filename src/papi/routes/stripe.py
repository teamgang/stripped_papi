from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse
from sqlalchemy.orm import Session
from typing import Optional

from papi.core import (
    ApiSchema,
    ApiError,
    get_db,
    Gibs,
    plog,
    Policies,
    ResponseBuilder)
from papi.models import (
    ATTEMPT_STATUS_FAILED,
    ATTEMPT_STATUS_WAITING_ON_STRIPE,
    ATTEMPT_STATUS_WAITING_TO_PROCESS,
    StripePaymentAttempt,
)
from papi.utils.payment_handler import (
    PaymentDetailsHandler,
    PaymentDetailsIn,
)
from papi.utils.payment_attempt import (
    get_and_update_payment_attempt_status_from_intent,
    process_payment_attempt,
)

router = APIRouter()

AgreementMustBeAcceptedError = ApiError(
    code=1090,
    msg='You must accept the terms and conditions.')

PaymentFailedError = ApiError(
    code=1090,
    msg='Payment failed. Try anothe method.')


class StripePayOut(ApiSchema):
    payment_attempt_id: Optional[str]
    stripe_url: Optional[str]
    status: str

@router.post('/stripe/pay', response_model=StripePayOut)
async def stripe_create_session(request: Request,
                                jin: PaymentDetailsIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    pdh = PaymentDetailsHandler(jin, db, gibs, rb)

    pdh.step1_validate()
    if pdh.spa.status == ATTEMPT_STATUS_FAILED:
        pdh.commit_changes()
        rb.add_error(PaymentFailedError)
        return rb.build_response()

    if jin.is_plastic_id_unlisted:
        session = pdh.step2_make_stripe_session_with_unlisted_card(request)

        rb.set_field('stripe_url', session.url)
        rb.set_field('status', 'waitForStripe')
        # print(str(pdh.spa.attempt_id))
        rb.set_field('payment_attempt_id', str(pdh.spa.attempt_id))
        return rb.build_response()

    else:
        session = pdh.step2_make_stripe_session_with_saved_card()

        if pdh.spa.status == ATTEMPT_STATUS_FAILED:
            rb.add_error(PaymentFailedError)
            rb.set_field('status', 'failed')
            return rb.build_response()
        else:
            rb.set_field('status', 'success')
            await process_payment_attempt(pdh.spa.attempt_id, rb, db)
            return rb.build_response()


@router.get('/payment/result', response_class=HTMLResponse)
async def stripe_success(payment_attempt_id: str,
                         db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    status = get_and_update_payment_attempt_status_from_intent(
        payment_attempt_id, rb, db)

    msg = 'Thank you! Your payment is processing.'
    if status >= ATTEMPT_STATUS_WAITING_TO_PROCESS:
        msg = 'Thank you! Your payment was successful.'
        await process_payment_attempt(payment_attempt_id, rb, db)

    elif status == ATTEMPT_STATUS_FAILED:
        msg = 'Your payment failed. Please try again.'

    elif status == ATTEMPT_STATUS_WAITING_ON_STRIPE:
        msg = 'Your payment is still processing.'

    else:
        plog.critical(
            f'unknown payment_attempt.status for attempt_id = {payment_attempt_id}')

    return HTMLResponse(msg)


class PaymentAttemptStatusIn(ApiSchema):
    payment_attempt_id: str

class PaymentAttemptStatusOut(ApiSchema):
    status: int

@router.post('/paymentAttempt/status.getAndHandle',
             response_model=PaymentAttemptStatusOut)
async def is_payment_attempt_successful(request: Request,
                                        jin: PaymentAttemptStatusIn,
                                        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    payment_attempt = db.query(StripePaymentAttempt).filter(
        StripePaymentAttempt.attempt_id == attempt_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs,
                                              payment_attempt.project_id,
                                              db))

    status = get_and_update_payment_attempt_status_from_intent(
        jin.payment_attempt_id, rb, db)

    rb.set_field('status', status)
    if status == ATTEMPT_STATUS_WAITING_TO_PROCESS:
        await process_payment_attempt(jin.payment_attempt_id, rb, db)

    return rb.build_response()


# @router.get('/paymentAttempt/process')
# async def test_process(attempt_id: str,
                       # db: Session = Depends(get_db)):

    # process_payment_attempt(attempt_id, db)

    # return {'ok': 'hi'}
