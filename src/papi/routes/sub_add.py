from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List
import uuid

from papi.core import (
    ApiSchema,
    ApiError,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    Puid,
    ResponseBuilder)
from papi.models import (
    Channel,
    ChannelSub,
    NetworkSub,
    Network,
)
from papi.utils import generate_token


router = APIRouter()

class NetworkSubIn(ApiSchema):
    channel_id: Puid
    instance_id: str
    network_id: Puid
    platform: str
    subscribe_as_surfer: bool
    subscribe_to_all_channels: bool

AlreadySubscribedError = ApiError(
    code=2001,
    msg='Your device is already subscribed.')

NotSignedInError = ApiError(
    code=2002,
    msg='You must be signed in.')

NetworkNotFoundError = ApiError(
    code=2003,
    msg='Network not found.')

# called from the pushboi app after a surfer clicks a subscribe button
@router.post('/networkSub/add.new', response_model=EmptySchema)
async def subscribe_to_network_from_pushboi(request: Request,
                                            jin: NetworkSubIn,
                                            db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    network = db.query(Network).filter(  #type:ignore
        Network.network_id == jin.network_id).first()

    if not network:
        rb.add_error(NetworkNotFoundError)
        return rb.build_response()

    if jin.subscribe_as_surfer:
        if not gibs.surfer_id:
            rb.add_error(NotSignedInError)
            return rb.build_response()

        surfer_or_instance_id = gibs.surfer_id
    else:
        surfer_or_instance_id = jin.instance_id

    network_sub = NetworkSub()
    network_sub.network_sub_id = uuid.uuid4()
    network_sub.surfer_or_instance_id = surfer_or_instance_id
    network_sub.is_silenced = False
    network_sub.network_id = network.network_id

    # make sure the surfer/instance is not subscribed
    check_sub = db.query(NetworkSub).filter(  #type:ignore
        and_((NetworkSub.network_id == network.network_id),
            (NetworkSub.surfer_or_instance_id == surfer_or_instance_id))).first()
    if check_sub:
        rb.add_error(AlreadySubscribedError)
        return rb.build_response()

    network.subscriber_count += 1
    network_sub.pushboi_id = generate_token()
    db.add(network)
    db.add(network_sub)

    channels_in_network = db.query(Channel).filter(  #type:ignore
        Channel.network_id == network.network_id).all()

    channel_subs: List[ChannelSub] = []
    for channel in channels_in_network:
        channel_sub = ChannelSub()
        channel_sub.network_sub_id = network_sub.network_sub_id
        channel_sub.channel_id = channel.channel_id
        channel_sub.network_id = channel.network_id
        channel_sub.is_silenced = False
        channel_sub.surfer_sound = ''
        channel_sub.use_surfer_sound = False

        if str(channel.channel_id) == jin.channel_id \
                or jin.subscribe_to_all_channels:
            channel_sub.is_subscribed = True
        else:
            channel_sub.is_subscribed = False

        channel_subs.append(channel_sub)
        db.add(channel_sub)

    db_commit(db, rb)
    return rb.build_response()
