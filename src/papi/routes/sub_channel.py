from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_, or_
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    PolicyRunner,
    Puid,
    ResponseBuilder,
)
from papi.models import (
    get_and_fix_channel_subs_of_network_with_ids,
    Channel,
    ChannelSub,
    NetworkSub,
)
from papi.routes.channel import ChannelOut

router = APIRouter()


class NetworkSubIdSchema(ApiSchema):
    network_sub_id: Puid

class ChannelSubOut(ApiSchema):
    channel_id: Puid
    network_id: Puid
    network_sub_id: Puid
    channel: ChannelOut
    is_silenced: bool
    is_subscribed: bool
    surfer_sound: str
    use_surfer_sound: bool

class ChannelSubsOut(ApiSchema):
    collection: List[Optional[ChannelSubOut]]

@router.post('/channelSubs/get.byNetworkSubId',
             response_model=ChannelSubsOut)
async def channel_subs_get_for_network_sub(
        request: Request,
        jin: NetworkSubIdSchema,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network_sub = await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    collection = get_and_fix_channel_subs_of_network_with_ids(network_sub, db)

    db_commit(db, rb)
    rb.set_field('collection', collection)
    return rb.build_response()


class ChannelIdNetworkSubIdIn(ApiSchema):
    channel_id: Puid
    network_sub_id: Puid

@router.post('/channelSub/unsubscribe', response_model=EmptySchema)
async def channel_sub_unsubscribe(request: Request,
                                  jin: ChannelIdNetworkSubIdIn,
                                  db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    channel_sub: ChannelSub = db.query(ChannelSub).filter( #type:ignore
        and_(ChannelSub.channel_id == jin.channel_id,
            ChannelSub.network_sub_id == jin.network_sub_id)).first()

    channel_sub.is_subscribed = False
    db.add(channel_sub)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/channelSub/subscribe', response_model=EmptySchema)
async def channel_sub_subscribe(request: Request,
                                jin: ChannelIdNetworkSubIdIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    channel_sub: ChannelSub = db.query(ChannelSub).filter( #type:ignore
        and_(ChannelSub.channel_id == jin.channel_id,
            ChannelSub.network_sub_id == jin.network_sub_id)).first()

    channel_sub.is_subscribed = True
    db.add(channel_sub)
    db_commit(db, rb)
    return rb.build_response()


class ChannelSettingsSaveToRemoteIn(ApiSchema):
    channel_id: Puid
    network_sub_id: Puid
    is_silenced: bool
    surfer_sound: str
    use_surfer_sound: bool

@router.post('/channelSub/settings.saveToRemote', response_model=EmptySchema)
async def channel_sub_settings_save_to_remote(
        request: Request,
        jin: ChannelSettingsSaveToRemoteIn,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    channel_sub: ChannelSub = db.query(ChannelSub).filter( #type:ignore
        and_(ChannelSub.channel_id == jin.channel_id,
             ChannelSub.network_sub_id == jin.network_sub_id)).first()

    channel_sub.is_silenced = jin.is_silenced
    channel_sub.surfer_sound = jin.surfer_sound
    channel_sub.use_surfer_sound = jin.use_surfer_sound

    db.add(channel_sub)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/channelSub/get.byIds', response_model=ChannelSubOut)
async def channel_sub_get_by_ids(request: Request,
                                 jin: ChannelIdNetworkSubIdIn,
                                 db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    channel_sub: ChannelSub = db.query(ChannelSub).filter( #type:ignore
        and_(ChannelSub.channel_id == jin.channel_id,
            ChannelSub.network_sub_id == jin.network_sub_id)).first()

    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()

    out = channel_sub.as_dict()
    out['channel'] = channel.as_dict()
    rb.set_fields_with_dict(out)
    return rb.build_response()
