from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List, Optional

from papi.core import (
    ApiError,
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    PolicyRunner,
    Puid,
    ResponseBuilder,
)
from papi.models import (
    ChannelSub,
    is_id_a_uuid,
    Network,
    NetworkSub,
)
from papi.models.projects_and_networks import (
    make_thumb_network_abbrev,
    make_thumb_network,
)
from papi.routes.network import NetworkOut

router = APIRouter()


NetworkOrProjectKeyRequiredError = ApiError(
    code=9001,
    msg='Network or project authorization key is required.')

ManagedUserIdSubscribedError = ApiError(
    code=9002,
    msg='The managed user ID is already subscribed to this network.')


def get_network_subs_big(db: Session, value: str):
    return db.query(Network.abbrev, #type:ignore
                    Network.color_primary, #type:ignore
                    Network.color_secondary, #type:ignore
                    Network.description, #type:ignore
                    Network.name, #type:ignore
                    Network.network_id, #type:ignore
                    Network.notes, #type:ignore
                    Network.page1, #type:ignore
                    Network.page2, #type:ignore
                    Network.project_id, #type:ignore
                    Network.route, #type:ignore
                    Network.thumb_selected, #type:ignore
                    Network.time_created,
                    Network.time_updated,

                    NetworkSub.surfer_or_instance_id, #type:ignore
                    NetworkSub.network_sub_id, #type:ignore
                    NetworkSub.is_silenced, #type:ignore
    ).join(Network).filter(
        NetworkSub.surfer_or_instance_id == value).all()

class InstanceIdSchema(ApiSchema):
    instance_id: str

class NetworkSubOut(ApiSchema):
    surfer_or_instance_id: str
    network_sub_id: Puid
    network: NetworkOut
    is_silenced: bool

class NetworkSubsOut(ApiSchema):
    collection: List[Optional[NetworkSubOut]]

@router.post('/networkSubs/get.byInstanceIdAndSurferId',
             response_model=NetworkSubsOut)
async def network_subs_get_by_instance_id_and_surfer_id(
        request: Request,
        jin: InstanceIdSchema,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    network_subs = get_network_subs_big(db, jin.instance_id)

    if gibs.surfer_id:
        surfer_network_subs = get_network_subs_big(db, gibs.surfer_id)
        network_subs.extend(surfer_network_subs)

    collection = []
    for sub in network_subs:
        netd = dict(sub)
        netd['thumb_abbrev'] = make_thumb_network_abbrev(netd['network_id'])
        netd['thumb_network'] = make_thumb_network(netd['network_id'])
        net = NetworkOut.parse_obj(netd)

        sub_packaged = {
            'is_silenced': sub['is_silenced'],
            'network_sub_id': sub['network_sub_id'],
            'surfer_or_instance_id': str(sub['surfer_or_instance_id']),

            # load network_now so that we pass all data in one go
            'network': net,
        }
        collection.append(sub_packaged)

    rb.set_field('collection', collection)
    return rb.build_response()


class NetworkSubIdIn(ApiSchema):
    network_sub_id: str

@router.post('/networkSub/get.byId', response_model=NetworkSubOut)
async def network_sub_get_by_id(request: Request,
                                jin: NetworkSubIdIn,
                                db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network_sub = await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == network_sub.network_id).first()

    out = network_sub.as_dict()
    out['network'] = network.as_dict()
    rb.set_fields_with_dict(out)
    return rb.build_response()


class NetworkUnsubscribeIn(ApiSchema):
    network_sub_id: Puid

@router.post('/networkSub/unsubscribe',
             response_model=EmptySchema)
async def network_sub_unsubscribe(request: Request,
                                  jin: NetworkUnsubscribeIn,
                                  db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network_sub = await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    db.delete(network_sub)
    db_commit(db, rb)

    channel_subs = db.query(ChannelSub).filter( #type:ignore
        ChannelSub.network_sub_id == jin.network_sub_id).all()

    for sub in channel_subs:
        db.delete(sub)
    db_commit(db, rb)
    return rb.build_response()


class NetworkSettingsSaveToRemoteIn(ApiSchema):
    is_silenced: bool
    network_sub_id: Puid
    surfer_or_instance_id: str

@router.post('/networkSub/settings.saveToRemote', response_model=EmptySchema)
async def network_sub_settings_save_to_remote(
        request: Request,
        jin: NetworkSettingsSaveToRemoteIn,
        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network_sub = await PolicyRunner.anon_or_surfer_owns_network_sub_id(
        gibs, jin.network_sub_id, db, rb)

    if network_sub.is_linked_to_surfer:
        if is_id_a_uuid(jin.surfer_or_instance_id):
            pass
        else:
            # user wants to delink subscription
            network_sub.surfer_or_instance_id = jin.surfer_or_instance_id

    else:
        if is_id_a_uuid(jin.surfer_or_instance_id):
            # user wants to link subscription
            # look to see if user is already subscribed
            network_sub_surfer: NetworkSub = db.query(NetworkSub).filter( #type:ignore
                and_(NetworkSub.network_sub_id == jin.network_sub_id,
                    NetworkSub.surfer_or_instance_id == gibs.surfer_id)).first()
            if network_sub_surfer:
                # Surfer is already subscribed on another device, so we can delete
                # this subscription
                db.delete(network_sub)
                db_commit(db, rb)

                network_sub = network_sub_surfer
            else:
                # Surfer is not subscribed, so we can alter this sub
                network_sub.surfer_or_instance_id = gibs.surfer_id
                db.add(network_sub)
        else:
            pass

    network_sub.is_silenced = jin.is_silenced

    db_commit(db, rb)
    return rb.build_response()


class NetworkSettingsGetIn(ApiSchema):
    instance_id: str
    network_id: Puid
    surfer_id: Puid

@router.post('/networkSub/settings.get', response_model=EmptySchema)
async def network_sub_settings_get(request: Request,
                                   jin: NetworkSettingsGetIn,
                                   db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    network_sub: NetworkSub = db.query(NetworkSub).filter( #type:ignore
        and_(NetworkSub.network_id == jin.network_id,
            NetworkSub.surfer_or_instance_id == jin.instance_id)).first()

    if network_sub:
        rb.exit_if_policy_fails(await Policies.public())
    else:
        network_sub: NetworkSub = db.query(NetworkSub).filter( #type:ignore
            and_(NetworkSub.network_id == jin.network_id,
                NetworkSub.surfer_of_instance_id == gibs.surfer_id)).first()
        rb.exit_if_policy_fails(
            await Policies.surfer_owns_network_sub(gibs, network_sub))

    rb.set_fields_with_dict(network_sub.as_dict())
    return rb.build_response()
