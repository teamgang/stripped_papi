from fastapi import APIRouter, Depends, Request
from jose import exceptions
from pydantic import Field
from sqlalchemy.orm import Session
import uuid

from papi.conf import conf
from papi.core import (
    ApiSchema,
    db_commit,
    decode_session_jwt,
    create_surfer_jwt,
    create_surfer_session_jwt,
    EmptySchema,
    JwtRefreshOut,
    get_db,
    Gibs,
    in_x_months,
    nowstamp,
    Policies,
    Puid,
    ResponseBuilder,
    SessionJwtIn,
    SurferIdIn,
)
from papi.core.errors import (
    EmailNotVerifiedError,
    EmailRegisteredError,
    LoginFailedError,
)
from papi.models import Surfer, SurferSession
from papi.payments import stripe
from papi.utils import send_email_verification

router = APIRouter()


def is_session_jwt_valid(db: Session, session_jwt: str) -> bool:
    try:
        payload = dict(decode_session_jwt(session_jwt))

        check_session: SurferSession = db.query(SurferSession).filter(
            SurferSession.instance_id == payload['instance_id']).first()  # type: ignore

        if check_session.surfer_id == payload['surfer_id']:
            return True

    except exceptions.ExpiredSignatureError:
        return False

    return False

class RegisterWithEmailIn(ApiSchema):
    email: str = Field(
        min_length=5,
    )
    password: str = Field(
        min_length=4,
    )

class SurferSignInOut(ApiSchema):
    surfer_id: Puid
    stripe_customer_id: Puid
    new_jwt: str
    session_jwt: str

@router.post('/surfer/register.withEmail', response_model=EmptySchema)
async def surfer_register_with_email(jin: RegisterWithEmailIn,
                                     db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    check_surfer = db.query(Surfer).filter(
        Surfer.email == jin.email).first()  # type: ignore

    surfer = Surfer()
    if check_surfer:
        rb.add_error(EmailRegisteredError)
        return rb.build_response()
    else:
        surfer.surfer_id = uuid.uuid4()
        surfer.is_email_verified = False
        surfer.email = jin.email
        surfer.password = conf.pwds.hash(jin.password)

    customer = stripe.Customer.create(
        email=surfer.email,
        metadata={
            'surfer_id': surfer.surfer_id,
        }
    )

    surfer.stripe_customer_id = customer.id
    db.add(surfer)
    db_commit(db, rb)
    send_email_verification(surfer.email, surfer.surfer_id, 'surfer')
    return rb.build_response()


class SignInIn(ApiSchema):
    email: str = Field(
        min_length=5,
    )
    password: str = Field(
        min_length=4,
    )
    instance_id: str
    platform: str

@router.post('/surfer/signIn.withEmail', response_model=SurferSignInOut)
async def surfer_sign_in_with_email(jin: SignInIn,
                                    db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    surfer: Surfer = db.query(Surfer).filter(
        Surfer.email == jin.email).first()  # type: ignore

    if not surfer.is_email_verified:
        rb.add_error(EmailNotVerifiedError)
        return rb.build_response()

    if conf.pwds.verify(jin.password, surfer.password):
        session_jwt = create_surfer_session_jwt(surfer.surfer_id,
                                                jin.instance_id)

        rb.set_field('new_jwt', create_surfer_jwt(surfer.surfer_id))
        rb.set_field('session_jwt', session_jwt)
        rb.set_field('stripe_customer_id', str(surfer.stripe_customer_id))
        rb.set_field('surfer_id', str(surfer.surfer_id))

        check_session = db.query(SurferSession).filter(
            SurferSession.instance_id == jin.instance_id).first()  # type: ignore
        if check_session:
            session = check_session
        else:
            session = SurferSession()

        session.instance_id = jin.instance_id
        session.surfer_id = surfer.surfer_id
        session.session_jwt = session_jwt
        session.platform = jin.platform
        session.time_last_used = int(nowstamp())
        session.time_expires = int(in_x_months(12).timestamp())
        db.add(session)
        db_commit(db, rb)
    else:
        rb.add_error(LoginFailedError)

    return rb.build_response()


class SurferOut(ApiSchema):
    surfer_id: Puid
    email: str

@router.post('/surfer/get.byId', response_model=SurferOut)
async def surfer_get_by_id(request: Request,
                           jin: SurferIdIn,
                           db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    surfer: Surfer = db.query(Surfer).filter(
        Surfer.surfer_id == jin.surfer_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.is_user_this_surfer(gibs, surfer))

    rb.set_fields_with_dict(surfer.as_dict())
    return rb.build_response()


@router.post('/surfer/signOut', response_model=EmptySchema)
async def surfer_sign_out(request: Request,
                          jin: SessionJwtIn,
                          db: Session = Depends(get_db)):
    gibs = Gibs(request, True); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    if is_session_jwt_valid(db, jin.session_jwt) and gibs.is_jwt_valid:
        payload = decode_session_jwt(jin.session_jwt)
        check_session = db.query(SurferSession).filter(
            SurferSession.instance_id == payload['instance_id']).first()  # type: ignore
        if check_session:
            db.delete(check_session)

    db_commit(db, rb)
    return rb.build_response()


@router.post('/surfer/jwt.refresh', response_model=JwtRefreshOut)
async def surfer_jwt_refresh(request: Request,
                             jin: SessionJwtIn,
                             db: Session = Depends(get_db)):
    gibs = Gibs(request, True); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    if is_session_jwt_valid(db, jin.session_jwt) and gibs.is_jwt_valid:
        payload = decode_session_jwt(jin.session_jwt)
        rb.set_field('new_jwt', create_surfer_jwt(payload['surfer_id']))
    else:
        rb.set_field('new_jwt', '')

    return rb.build_response()


@router.post('/surfer/email.verify', response_model=EmptySchema)
async def surfer_email_verify(jin: SurferIdIn,
                              db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    surfer: Surfer = db.query(Surfer).filter(
        Surfer.surfer_id == jin.surfer_id).first()  # type: ignore

    surfer.is_email_verified = True
    db.add(surfer)
    db_commit(db, rb)
    return rb.build_response()


@router.post('/surfer/delete', response_model=EmptySchema)
async def surfer_delete(request: Request,
                        jin: SurferIdIn,
                        db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    surfer: Surfer = db.query(Surfer).filter(
        Surfer.surfer_id == jin.surfer_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.is_user_this_surfer(gibs, surfer))

    db.delete(surfer)
    db_commit(db, rb)
    return rb.build_response()
