from fastapi import APIRouter, Depends, Request
import requests
from sqlalchemy.orm import Session
from starlette.responses import RedirectResponse
import uuid

from papi.core import (
    ApiSchema,
    create_surfer_jwt,
    create_surfer_session_jwt,
    db_commit,
    EmptySchema,
    get_db,
    in_x_months,
    nowstamp,
    plog,
    Policies,
    ResponseBuilder,
)

from papi.core.errors import (
    FailedToValidateWithAppleError,
)
from papi.models import Surfer, SurferSession
from papi.payments import stripe
from papi.routes.surfer import SurferSignInOut
from papi.utils import generate_token
from papi.utils.apple_auth import IdTokenDetails, make_client_secret

router = APIRouter()


class AppleIn(ApiSchema):
    auth_code: str
    id_token: str
    instance_id: str
    platform: str

@router.post('/surfer/signIn.withApple', response_model=SurferSignInOut)
async def surfer_sign_in_with_apple(jin: AppleIn,
                                    db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())
    details_in = IdTokenDetails(jin.id_token)

    url = 'https://appleid.apple.com/auth/token'
    headers = {'content-type': "application/x-www-form-urlencoded"}
    client_secret = make_client_secret(details_in.get_client_id())
    resp = requests.post(
        url,
        headers=headers,
        data={
            'client_id': details_in.get_client_id(),
            'client_secret': client_secret,
            'code': jin.auth_code,
            'grant_type': 'authorization_code',  # OR 'refresh_token'
        },
    )

    apj = resp.json()
    if 'error' in apj:
        plog.error('invalid_client')
        rb.add_error(FailedToValidateWithAppleError)
        return rb.build_response()

    apple_details = IdTokenDetails(apj.get('id_token', ''))

    if not apple_details.is_valid():
        rb.add_error(FailedToValidateWithAppleError)
        return rb.build_response()

    surfer: Surfer = db.query(Surfer).filter(
        Surfer.email == apple_details.email).first()  # type: ignore

    if not surfer:
        surfer = Surfer()
        surfer.surfer_id = uuid.uuid4()
        surfer.is_email_verified = apple_details.is_email_verified
        surfer.email = apple_details.email
        surfer.password = generate_token()

        customer = stripe.Customer.create(
            email=surfer.email,
            metadata={
                'surfer_id': surfer.surfer_id,
            }
        )

        surfer.stripe_customer_id = customer.id
        db.add(surfer)
        db_commit(db, rb)

    # apple account can verify email
    if not surfer.is_email_verified:
        surfer.is_email_verified = apple_details.is_email_verified

    session_jwt = create_surfer_session_jwt(surfer.surfer_id,
                                            jin.instance_id)

    rb.set_field('new_jwt', create_surfer_jwt(surfer.surfer_id))
    rb.set_field('session_jwt', session_jwt)
    rb.set_field('stripe_customer_id', str(surfer.stripe_customer_id))
    rb.set_field('surfer_id', str(surfer.surfer_id))

    check_session = db.query(SurferSession).filter(
        SurferSession.instance_id == jin.instance_id).first()  # type: ignore
    if check_session:
        session = check_session
    else:
        session = SurferSession()

    session.instance_id = jin.instance_id
    session.surfer_id = surfer.surfer_id
    session.is_apple_session = True
    session.is_google_session = False
    session.session_jwt = session_jwt
    session.platform = jin.platform
    surfer.refresh_token_apple = apj.get('refresh_token', '')
    session.time_last_used = int(nowstamp())
    session.time_expires = int(in_x_months(12).timestamp())
    session.set_next_24h_check()
    db.add(session)
    db_commit(db, rb)

    return rb.build_response()

@router.post('/surfer/signIn.withApple.callback.android',
             response_model=EmptySchema)
async def surfer_sign_in_with_apple_callback(request: Request):
    prefix = 'intent://callback?'
    param_string = (await request.body()).decode('UTF-8')
    package = 'io.pushboi.and'
    suffix = f'#Intent;package={package};scheme=signinwithapple;end'
    redir = f'{prefix}{param_string}{suffix}'
    return RedirectResponse(redir, status_code=307)
