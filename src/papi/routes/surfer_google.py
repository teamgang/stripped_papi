from fastapi import APIRouter, Depends
import requests
from sqlalchemy.orm import Session
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    create_surfer_jwt,
    create_surfer_session_jwt,
    get_db,
    in_x_months,
    nowstamp,
    Policies,
    ResponseBuilder,
)

from papi.core.errors import (
    FailedToValidateWithGoogleError,
)
from papi.models import Surfer, SurferSession
from papi.payments import stripe
from papi.routes.surfer import SurferSignInOut
from papi.utils import generate_token

router = APIRouter()


class GoogleIn(ApiSchema):
    token: str
    instance_id: str
    platform: str

@router.post('/surfer/signIn.withGoogle', response_model=SurferSignInOut)
async def surfer_sign_in_with_google(jin: GoogleIn,
                                     db: Session = Depends(get_db)):
    rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.public())

    url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='
    resp = requests.get(f'{url}{jin.token}')

    resp_json = resp.json()
    google_email = resp_json.get('email', '')
    google_id = resp_json.get('user_id', '')
    expires_in = resp_json.get('expires_in', 0)

    if resp.status_code != 200 or \
            google_email == '' or \
            google_id == '' or\
            expires_in <= 0:
        rb.add_error(FailedToValidateWithGoogleError)
        return rb.build_response()

    surfer: Surfer = db.query(Surfer).filter(
        Surfer.email == google_email).first()  # type: ignore

    if not surfer:
        surfer = Surfer()
        surfer.surfer_id = uuid.uuid4()
        surfer.is_email_verified = True
        surfer.email = google_email
        surfer.google_id = google_id
        surfer.password = generate_token()

        customer = stripe.Customer.create(
            email=surfer.email,
            metadata={
                'surfer_id': surfer.surfer_id,
            }
        )

        surfer.stripe_customer_id = customer.id
        db.add(surfer)
        db_commit(db, rb)

    # google account can verify email
    if not surfer.is_email_verified:
        surfer.is_email_verified = True
        db.add(surfer)

    session_jwt = create_surfer_session_jwt(surfer.surfer_id,
                                            jin.instance_id)

    rb.set_field('new_jwt', create_surfer_jwt(surfer.surfer_id))
    rb.set_field('session_jwt', session_jwt)
    rb.set_field('stripe_customer_id', str(surfer.stripe_customer_id))
    rb.set_field('surfer_id', str(surfer.surfer_id))

    check_session = db.query(SurferSession).filter(
        SurferSession.instance_id == jin.instance_id).first()  # type: ignore
    if check_session:
        session = check_session
    else:
        session = SurferSession()

    session.instance_id = jin.instance_id
    session.surfer_id = surfer.surfer_id
    session.is_apple_session = False
    session.is_google_session = True
    session.session_jwt = session_jwt
    session.platform = jin.platform
    session.time_last_used = int(nowstamp())
    session.time_expires = int(in_x_months(12).timestamp())
    db.add(session)
    db_commit(db, rb)

    return rb.build_response()
