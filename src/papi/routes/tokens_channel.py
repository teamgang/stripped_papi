from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    ChannelIdIn,
    EmptySchema,
    get_db,
    Gibs,
    Policies,
    ResponseBuilder)
from papi.models import Channel, ChannelToken
from papi.utils import generate_token

router = APIRouter()


class ChannelTokenCreateSchema(ApiSchema):
    channel_id: str

@router.post('/channelToken/create', response_model=EmptySchema)
async def channel_token_create(request: Request,
                               jin: ChannelTokenCreateSchema,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    channel = db.query(Channel).filter(
        Channel.channel_id == channel_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, channel.project_id, db))

    tokens = db.query(ChannelToken).filter( #type:ignore
        ChannelToken.channel_id == jin.channel_id).all()

    new_token = ''
    keep_trying = True
    # we might want to show the user the last 4 digits of a token, so let's
    # make sure the last 4 digits are unique
    while keep_trying:
        new_token = generate_token()
        last4 = new_token[-4:]
        no_collisions = True
        for token in tokens:
            if token.token[-4:] == last4:
                no_collisions = False

        if no_collisions:
            keep_trying = False

    channel_token = ChannelToken()
    channel_token.channel_id = jin.channel_id
    channel_token.project_id = channel.project_id
    channel_token.token = f'chan_{new_token}'

    db.add(channel_token)
    db_commit(db, rb)
    return rb.build_response()


class ChannelTokenDeleteSchema(ApiSchema):
    token: str

@router.post('/channelToken/delete', response_model=EmptySchema)
async def channel_token_delete(request: Request,
                               jin: ChannelTokenDeleteSchema,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()

    token = db.query(ChannelToken).filter( #type:ignore
        ChannelToken.token == jin.token).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, token.project_id, db))

    db.delete(token)
    db_commit(db, rb)
    return rb.build_response()


class ChannelTokenSchema(ApiSchema):
    channel_id: uuid.UUID
    token: str

class ChannelTokenCollectionSchema(ApiSchema):
    collection: List[Optional[ChannelTokenSchema]]

@router.post('/channelTokens/get.byChannelId',
             response_model=ChannelTokenCollectionSchema)
async def channel_tokens_get_by_channel_id(request: Request,
                                           jin: ChannelIdIn,
                                           db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_channel_id(gibs, jin.channel_id, db))

    tokens = db.query(ChannelToken).filter( #type:ignore
        ChannelToken.channel_id == jin.channel_id).all()
    collection = [d.as_dict() for d in tokens]

    rb.set_field('collection', collection)
    return rb.build_response()

