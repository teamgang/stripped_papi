from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from typing import List, Optional
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    EmptySchema,
    get_db,
    Gibs,
    NetworkIdIn,
    Policies,
    ResponseBuilder)
from papi.models import Network, NetworkToken
from papi.utils import generate_token

router = APIRouter()


class NetworkTokenCreateSchema(ApiSchema):
    network_id: str

@router.post('/networkToken/create', response_model=EmptySchema)
async def network_token_create(request: Request,
                               jin: NetworkTokenCreateSchema,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    network = db.query(Network).filter(
        Network.network_id == network_id).first()  # type: ignore
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, network.project_id, db))

    tokens = db.query(NetworkToken).filter( #type:ignore
        NetworkToken.network_id == jin.network_id).all()

    new_token = ''
    keep_trying = True
    # we might want to show the user the last 4 digits of a token, so let's
    # make sure the last 4 digits are unique
    while keep_trying:
        new_token = generate_token()
        last4 = new_token[-4:]
        no_collisions = True
        for token in tokens:
            if token.token[-4:] == last4:
                no_collisions = False

        if no_collisions:
            keep_trying = False

    network_token = NetworkToken()
    network_token.network_id = jin.network_id
    network_token.project_id = network.project_id
    network_token.token = f'net_{new_token}'

    db.add(network_token)
    db_commit(db, rb)
    return rb.build_response()


class NetworkTokenDeleteSchema(ApiSchema):
    token: str

@router.post('/networkToken/delete', response_model=EmptySchema)
async def network_token_delete(request: Request,
                               jin: NetworkTokenDeleteSchema,
                               db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(await Policies.is_user_signed_in(gibs))

    token = db.query(NetworkToken).filter( #type:ignore
        NetworkToken.token == jin.token).first()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, token.project_id, db))

    db.delete(token)
    db_commit(db, rb)
    return rb.build_response()


class NetworkTokenSchema(ApiSchema):
    network_id: uuid.UUID
    token: str

class NetworkTokenCollectionSchema(ApiSchema):
    collection: List[Optional[NetworkTokenSchema]]

@router.post('/networkTokens/get.byNetworkId',
             response_model=NetworkTokenCollectionSchema)
async def network_tokens_get_by_network_id(request: Request,
                                           jin: NetworkIdIn,
                                           db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_network_id(gibs, jin.network_id, db))

    tokens = db.query(NetworkToken).filter( #type:ignore
        NetworkToken.network_id == jin.network_id).all()
    collection = [d.as_dict() for d in tokens]

    rb.set_field('collection', collection)
    return rb.build_response()

