from fastapi import APIRouter, Depends, Request
from sqlalchemy import and_
from sqlalchemy.orm import Session
import uuid

from papi.core import (
    ApiError,
    ApiSchema,
    ChannelIdIn,
    db_commit,
    EmptySchema,
    get_db,
    get_period_now,
    Gibs,
    in_x_days,
    now,
    nowstamp,
    plog,
    Policies,
    ProjectIdIn,
    ResponseBuilder)
from papi.models import (
    Channel,
    Charge,
    CHARGE_TYPE_STAT_EDIT_TOKEN,
    Network,
    Plan,
    Project,
    StatEditToken,
    STAT_EDIT_STATUS_CONSUMED,
    STAT_EDIT_STATUS_FREE,
    STAT_EDIT_STATUS_PURCHASED,
)

router = APIRouter()

NoTokenError = ApiError(
    code=4404,
    msg='No token to consume.')

NoTokenAndNoFreeUseError = ApiError(
    code=4404,
    msg='No token and no free use to consume.')


@router.post('/stat/token.buy', response_model=EmptySchema)
async def stat_token_buy(request: Request,
                         jin: ProjectIdIn,
                         db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    #NOTE: edit tokens are not tied to a channel until they are activated
    token = StatEditToken()
    token.stat_edit_token_id = str(uuid.uuid4())
    token.project_id = jin.project_id
    token.status = STAT_EDIT_STATUS_PURCHASED
    token.time_activated = 0
    token.time_expires = 0

    project: Project = db.query(Project).filter( #type:ignore
        Project.project_id == jin.project_id).first()

    plan: Plan = db.query(Plan).filter( #type:ignore
        Plan.plan_id == project.plan_id).first()

    charge = Charge()
    charge.charge_id = str(uuid.uuid4())
    charge.charge_type = CHARGE_TYPE_STAT_EDIT_TOKEN
    charge.period = get_period_now()
    charge.plan_id = project.plan_id
    charge.price = plan.static_edit_token_price
    charge.project_id = project.project_id
    charge.units = 1

    db.add(token)
    db.add(charge)
    db_commit(db, rb)
    return rb.build_response()


class StatTokenStatusByChannelIn(ApiSchema):
    month: int
    channel_id: str
    year: int

class StatTokenStatusByChannelOut(ApiSchema):
    monthly_tokens_left: int
    tokens_purchased: int
    tokens_consumed: int

@router.post('/stat/token.status.byChannelAndPeriod',
             response_model=StatTokenStatusByChannelOut)
async def stat_token_status_by_channel(request: Request,
                                       jin: StatTokenStatusByChannelIn,
                                       db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_channel_id(gibs, jin.channel_id, db))

    out = StatTokenStatusByChannelOut

    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == channel.network_id).first()
    project: Project = db.query(Project).filter( #type:ignore
        Project.project_id == network.project_id).first()

    tokens = db.query(StatEditToken).filter( #type:ignore
        StatEditToken.project_id == project.project_id).all()

    plan: Plan = db.query(Plan).filter( #type:ignore
        Plan.plan_id == project.plan_id).first()

    out.monthly_tokens_left = plan.monthly_free_static_edit_tokens
    out.tokens_purchased = 0
    out.tokens_consumed = 0

    for token in tokens:
        if token.status == STAT_EDIT_STATUS_PURCHASED:
            out.tokens_purchased += 1

        elif token.status == STAT_EDIT_STATUS_CONSUMED:
            out.tokens_consumed += 1

        elif token.status == STAT_EDIT_STATUS_FREE:
            pass

        else:
            msg = (f'invalid status for stat_edit_token_id: ',
                   f'{token.stat_edit_token_id}')
            plog.exception(msg)

        if token.is_activated_in_period(jin.year, jin.month) \
                and jin.channel_id == str(token.channel_id):
            out.monthly_tokens_left -= 1

    rb.set_field('monthly_tokens_left', out.monthly_tokens_left)
    rb.set_field('tokens_consumed', out.tokens_consumed)
    rb.set_field('tokens_purchased', out.tokens_purchased)
    return rb.build_response()


@router.post('/stat/token.consume', response_model=EmptySchema)
async def stat_token_consume(request: Request,
                             jin: ChannelIdIn,
                             db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.channel_id, db))

    channel: Channel = db.query(Channel).filter( #type:ignore
        Channel.channel_id == jin.channel_id).first()
    network: Network = db.query(Network).filter( #type:ignore
        Network.network_id == channel.network_id).first()
    project: Project = db.query(Project).filter( #type:ignore
        Project.project_id == network.project_id).first()


    #FIRST look to see if we can create free token
    plan: Plan = db.query(Plan).filter( #type:ignore
        Plan.plan_id == project.plan_id).first()
    monthly_tokens_left = plan.monthly_free_static_edit_tokens

    tokens = db.query(StatEditToken).filter( #type:ignore
        StatEditToken.project_id == project.project_id).all()
    for token in tokens:
        if token.is_activated_in_period(now().year, now().month) \
                and jin.channel_id == str(token.channel_id):
            monthly_tokens_left -= 1

    if monthly_tokens_left > 0:
        token = StatEditToken()
        token.stat_edit_token_id = str(uuid.uuid4())
        token.status = STAT_EDIT_STATUS_FREE
    else:
        #THEN if no free tokens, look for one in db
        token = db.query(StatEditToken).filter( #type:ignore
            and_(StatEditToken.project_id == network.project_id,
                 StatEditToken.status == STAT_EDIT_STATUS_PURCHASED)).first()

        if token:
            token.status = STAT_EDIT_STATUS_CONSUMED
        else:
            #THEN if no token in db, raise error
            rb.add_error(NoTokenAndNoFreeUseError)
            return rb.build_response()

    token.project_id = project.project_id
    token.channel_id = jin.channel_id
    token.time_activated = int(nowstamp())
    token.time_expires = int(in_x_days(1).timestamp())

    channel.static_edit_expires = token.time_expires

    db.add(token)
    db.add(channel)
    db_commit(db, rb)
    return rb.build_response()


class StatTokenStatusByProjectIn(ApiSchema):
    month: int
    project_id: str
    year: int

class StatTokenStatusByProjectOut(ApiSchema):
    tokens_purchased: int
    tokens_consumed: int

@router.post('/stat/token.status.byProjectAndPeriod',
             response_model=StatTokenStatusByProjectOut)
async def stat_token_status_by_project(request: Request,
                                       jin: StatTokenStatusByProjectIn,
                                       db: Session = Depends(get_db)):
    gibs = Gibs(request); rb = ResponseBuilder()
    rb.exit_if_policy_fails(
        await Policies.user_admins_project_id(gibs, jin.project_id, db))

    out = StatTokenStatusByProjectOut

    tokens = db.query(StatEditToken).filter( #type:ignore
        StatEditToken.project_id == jin.project_id).all()

    out.tokens_purchased = 0
    out.tokens_consumed = 0

    for token in tokens:
        if token.status == STAT_EDIT_STATUS_PURCHASED:
            out.tokens_purchased += 1

        elif token.status == STAT_EDIT_STATUS_CONSUMED:
            out.tokens_consumed += 1

        elif token.status == STAT_EDIT_STATUS_FREE:
            pass

        else:
            msg = (f'invalid status for stat_edit_token_id: ',
                   f'{token.stat_edit_token_id}')
            plog.exception(msg)

    rb.set_field('tokens_consumed', out.tokens_consumed)
    rb.set_field('tokens_purchased', out.tokens_purchased)
    return rb.build_response()
