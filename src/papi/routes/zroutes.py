import datetime
from fastapi import APIRouter, Depends, Request

from papi.core.auth import create_jwt
from papi.core.database import get_db
from papi.core.generics import ApiSchema
from papi.core.gibs import Gibs

router = APIRouter()


class MakeJwtForUserSchema(ApiSchema):
    developer_id: str

class TokenSchema(ApiSchema):
    token: str

@router.post('/z/makeJwtForUser', response_model=TokenSchema)
async def z_make_jwt_for_user(request: Request, jin: MakeJwtForUserSchema):
    # we can generate a long jwt here for testing
    token = create_jwt(to_encode={'developer_id': jin.developer_id},
                       expires_delta=datetime.timedelta(days=365))
    print(token)
    return {'token': token}


@router.post('/z/tables.truncate')
async def z_truncate_tables(request: Request, Session = Depends(get_db)):
    gibs = Gibs(request)
    # await Device.delete.gino.status()
    # await HealthCheck.delete.gino.status()
    return {'tables': 'wiped'}


@router.post('/z/print.jwt')
async def z_print_jwt(request: Request):
    gibs = Gibs(request)
    print(gibs.bearer_token)
    return {'new_jwt': gibs.bearer_token}


class HealthOut(ApiSchema):
    name: str
    health: str

@router.get('/z/health', response_model=HealthOut)
async def z_health():
    return {
        'name': 'papi',
        'health': '100',
        'isOk': True,
    }


# @app.get('/apiv1/devices')
# async def devices(request):
    # devices = await Device.query.gino.all()
    # return response.json([device.to_dict() for device in devices])

# @app.get('/apiv1/devices/<id>')
# async def devices(request, id):
    # device = await Device.get(id)
    # return response.json(device.to_dict())

# @app.get('/apiv1/devices/<id>/<model>')
# async def devices(request, id, model):
    # device = await Device.get(id)
    # await device.update(model=model).apply()
    # return response.json(device.to_dict())
