from sqlalchemy.orm import Session
from typing import List, Set
import uuid

from papi.core import plog, Puid
from papi.models import (
    Alert,
    Channel,
    Network,
    NetworkSub,
    TASK_META_CHANNEL_DELETED,
    TASK_META_NEW_CHANNEL,
)
from papi.queues import Task


class AppAlerter:
    def __init__(self, task: Task, db: Session):
        self.task: Task = task
        self.db: Session = db

        self.instance_recipients: List[str] = []
        self.recipient_instance_ids: Set[Puid] = set()
        self.alerts: List[Alert] = []

    def run_all_steps(self):
        channel: Channel = self.db.query(Channel).filter( #type:ignore
            Channel.channel_id == self.task.channel_id).first()

        network: Network = self.db.query(Network).filter( #type:ignore
            Network.network_id == self.task.network_id).first()

        if self.task.meta == TASK_META_NEW_CHANNEL:
            self.alert_subject = f'{network.name} has a new channel!'
            self.alert_msg = f'Click here to subscribe! {channel.name}'
            self._build_new_channel_alerts()

        if self.task.meta == TASK_META_CHANNEL_DELETED:
            self.alert_subject = f'{network.name} removed a channel'
            self.alert_msg = (f'The channel {channel.name} was removed. '
                              'Notifications will no longer be sent for this '
                              'channel.')
            self._build_new_channel_alerts()

        for alert in self.alerts:
            self.db.add(alert)

        plog.debug(f'Sent {len(self.alerts)} alerts.')
        self.db.commit()

    def _build_new_channel_alerts(self) -> None:
        network_subs = self.db.query(NetworkSub).filter( #type:ignore
            NetworkSub.network_id == self.task.network_id).all()

        for sub in network_subs:
            alert = Alert()
            alert.alert_id = uuid.uuid4()
            alert.channel_id = self.task.channel_id
            alert.network_id = self.task.network_id
            alert.subject = self.alert_subject
            alert.msg = self.alert_msg

            if sub.is_linked_to_surfer:
                alert.surfer_id = sub.surfer_id
            else:
                alert.instance_id = sub.instance_id

            self.alerts.append(alert)
