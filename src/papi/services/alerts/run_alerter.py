import sys

from papi.conf import conf
from papi.core import plog, SessionLocal
from papi.queues.task_consumer import TaskConsumer
from papi.services.alerts.app_alerter import AppAlerter
from papi.queues.task import Task


def create_alerts(task: Task):
    with SessionLocal() as db:
        try:
            plog.i(f'found alert job for network {task.network_id}')
            aa = AppAlerter(task, db)
            aa.run_all_steps()
            plog.i(f'finished job')

        except Exception as err:
            plog.exception(f'alerter failed: {task.dumps_data()}', err)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        task_consumer = TaskConsumer(
            queue_name='for_alerter',
            rab_host=conf.rab_host,
            rab_password=conf.rab_password,
            rab_user=conf.rab_user,
        )
        task_consumer.register_callback(create_alerts)
        task_consumer.listen_forever()

    elif len(sys.argv) == 2:
        if sys.argv[1] == 'offline':  # skip the queue
            print(f'Running offline')
            task = Task()
            create_alerts(task)
