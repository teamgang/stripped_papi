from sqlalchemy.orm import Session
from typing import List

from papi.conf import conf
from papi.models import (
    Bill,
    Developer,
    ProjectHasDeveloper,
)
from papi.utils import (
    AlertData,
    create_alert_for_devs,
    DevEmailSchema,
    Mailer,
)


class BillAlerter:
    def __init__(self, bill: Bill, db: Session, mailer: Mailer):
        self.bill = bill
        self.db = db
        self.mailer = mailer

        devs_raw = db.query( #type:ignore
            ProjectHasDeveloper.project_id,
            ProjectHasDeveloper.developer_id,
            Developer.email,
            Developer.allow_email_bills,
        ).join(Developer).filter(
            ProjectHasDeveloper.project_id == self.bill.project_id).all()

        self.devs: List[DevEmailSchema] = []
        for dev in devs_raw:
            self.devs.append(DevEmailSchema(
                developer_id=dev['developer_id'],
                project_id=dev['project_id'],
                email=dev['email'],
                allow_email_bills=dev['allow_email_bills'],
            ))

        self.ad = AlertData()
        self.ad.from_ = conf.email_noreply
        self.ad.project_id = bill.project_id
        self.link = conf.make_url('/bill/{self.bill.bill_id}')

    # 01
    def alert_bill_ready(self):
        self.ad.subject = 'Pushboi: Your bill is ready'

        self.ad.body = (
            'Your bill for pushboi is ready. If you have autopayment enabled '
            'or credit on your project, then your bill will automatically be '
            'paid on the 7th of the month. If you bill is unpaid for 2 weeks, '
            'then limits will be placed on your project. Click the link below '
            'to view your bill.'
            f'<br /><a href="{self.link}">{self.link}</a>')

        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)

    # 02.a
    def alert_bill_due_now(self):
        self.ad.subject = 'Pushboi: Your bill is due today'

        self.ad.body = (
            'Your bill for pushboi is due today. Autopayment was not enabled '
            'so you must complete the payment manually. Click the link below '
            'to view your bill.'
            f'<br /><a href="{self.link}">{self.link}</a>')
        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)

    # 02.b
    def alert_bill_paid(self):
        self.ad.subject = 'Pushboi: Your bill has been paid'

        self.ad.body = ('Your bill for pushboi has been paid. Click the link '
                        f'below to view it.'
                        f'<br /><a href="{self.link}">{self.link}</a>')
        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)

    # 02.c
    def alert_bill_payment_failed(self):
        self.ad.from_ = conf.email_noreply
        self.ad.subject = 'ATTENTION: You Pushboi payment has failed'

        self.ad.body = (
            'A payment has failed for your Pushboi bill. If your bill '
            'remains unpaid on the 21st of the month, then limits will be '
            'applied to this project. Click the link below to view your bill.'
            f'<br /><a href="{self.link}">{self.link}</a>')
        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)

    # 03
    def alert_bill_payment_late(self):
        self.ad.from_ = conf.email_noreply
        self.ad.subject = 'ATTENTION: Your Pushboi payment is late'

        self.ad.body = (
            'Your payment for Pushboi is 1 week late. If your bill '
            'remains unpaid on the 21st of the month, then limits will be '
            'applied to this project. Click the link below to view your bill.'
            f'<br /><a href="{self.link}">{self.link}</a>')
        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)

    # 04
    def alert_project_in_arrears(self):
        self.ad.from_ = conf.email_noreply
        self.ad.subject = 'ATTENTION: your Pushboi project has been limited'

        self.ad.body = (
            'Your Pushboi project payment is 2 weeks past due. Therefore, we '
            'have limited your ability to send new notifications. Click the '
            'link below to view your bill.'
            f'<br /><a href="{self.link}">{self.link}</a>')
        self.mailer.send_email_to_devs(self.devs, self.ad)
        create_alert_for_devs(self.devs, self.ad, self.db)
