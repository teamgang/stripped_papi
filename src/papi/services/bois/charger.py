from sqlalchemy.orm import Session
from typing import List, Set
import uuid

from papi.core import ceildiv, get_period_now, plog
from papi.models import (
    Boi,
    BOI_STATUS_DONE,
    BoiRecipient,
    Charge,
    CHARGE_TYPE_BOIS_CUSTOM,
    CHARGE_TYPE_BOIS_LIMITLESS,
    CHARGE_TYPE_BOIS_STATIC,
    Plan,
    Project,
)
from papi.queues import Task


class BoiCharger:
    def __init__(self, task: Task, db: Session):
        self.task: Task = task
        self.db: Session = db
        self.boi: Boi = self.db.query(Boi).filter( #type:ignore
            Boi.boi_id == task.boi_id).first()

        self.boi_recipients: List[BoiRecipient] = []
        self.recipient_instance_ids: Set[str] = set()

    def run_all_steps(self):
        self._create_charge()
        self.boi.status = BOI_STATUS_DONE
        self.db.add(self.boi)
        self.db.commit()

    def _create_charge(self):
        num_sent: int = self.boi.count_success #type:ignore

        project: Project = self.db.query(Project).filter( #type:ignore
            Project.project_id == self.boi.project_id).first()

        plan: Plan = self.db.query(Plan).filter( #type:ignore
            Plan.plan_id == project.plan_id).first()

        charge = Charge()
        charge.charge_id = str(uuid.uuid4())

        if (self.boi.is_plasticity_custom()):
            num_to_buy = num_sent - project.custom_remaining()
            packs_to_buy = ceildiv(num_to_buy, plan.add_custom_units)
            total_price = plan.add_custom_price * packs_to_buy #type:ignore
            project.custom_bought += packs_to_buy * plan.add_custom_units #type:ignore
            charge.charge_type = CHARGE_TYPE_BOIS_CUSTOM

        elif (self.boi.is_plasticity_limitless()):
            num_to_buy = num_sent - project.limitless_remaining()
            packs_to_buy = ceildiv(num_to_buy, plan.add_limitless_units)
            total_price = plan.add_limitless_price * packs_to_buy #type:ignore
            project.limitless_bought += packs_to_buy * plan.add_limitless_units #type:ignore
            charge.charge_type = CHARGE_TYPE_BOIS_LIMITLESS

        elif (self.boi.is_plasticity_static()):
            num_to_buy = num_sent - project.static_remaining()
            packs_to_buy = ceildiv(num_to_buy, plan.add_static_units)
            total_price = plan.add_static_price * packs_to_buy #type:ignore
            project.static_bought += packs_to_buy * plan.add_static_units #type:ignore
            charge.charge_type = CHARGE_TYPE_BOIS_STATIC

        else:
            msg = (f'invalid boi charge type {self.boi.plasticity} for '
                   f'{self.boi.boi_id}')
            plog.critical(msg)
            raise RuntimeError(msg)

        charge.boi_id = self.boi.boi_id
        charge.period = get_period_now()
        charge.plan_id = project.plan_id
        charge.price = total_price
        charge.project_id = project.project_id
        charge.units = packs_to_buy
        self.db.add(project)
        self.db.add(charge)
        self.db.commit()
