import sys

from papi.conf import conf
from papi.core import plog, SessionLocal
from papi.queues.task_consumer import TaskConsumer
from papi.services.bois.charger import BoiCharger
from papi.queues.task import Task


def charge_boi(task: Task):
    with SessionLocal() as db:
        try:
            plog.i(f'found charge job for boi {task.boi_id}')
            bs = BoiCharger(task, db)
            bs.run_all_steps()
            plog.i(f'finished job')

        except Exception as err:
            plog.exception(f'boi charger failed: {task.dumps_data()}', err)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        task_consumer = TaskConsumer(
            queue_name='for_charger',
            rab_host=conf.rab_host,
            rab_password=conf.rab_password,
            rab_user=conf.rab_user,
        )
        task_consumer.register_callback(charge_boi)
        task_consumer.listen_forever()

    elif len(sys.argv) == 2:
        if sys.argv[1] == 'offline':  # skip the queue
            print(f'Running offline')
            task = Task()
            charge_boi(task)
