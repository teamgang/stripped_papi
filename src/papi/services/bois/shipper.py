from firebase_admin import messaging
import json
from sqlalchemy.orm import Session
from typing import List, Set
import uuid

from papi.core import plog, Puid
from papi.models import (
    Boi,
    BOI_STATUS_ERROR_CHARGE,
    BOI_STATUS_FOR_CHARGE,
    BoiRecipient,
    Channel,
    ChannelSub,
    NetworkSub,
    Plan,
    Project,
    SurferSession,
)
from papi.queues import Task


class BoiShipper:
    def __init__(self, task: Task, db: Session):
        self.task: Task = task
        self.db: Session = db
        self.boi: Boi = self.db.query(Boi).filter( #type:ignore
            Boi.boi_id == task.boi_id).first()

        self.boi_recipients: List[BoiRecipient] = []
        self.recipient_instance_ids: Set[Puid] = set()

        self.project: Project = self.db.query(Project).filter( #type:ignore
            Project.project_id == self.boi.project_id).first()

    def run_all_steps(self):
        self._build_boi_recipients_list()
        can_send = self._is_plan_allowed_to_ship_boi()
        print(f'can i send it? {can_send}')
        if can_send:
            self._multicast_boi()
            self.boi.status = BOI_STATUS_FOR_CHARGE
        else:
            self.boi.status = BOI_STATUS_ERROR_CHARGE

        self.db.add(self.boi)
        self.db.commit()

    def _build_boi_recipients_list(self) -> None:
        channel_subs = self.db.query(ChannelSub).filter( #type:ignore
            ChannelSub.channel_id == self.boi.channel_id).all()

        for sub in channel_subs:
            is_silenced = sub.is_silenced or False
            instance_ids_of_sub = []
            network_sub: NetworkSub = self.db.query(NetworkSub).filter( #type:ignore
                NetworkSub.network_sub_id == sub.network_sub_id).first()

            if not network_sub:
                # this case should never happen... just delete this channel_sub
                self.db.delete(sub)
                continue

            if network_sub.is_linked_to_surfer:
                is_silenced = network_sub.is_silenced or sub.is_silenced
                sessions = self.db.query(SurferSession).filter(
                    SurferSession.surfer_id == network_sub.surfer_or_instance_id).all()  # type: ignore

                for session in sessions:
                    instance_ids_of_sub.append(session.instance_id)
            else:
                instance_ids_of_sub.append(network_sub.surfer_or_instance_id)

            for instance_id in instance_ids_of_sub:
                br = BoiRecipient()
                br.boi_recipient_id = str(uuid.uuid4())
                br.boi_id = self.boi.boi_id
                br.instance_id = instance_id

                br.network_sub_id = sub.network_sub_id
                br.is_silenced = is_silenced
                br.surfer_sound = sub.surfer_sound
                br.use_surfer_sound = sub.use_surfer_sound
                self.boi_recipients.append(br)

        for recipient in self.boi_recipients:
            if recipient.instance_id:
                if recipient.instance_id in self.recipient_instance_ids:
                    #NOTE this can happen if a surfer also has an anon sub
                    plog.error(
                        f'Attempted to send to same recipient twice \n'\
                        f'{recipient.instance_id}, boi_id: {self.boi.boi_id}')
                else:
                    self.db.add(recipient)
                    self.recipient_instance_ids.add(recipient.instance_id)

        # plog.v(str(self.recipient_instance_ids))
        self.boi.count_recipient_instance_ids = len(self.recipient_instance_ids)
        self.db.add(self.boi)
        self.db.commit()

        instance_ids_list = list(self.recipient_instance_ids)
        self.recipient_instance_ids.clear()  # no longer needed, save some RAM

    def _is_plan_allowed_to_ship_boi(self) -> bool:
        num_to_send: int = self.boi.count_recipient_instance_ids #type:ignore

        plan: Plan = self.db.query(Plan).filter( #type:ignore
            Plan.plan_id == self.project.plan_id).first()

        if self.boi.is_plasticity_custom():
            if self.project.custom_remaining() >= num_to_send:
                return True
            else:
                if plan.can_purchase_bois():
                    return True

        elif self.boi.is_plasticity_limitless():
            if self.project.limitless_remaining() >= num_to_send:
                return True
            else:
                if plan.can_purchase_bois():
                    return True

        elif self.boi.is_plasticity_static():
            if self.project.static_remaining() >= num_to_send:
                return True
            else:
                if plan.can_purchase_bois():
                    return True

        return False

    def _multicast_boi(self) -> None:
        # fb_notification = messaging.Notification(
            # title=self.notification.title,
            # body=self.notification.body,
            # image=self.notification.thumb_selected,
        # )

        if len(self.boi_recipients) > 0:
            boi_tx = self._make_boi_tx(self.boi_recipients[0])
            # messages are limited to 4096 bytes
            if len(json.dumps(boi_tx).encode('utf-8')) > 4050:
                plog.critical('boi too big boi_id: {self.boi.boi_id}')
                return

        for boi_recipient in self.boi_recipients:
            #TODO: we could group messages sent to same user
            print(f'sending to {boi_recipient.instance_id}')

            try:
                message = messaging.MulticastMessage(
                    tokens=[boi_recipient.instance_id],
                    # notification=fb_notification,
                    data=self._make_boi_tx(boi_recipient),
                    android=messaging.AndroidConfig(
                        priority='high',
                        # notification=messaging.AndroidNotification(
                            # icon='stock_ticker_update',
                            # color='#f45342',
                            # image='https://i.imgur.com/1M3cRXh.png',
                        # ),
                    ),

                    apns=messaging.APNSConfig(
                        # default apns-priority is 10 and sends notification
                        # immediately. 5 consideders power-saving options
                        headers={'apns-priority': '10'},
                        payload=messaging.APNSPayload(
                            aps=messaging.Aps(
                                content_available=True,
                                # alert=messaging.ApsAlert(
                                    # title='$GOOG up 1.43% on the day',
                                    # body='$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
                                # ),
                                #TODO: count user's undread messages for badge
                                # badge=42,
                            ),
                        ),
                    ),
                )
                #TODO: process the response from messaging.send
                # messaging.send(message)
                fb_response = messaging.send_multicast(message)
                # plog.debug(f'FIREBASE FAILURE COUNT:\n{fb_response.failure_count}')
                self.boi.count_failure = fb_response.failure_count
                self.boi.count_success = fb_response.success_count
                self.db.add(self.boi)
                self.db.commit()
                print('seemed to work')

            except Exception as err:
                print(err)
                plog.exception((f'failed to ship boi_id: {self.boi.boi_id} '
                                f'to instance_id {boi_recipient.instance_id}'),
                               err)

        if self.boi.is_plasticity_custom():
            self.project.custom_sent += self.boi.count_success
        if self.boi.is_plasticity_limitless():
            self.project.limitless_sent += self.boi.count_success
        if self.boi.is_plasticity_static():
            self.project.static_sent += self.boi.count_success

        self.db.add(self.project)
        self.db.commit()

    def _make_boi_tx(self, br: BoiRecipient):
        channel: Channel = self.db.query(Channel).filter( #type:ignore
            Channel.channel_id == self.boi.channel_id).first()

        return {
            'channel_key': 'static',

            'channel_abbrev': channel.abbrev,
            'channel_name': channel.name,
            'color_primary': channel.color_primary,
            'color_secondary': channel.color_secondary,

            'boi_id': str(self.boi.boi_id),
            'channel_id': str(self.boi.channel_id),
            'network_id': str(self.boi.network_id),
            'body': self.boi.body,
            'layout': self.boi.layout,
            'plasticity': self.boi.plasticity,
            'title': self.boi.title,
            'thumb_selected_url': self.boi.get_thumb_selected_url(channel),
            'url': self.boi.url,

            'network_sub_id': str(br.network_sub_id),
            'is_silenced': str(br.is_silenced),
            'surfer_sound': br.surfer_sound,
            'use_surfer_sound': str(br.use_surfer_sound),
        }
