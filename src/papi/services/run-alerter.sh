#!/bin/sh

cd alerts

export DB_CHOICE=postgresql
export PAPI_ENV=dev
export PAPI_ROOT=$HOME/projects/papi

/usr/bin/env python3 run_alerter.py
