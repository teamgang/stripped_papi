#!/bin/sh

echo you can run this like
echo ./run-biller.sh day=1 period=202203

cd bills

export DB_CHOICE=postgresql
export PAPI_ENV=dev
export PAPI_ROOT=$HOME/projects/papi

/usr/bin/env python3 run_biller.py $@
