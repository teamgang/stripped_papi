from pydantic import BaseModel
from sqlalchemy.orm import Session
from typing import List, Union
import uuid

from papi.models import Alert


class DevEmailSchema(BaseModel):
    developer_id: Union[str, uuid.UUID]
    project_id: Union[str, uuid.UUID]
    email: str
    allow_email_bills: bool

class AlertData(BaseModel):
    developer_id: Union[str, uuid.UUID] = ''
    body: str = ''
    from_: str = ''
    project_id: Union[str, uuid.UUID] = ''
    subject: str = ''
    was_allow_email_bills_enabled: bool = False


def create_alert_for_devs(
        devs: List[DevEmailSchema],
        ad: AlertData,
        db: Session,
    ):
    for dev in devs:
        alert = Alert()
        alert.alert_id = uuid.uuid4()
        alert.developer_id = dev.developer_id
        alert.msg = ad.body
        alert.project_id = ad.project_id
        alert.subject = ad.subject
        alert.was_viewed = False
        db.add(alert)
        db.commit()
