import jwt
import requests

from papi.conf import conf
from papi.core import in_x_months, nowstamp, plog
from papi.models import SurferSession


VALID_AUDIENCES = ['io.pushboi.and', 'io.pushboi.ios']

class IdTokenDetails:
    def __init__(self, id_token: str):
        dec = jwt.decode(id_token,
                             audience='https://appleid.apple.com',
                             options={"verify_signature": False},
                             algorithms=['ES256'],
                             )

        self.audience = dec.get('aud', '')
        self.email = dec.get('email', '')
        email_verified_str = dec.get('email_verified', False)
        self.is_email_verified = True if email_verified_str == 'true' else False
        self.exp: int = int(dec.get('exp', '0'))
        self.issuer = dec.get('iss', '')

        self.platform = 'notset'
        if self.audience == 'io.pushboi.and':
            self.platform = 'android'
        if self.audience == 'io.pushboi.ios':
            self.platform = 'ios'

    def get_client_id(self):
        return self.audience

    def is_valid(self):
        if self.issuer == 'https://appleid.apple.com' and \
                self.audience in VALID_AUDIENCES and \
                self.exp > nowstamp():
            return True
        return False


def make_client_secret(sub: str):
    headers = {
        'alg': 'ES256',
        'kid': conf.apple_sign_in_key_id,
    }

    payload = {
        'iss': conf.apple_team_id,
        'iat': nowstamp(),
        'exp': in_x_months(2).timestamp(),
        'aud': 'https://appleid.apple.com',
        'sub': sub,

        # 'c_hash': decoded['c_hash'],
        # 'email': decoded['email'],
        # 'email_verified': decoded['email_verified'],
        # 'nonce_supported': decoded['nonce_supported'],
    }

    return jwt.encode(
        payload,
        conf.apple_sign_in_p8,
        algorithm='ES256',
        headers=headers,
    )


def apple_24h_check(session: SurferSession) -> bool:
    url = 'https://appleid.apple.com/auth/token'
    headers = {'content-type': "application/x-www-form-urlencoded"}
    client_secret = make_client_secret(session.get_client_id())
    resp = requests.post(
        url,
        headers=headers,
        data={
            'client_id': session.get_client_id(),
            'client_secret': client_secret,
            'grant_type': 'refresh_token',  # OR 'refresh_token'
            'refresh_token': session.refresh_token_apple,
        },
    )

    apj = resp.json()
    if 'error' in apj:
        plog.error('invalid_client')

    apple_details = IdTokenDetails(apj.get('id_token', ''))
    #NOTE: basically, if email is set, then we can assume it's good
    if apple_details.email != '':
        return True
    else:
        return False
