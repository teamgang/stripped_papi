class BasketItem:
    def __init__(self,
                 admin_notes: str,
                 item_id: str,
                 item_type: str,
                 name: str,
                 plan_id: str,
                 price: int,
                 project_id: str,
                 ):
        self.admin_notes = admin_notes
        self.item_id = item_id
        self.item_type = item_type
        self.name = name
        self.plan_id = plan_id
        self.price = price
        self.project_id = project_id

    def printf(self):
        return (
            f'admin_notes: {self.admin_notes}\n'\
            f'item_id: {self.item_id}\n'\
            f'item_type: {self.item_type}\n'\
            f'name: {self.name}\n'\
            f'plan_id: {self.plan_id}\n'\
            f'price: {self.price}\n'\
            f'project_id: {self.project_id}\n'\
        )
