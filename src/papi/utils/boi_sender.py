from sqlalchemy import and_
from sqlalchemy.orm import Session
from typing import List
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    get_period_now,
    plog,
    ResponseBuilder,
)
from papi.models import (
    AS_USER,
    Channel,
    ChannelToken,
    Network,
    NetworkToken,
    Boi,
    BOI_STATUS_FOR_SHIP,
    Plan,
    Project,
    ProjectHasDeveloper,
)
from papi.queues import publish_task, Task


class BoiSenderIn(ApiSchema):
    body: str = ''
    channel_route: str
    layout: str = 'default'
    notes: str = ''
    plasticity: str = ''
    title: str = ''
    thumb_selected: str = ''
    token: str = ''
    url: str = ''


class BoiSenderProcessor:
    def __init__(self, db: Session, jin: BoiSenderIn, developer_id: str):
        self.jin: BoiSenderIn = jin
        self.developer_id = developer_id
        self.db = db

        self.channel: Channel = db.query(Channel).filter( #type:ignore
            Channel.route == self.jin.channel_route).first()

        self.network: Network = db.query(Network).filter( #type:ignore
            Network.network_id == self.channel.network_id).first()

        if jin.token.startswith('chan_'):
            self.token: str = jin.token
            self.db_token: ChannelToken = db.query(ChannelToken).filter( #type:ignore
                and_(ChannelToken.token == self.jin.token,
                     ChannelToken.channel_id == self.channel.channel_id)).first()
        elif jin.token.startswith('net_'):
            self.token: str = jin.token
            self.db_token: NetworkToken = db.query(NetworkToken).filter( #type:ignore
                and_(NetworkToken.token == self.jin.token,
                     NetworkToken.network_id == self.network.network_id)).first()
        else:
            self.token: str = AS_USER

        self.project: Project = db.query(Project).filter( #type:ignore
            Project.project_id == self.network.project_id).first()

        self.plan: Plan = db.query(Plan).filter( #type:ignore
            Plan.plan_id == self.project.plan_id).first()

        self.body: str = jin.body
        self.layout: str = jin.layout
        self.notes: str = jin.notes
        self.plasticity: str = jin.plasticity
        self.thumb_selected: str = jin.thumb_selected
        self.title: str = jin.title
        self.url: str = jin.url

        self.boi: Boi = Boi()

    def is_project_in_arrears(self) -> bool:
        return self.project.is_in_arrears

    def is_token_valid_or_developer_authorized(self) -> bool:
        if self.token == AS_USER:
            devs: List[ProjectHasDeveloper] = self.db.query(
                ProjectHasDeveloper).filter( #type:ignore
                ProjectHasDeveloper.project_id == self.network.project_id).all()

            for dev in devs:
                if str(dev.developer_id) == self.developer_id:
                    return True
        else:
            if self.db_token:
                return True

        return False

    def send_to_shipper(self, db: Session, rb: ResponseBuilder) -> None:
        self.boi.boi_id = uuid.uuid4()
        self.boi.channel_id = self.channel.channel_id
        self.boi.network_id = self.channel.network_id
        self.boi.project_id = self.network.project_id
        self.boi.period = get_period_now()
        self.boi.plan_id = self.project.plan_id
        self.boi.is_hidden = False
        self.boi.layout = self.layout
        self.boi.notes = self.notes
        self.boi.plasticity = self.plasticity
        self.boi.status = BOI_STATUS_FOR_SHIP
        self.boi.thumb_selected = self.thumb_selected
        self.boi.token = self.token
        self.boi.url = self.url

        if self.boi.is_plasticity_static():
            self.boi.body = self.channel.static_body
            self.boi.title = self.channel.static_title
        else:
            self.boi.body = self.body
            self.boi.title = self.title

        db.add(self.boi)
        db_commit(db, rb)

        plog.d('sending to shipper')

        task: Task = Task()
        task.boi_id = str(self.boi.boi_id)
        task.channel_id = str(self.channel.channel_id)
        task.network_id = str(self.network.network_id)
        task.project_id = str(self.project.project_id)

        publish_task(task, 'for_shipper')
