from email.message import EmailMessage
from sqlalchemy.orm import Session
from smtplib import SMTP, SMTPException
from typing import List, Union
import ssl
import uuid

from papi.conf import conf, Conf
from papi.core import plog, SessionLocal
from papi.models import Email
from papi.utils import AlertData, DevEmailSchema

mailcon_type = Union[None, SMTP]


MAIL_CONTEXT = ssl.create_default_context()


def send_email_simple(from_: str, to_: str, subject: str, body: str):
    with SessionLocal() as db:
        mailer = make_mailer(db, conf)
        mailer.send_email(from_, to_, subject, body)


def make_mailer(db: Session, conf: Conf):
        return Mailer(db, conf.smtp_host, conf.smtp_user, conf.smtp_pass)


class Mailer:
    def __init__(self,
                 db: Session,
                 smtp_host: str,
                 smtp_user: str,
                 smtp_pass: str):
        self.db = db
        try:
            self.mailcon: mailcon_type = SMTP(smtp_host, 587)
            self.mailcon.starttls(context=MAIL_CONTEXT)
            self.mailcon.login(smtp_user, smtp_pass)
        except ConnectionRefusedError:
            plog.debug(f'failed to connect to smtp at {smtp_host}')
            self.mailcon: mailcon_type = None

    def send_email_to_devs(self, devs: List[DevEmailSchema], ad: AlertData):
        for dev in devs:
            record = Email()
            record.email_id = uuid.uuid4()
            record.from_ = ad.from_
            record.to_ = dev.email
            record.body = ad.body
            record.subject = ad.subject
            record.project_id = ad.project_id

            record.developer_id = dev.developer_id
            record.was_allow_email_bills_enabled = dev.allow_email_bills

            self.db.add(record)
            self.db.commit()

            msg = EmailMessage()
            msg['From'] = ad.from_
            msg['To'] = dev.email
            msg['Subject'] = ad.subject
            msg['Importance'] = 'high'
            msg.set_content(ad.body)

            plog.info(f'Attempt to send email to {dev.email}: {ad.subject}\n'
                      f'{ad.body}')

            if dev.allow_email_bills:
                if self.mailcon:
                    try:
                        self.mailcon.send_message(msg)
                    except SMTPException:
                        plog.exception('unable to send email: '
                                       f'{record.email_id}')
                else:
                    plog.debug(f'could not send email since no mailcon for '
                               f'{record.email_id}')

    def send_email(self, from_: str, to_: str, subject: str, body: str):
        record = Email()
        record.email_id = uuid.uuid4()
        record.from_ = from_
        record.to_ = to_
        record.subject = subject
        record.body = body

        self.db.add(record)
        self.db.commit()

        msg = EmailMessage()
        msg['From'] = from_
        msg['To'] = to_
        msg['Subject'] = subject
        # msg['Importance'] = 'high'
        msg.set_content(body)

        plog.info(f'Attempt to send email to {to_}: {subject}\n{msg}')

        if self.mailcon:
            try:
                self.mailcon.send_message(msg)
            except SMTPException:
                plog.exception('unable to send email: '
                               f'{record.email_id}')
        else:
            plog.debug(f'could not send email since no mailcon for '
                       f'{record.email_id}')
