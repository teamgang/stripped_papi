import asyncio
from dateutil.relativedelta import relativedelta
from fastapi import Depends
import json
import pprint
from sqlalchemy.orm import Session
import uuid

from papi.core import (
    db_commit,
    get_db,
    nowstamp,
    now,
    plog,
    Puid,
    ResponseBuilder,
)
from papi.models import (
    ATTEMPT_STATUS_FAILED,
    ATTEMPT_STATUS_WAITING_ON_STRIPE,
    ATTEMPT_STATUS_WAITING_TO_PROCESS,
    ATTEMPT_STATUS_PROCESSING,
    ATTEMPT_STATUS_PROCESSED,
    Bill,
    BILL_STATUS_PAID,
    BILL_STATUS_PROCESSING,
    BILL_STATUS_TRY_AGAIN,
    Payment,
    PAYMENT_PLATFORM_STRIPE,
    # PAYMENT_PLATFORM_BTCPAY,
    Plan,
    Project,
    SILO_ADD_CREDIT,
    SILO_BASKET,
    SILO_BILL_PAY,
    SILO_PLAN,
    StripePaymentAttempt,
)

from papi.payments import stripe
from papi.services.bills.run_biller import try_to_pay_failed_bills
from papi.utils.basket_item import BasketItem
from papi.validation.stripe_payment_attempt import ValidateStripePaymentAttempt


def get_and_update_payment_attempt_status_from_intent(
    attempt_id: str,
    rb: ResponseBuilder,
    db: Session,
) -> int:
    payment_attempt = db.query(StripePaymentAttempt).filter(
        StripePaymentAttempt.attempt_id == attempt_id).first()  # type: ignore

    payment_intent = stripe.PaymentIntent.retrieve(
        payment_attempt.payment_intent_id)

    # list of statuses:
    # https://stripe.com/docs/payments/payment-intents/verifying-status
    # requires_payment_method,
    # requires_confirmation
    # requires_action
    # processing
    # requires_capture
    # canceled
    # succeeded

    new_status = ATTEMPT_STATUS_WAITING_ON_STRIPE

    if payment_intent.status == 'succeeded':
        new_status = ATTEMPT_STATUS_PROCESSED
    elif payment_intent.status == 'canceled':
        new_status = ATTEMPT_STATUS_FAILED
    elif payment_intent.status == 'requires_payment_method' or \
        payment_intent.status == 'requires_confirmation' or \
        payment_intent.status == 'requires_action' or \
        payment_intent.status == 'processing' or \
        payment_intent.status == 'requires_capture':
        new_status = ATTEMPT_STATUS_WAITING_ON_STRIPE
    else:
        plog.critical(
            f'unknown stripe PaymentIntent status for attempt_id = {attempt_id}')

    payment_attempt.status = new_status
    db.add(payment_attempt)
    db_commit(db, rb)

    return new_status

async def process_payment_attempt(attempt_id: Puid,
                                  rb: ResponseBuilder,
                                  db: Session = Depends(get_db)):
    await asyncio.sleep(2)
    print(f'processing payment {attempt_id}')

    payment_attempt = db.query(StripePaymentAttempt).filter(
        StripePaymentAttempt.attempt_id == attempt_id).first()  # type: ignore

    if payment_attempt.status != ATTEMPT_STATUS_WAITING_TO_PROCESS:
        return

    if (not ValidateStripePaymentAttempt.all_fields(payment_attempt, db)):
        payment_attempt.status = ATTEMPT_STATUS_FAILED
        payment_attempt.admin_notes += '; failed validation'
        db.add(payment_attempt)
        db_commit(db, rb)
        return

    print(f'attempt status {payment_attempt.status}')

    age = nowstamp() - payment_attempt.time_created

    # TODO: decide a good timeout value here. we can start with 1 hour
    print(f'age: {age}')
    if age > 3600:
        print(f'age > 3600')
        print('payment older than 1 hour - mark it as dead')
        payment_attempt.status = ATTEMPT_STATUS_FAILED
        payment_attempt.admin_notes += '; payment was not made in 1 hour'
        db.add(payment_attempt)
        db_commit(db, rb)
        return

    #TODO: fix this function
    should_process = _should_process_payment_attempt(payment_attempt)
    should_process = True
    print(f'should_process: {should_process}')

    payment_attempt.time_updated = nowstamp()
    if not should_process:
        return

    payment_attempt.status = ATTEMPT_STATUS_PROCESSING
    db.add(payment_attempt)
    db_commit(db, rb)

    print(payment_attempt.basket)
    pp = pprint.PrettyPrinter(indent=4)
    basket = json.loads(payment_attempt.basket)
    pp.pprint(basket)

    if payment_attempt.silo == SILO_ADD_CREDIT:
        _process_silo_add_credit(payment_attempt)

    elif payment_attempt.silo == SILO_BILL_PAY:
        _process_silo_bill_pay(payment_attempt, rb, db)

    elif payment_attempt.silo == SILO_BASKET:
        for itemRaw in basket:
            item = BasketItem(**itemRaw)
            _process_basket_item(item)

    elif payment_attempt.silo == SILO_PLAN:
        _process_silo_plan(payment_attempt, rb, db)

    else:
        msg = f'Invalid payment_attempt.silo+{payment_attempt.silo}'
        plog.critical(msg)
        raise RuntimeError(msg)

    project = db.query(Project).filter(  #type:ignore
        Project.project_id == payment_attempt.project_id).first()

    if payment_attempt.is_autopay_selected:
        #NOTE: do nothing when false, since we don't want to disable previous
        # autopay method
        project.is_autopay_selected = True

    _adjust_project_credit(payment_attempt, project)
    db.add(project)
    db_commit(db, rb)

    payment_attempt.status = ATTEMPT_STATUS_PROCESSED
    db.add(payment_attempt)
    db_commit(db, rb)

    pa = payment_attempt
    # payment is created for successful payment attempt
    payment = Payment()
    payment.payment_id = str(uuid.uuid4())
    payment.project_id = pa.project_id
    payment.payment_intent_id = pa.payment_intent_id
    payment.payment_platform = PAYMENT_PLATFORM_STRIPE

    payment.admin_notes = pa.admin_notes
    payment.basket = pa.basket
    payment.bill_id = pa.bill_id
    payment.credit_applied = pa.credit_applied
    payment.currency = pa.currency
    payment.is_autopay_selected = pa.is_autopay_selected
    payment.silo = pa.silo
    payment.plastic_id = pa.plastic_id
    payment.status = pa.status
    payment.terms_accepted_version = pa.terms_accepted_version
    payment.total_price = pa.total_price
    payment.total_price_after_credit = pa.total_price_after_credit

    db.add(payment)
    db_commit(db, rb)


def _process_silo_plan(pa: StripePaymentAttempt,
                       rb: ResponseBuilder,
                       db: Session):
    print('++++++++++++++++++++++++++++++++++')
    print('processing silo plan')
    project = db.query(Project).filter(  #type:ignore
        Project.project_id == pa.project_id).first()

    plan = db.query(Plan).filter(  #type:ignore
        Plan.plan_id == pa.plan_id).first()

    project.time_plan_expires = (now() + \
        relativedelta(months=+plan.months)).timestamp()

    print('======================')
    print(project.custom_bought)
    print(project.limitless_bought)
    print(project.static_bought)
    print('+++++++++++++++++++++++++++++')
    print(plan.bundled_custom)
    print(plan.bundled_limitless)
    print(plan.bundled_static)
    project.custom_bought += plan.bundled_custom
    project.limitless_bought += plan.bundled_limitless
    project.static_bought += plan.bundled_static

    project.plan_id = pa.plan_id
    project.plan_id_next = project.plan_id

    db.add(project)
    db_commit(db, rb)


def _process_silo_add_credit(pa: StripePaymentAttempt):
    print('++++++++++++++++++++++++++++++++++')
    print('processing silo add credit: checking unpaid bills')
    try_to_pay_failed_bills(pa.project_id)


def _process_silo_bill_pay(pa: StripePaymentAttempt,
                           rb: ResponseBuilder,
                           db: Session):
    print('++++++++++++++++++++++++++++++++++')
    print('processing silo bill pay')
    bill = db.query(Bill).filter(  #type:ignore
        Bill.bill_id == pa.bill_id).first()

    if pa.status == ATTEMPT_STATUS_FAILED:
        bill.status = BILL_STATUS_TRY_AGAIN
    elif pa.status == ATTEMPT_STATUS_WAITING_ON_STRIPE \
            or pa.status == ATTEMPT_STATUS_WAITING_TO_PROCESS \
            or pa.status == ATTEMPT_STATUS_PROCESSING:
        bill.status = BILL_STATUS_PROCESSING
    elif pa.status == ATTEMPT_STATUS_PROCESSED:
        bill.status = BILL_STATUS_PAID

    db.add(bill)
    db_commit(db, rb)


def _adjust_project_credit(pa: StripePaymentAttempt, project: Project) -> None:
    max_credit = project.max_credit_to_apply(pa.total_price)
    if max_credit != pa.credit_applied:
        msg = (f'credit application mismatch: {pa.credit_applied} was ',
               f'applied, but max credit is {max_credit} at {nowstamp()}')
        plog.warning(msg)
        pa.admin_notes += f'; {msg}'

    if pa.silo == SILO_ADD_CREDIT:
        project.credit += pa.total_price

    elif pa.silo == SILO_BILL_PAY or \
            pa.silo == SILO_BASKET or \
            pa.silo == SILO_PLAN:
        project.credit -= pa.credit_applied

    else:
        msg = f'Invalid payment_attempt.silo+{pa.silo}'
        plog.critical(msg)
        raise RuntimeError(msg)


def _process_basket_item(item: BasketItem):
    print('++++++++++++++++++++++++++++++++++')
    print('processing silo basket... not implemented')
    print(item.name)


def _should_process_payment_attempt(
    payment_attempt: StripePaymentAttempt) -> bool:

    age = nowstamp() - payment_attempt.time_created
    time_since_update = nowstamp() - payment_attempt.time_updated

    # after 2 minutes, poll slower
    if age > 120 and time_since_update > 30:
        return True

    # after 3 minutes, poll slower
    if age > 180 and time_since_update > 60:
        return True

    if age > 1200:
        return False

    return False
