from fastapi import Request
from sqlalchemy.orm import Session
from stripe.error import CardError
import uuid

from papi.core import (
    ApiSchema,
    db_commit,
    Gibs,
    plog,
    ResponseBuilder,
)
from papi.models import (
    ATTEMPT_STATUS_CREATED,
    ATTEMPT_STATUS_FAILED,
    ATTEMPT_STATUS_WAITING_ON_STRIPE,
    ATTEMPT_STATUS_WAITING_TO_PROCESS,
    Developer,
    SILO_BILL_PAY,
    StripePaymentAttempt,
)
from papi.payments import stripe
from papi.validation.stripe_payment_attempt import ValidateStripePaymentAttempt


class PaymentDetailsIn(ApiSchema):
    bill_id: str
    credit_applied: int
    is_autopay_selected: bool
    is_save_card_selected: bool
    is_plastic_id_unlisted: bool
    silo: str
    terms_accepted_version: str
    total_price: int
    total_price_after_credit: int
    plan_id: int
    plastic_id: str
    project_id: str
    basket: str


class PaymentDetailsHandler:
    def __init__(self, jin: PaymentDetailsIn,
                 db: Session,
                 gibs: Gibs,
                 rb: ResponseBuilder):
        self.db = db
        self.rb = rb

        self.spa = StripePaymentAttempt()
        self.spa.attempt_id = uuid.uuid4()
        self.spa.credit_applied = jin.credit_applied
        self.spa.plan_id = jin.plan_id
        self.spa.project_id = jin.project_id
        self.spa.silo = jin.silo
        self.spa.total_price = jin.total_price
        self.spa.total_price_after_credit = jin.total_price_after_credit
        self.spa.is_autopay_selected = jin.is_autopay_selected
        self.spa.plastic_id = jin.plastic_id
        self.terms_accepted_version = jin.terms_accepted_version
        self.spa.basket = jin.basket
        self.spa.status = ATTEMPT_STATUS_CREATED

        if jin.silo == SILO_BILL_PAY:
            self.spa.bill_id = jin.bill_id

        plog.v('payment_attempt created')

        self.jin = jin

        self.developer = db.query(Developer).filter(
            Developer.developer_id == gibs.developer_id).first()  # type: ignore

    def commit_changes(self):
        self.db.add(self.spa)
        db_commit(self.db, self.rb)

    def step1_validate(self):
        if (not ValidateStripePaymentAttempt.all_fields(self.spa, self.db)):
            plog.warning('payment_attempt validation failed')
            self.spa.admin_notes = 'Failed validation'
            self.spa.status = ATTEMPT_STATUS_FAILED
            self.spa.payment_intent_id = 'neverReached'
            self.commit_changes()

    # def step2_attempt_payment_unlisted_card(self):
    def step2_make_stripe_session_with_unlisted_card(self, request: Request):
        success_url = (f'{request.base_url}v1/payment/result'
                    f'?payment_attempt_id={self.spa.attempt_id}')
        cancel_url = success_url

        kargs = {
            'customer': self.developer.stripe_customer_id,
            'line_items': [{
                'price_data': {
                    'currency': 'usd',
                    'product_data': {
                        'name': 'Subscription price after credit',
                    },
                    'unit_amount': self.jin.total_price_after_credit,
                },
                'quantity': 1,
            }],
            'mode': 'payment',
            'payment_method_types': ['card'],
            'success_url': success_url,
            'cancel_url': cancel_url,
        }

        if self.jin.is_save_card_selected:
            plog.v('save_card_selected == True')
            session = stripe.checkout.Session.create(
                **kargs,
                payment_intent_data={
                    'setup_future_usage': 'off_session',
                },
            )

        else:
            plog.v('save_card_selected == False')
            session = stripe.checkout.Session.create(
                **kargs,
            )

        self.spa.payment_intent_id = session.payment_intent

        # print(self.spa.payment_intent_id)
        # print(success_url)
        # print(cancel_url)
        self.commit_changes()
        return session


    def step2_make_stripe_session_with_saved_card(self):
        try:
            pi = stripe.PaymentIntent.create(
                amount=self.jin.total_price_after_credit,
                currency='usd',
                customer=self.developer.stripe_customer_id,
                payment_method=self.jin.plastic_id,
                off_session=True,
                confirm=True,
            )
            payment_intent_id = pi.id

            if pi.status == 'succeeded':
                self.spa.status = ATTEMPT_STATUS_WAITING_TO_PROCESS
            elif pi.status == 'canceled':
                self.spa.status = ATTEMPT_STATUS_FAILED
            elif pi.status == 'requires_payment_method' or \
                pi.status == 'requires_confirmation' or \
                pi.status == 'requires_action' or \
                pi.status == 'processing' or \
                pi.status == 'requires_capture':
                self.spa.status = ATTEMPT_STATUS_WAITING_ON_STRIPE
            else:
                plog.critical(
                    f'unknown stripe PaymentIntent status for attempt_id = '\
                    f'{self.spa.attempt_id}')
                self.spa.status = ATTEMPT_STATUS_FAILED

        except CardError as e:
            err = e.error
            # Error code will be authentication_required if authentication is needed
            msg = f'Code is {err.code} for spa {self.spa.attempt_id}'
            plog.critical(msg)
            payment_intent_id = err.payment_intent['id']
            self.spa.status = ATTEMPT_STATUS_FAILED

        self.spa.payment_intent_id = payment_intent_id
        self.commit_changes()
