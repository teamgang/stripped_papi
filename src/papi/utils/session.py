from jose import exceptions
from sqlalchemy.orm import Session

from papi.core import decode_session_jwt
from papi.models import SurferSession
from papi.utils.apple_auth import apple_24h_check


def is_session_jwt_valid(db: Session, session_jwt: str) -> bool:
    try:
        payload = dict(decode_session_jwt(session_jwt))

        check_session: SurferSession = db.query(SurferSession).filter(
            SurferSession.instance_id == payload['instance_id']).first()  # type: ignore

        if check_session.needs_24h_check():
            apple_auth_valid = apple_24h_check(check_session)
            if not apple_auth_valid:
                return False

        if check_session.surfer_id == payload['surfer_id']:
            return True

    except exceptions.ExpiredSignatureError:
        return False

    return False
