from firebase_admin import messaging
from sqlalchemy.orm import Session
from typing import List

from papi.core import plog
from papi.models import (
    SurferSession,
)

COMMAND_SYNC_ALL_SETTINGS = 'commandSyncAllSettings'

##
# this code is probably dead - we should just sync on every notification
##
def boi_send_command_sync_settings(db: Session, instance_id: str):
    check_surfer_session: SurferSession = db.query(SurferSession).filter(
        SurferSession.instance_id == instance_id).all()  # type: ignore

    if check_surfer_session:
        sessions = db.query(SurferSession).filter(
            SurferSession.surfer_id == check_surfer_session.surfer_id).all()  # type: ignore
        instance_ids: List[str] = [x.instance_id for x in sessions]
    else:
        instance_ids: List[str] = [instance_id]

    boi_tx = {
        'channel_key': 'command',
        'plasticity': COMMAND_SYNC_ALL_SETTINGS,
    }

    try:
        message = messaging.MulticastMessage(
            tokens=list(instance_ids),
            # notification=fb_notification,
            data=boi_tx,
            android=messaging.AndroidConfig(
                priority='high',
                # notification=messaging.AndroidNotification(
                    # icon='stock_ticker_update',
                    # color='#f45342',
                    # image='https://i.imgur.com/1M3cRXh.png',
                # ),
            ),

            apns=messaging.APNSConfig(
                # default apns-priority is 10 and sends notification
                # immediately. 5 consideders power-saving options
                headers={'apns-priority': '10'},
                payload=messaging.APNSPayload(
                    aps=messaging.Aps(
                        content_available=True,
                        # alert=messaging.ApsAlert(
                            # title='$GOOG up 1.43% on the day',
                            # body='$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
                        # ),
                    ),
                ),
            ),
        )
        #TODO: process the response from messaging.send
        # messaging.send(message)
        fb_response = messaging.send_multicast(message)
        plog.debug(f'FIREBASE FAILURE COUNT:\n{fb_response.failure_count}')

    except Exception as err:
        plog.exception('failed to send a message', err)
        raise err
