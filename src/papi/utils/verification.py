from papi.conf import conf
from papi.core import (
    plog,
    SessionLocal,
    Puid,
)
from papi.utils.mailer import make_mailer


def send_email_verification(to_: str, id: Puid, type_: str):
    if type_ != 'developer' and type_ != 'surfer':
        plog.critical(f'Invalid type passed to email verification:+{type_}\n'
                      f'For developerId:+{id}')

    subject = 'PushBoi: Verify your e-mail'
    url = conf.make_url(f'{type_}/email.confirm.link?miscId={id}')
    msg = ('Click the link below to verify your e-mail address for PushBoi.'
           f'\n\n{url}')

    with SessionLocal() as db:
        mailer = make_mailer(db, conf)
        mailer.send_email(conf.email_noreply, to_, subject, msg)


def send_change_email_verification(to_: str, token: str, type_: str):
    if type_ != 'developer' and type_ != 'surfer':
        plog.critical(f'Invalid type passed to change email verification:+'
                      f'{type_}\nFor token:+{token}')

    subject = 'PushBoi: Verify e-mail change'
    url = conf.make_url(f'{type_}/change.email.validate.new.link?token={token}')
    msg = ('Click the link below to change your e-mail address for PushBoi.'
           f'\n\n{url}')

    with SessionLocal() as db:
        mailer = make_mailer(db, conf)
        mailer.send_email(conf.email_noreply, to_, subject, msg)
