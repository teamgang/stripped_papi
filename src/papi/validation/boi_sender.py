import json
from sqlalchemy.orm import Session

from papi.core import plog
from papi.models import (
    Plan,
    Project,
    Notification,
    PLASTICITY_CUSTOM,
    PLASTICITY_LIMITLESS,
    PLASTICITY_STATIC,
)
from papi.utils.notifications import BoiSenderIn
from papi.validation.validation import Validation


class ValidateBoiSender:
    @staticmethod
    def all_fields(
            bs: BoiSenderIn, db: Session) -> bool:
        return ValidateBoiSender.is_token_valid(bs, db).is_valid and \
            ValidateBoiSender.is_token_valid(bs, db).is_valid

    @staticmethod
    def is_plasticity_correct(
            bs: BoiSenderIn,
            db: Session) -> Validation:
        validation = Validation()
        if not bs.terms_accepted_version:
            msg = f'terms not accepted for payment_attempt {pa.as_dict()}'
            plog.critical(msg)
            validation.addError(msg)
        return validation


    @staticmethod
    def is_token_valid(
            bs: BoiSenderIn,
            db: Session) -> Validation:
        validation = Validation()
        if not bs.terms_accepted_version:
            msg = f'terms not accepted for payment_attempt {pa.as_dict()}'
            plog.critical(msg)
            validation.addError(msg)
        return validation

