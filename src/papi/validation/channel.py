from sqlalchemy import and_
from sqlalchemy.orm import Session

from papi.core import Puid
from papi.core.errors import (
    ChannelRouteTakenError,
    makeNameBadWordError,
    makeRouteBadWordError,
    NameTooLongError,
    NameTooShortError,
    NotesTooLongError,
    RouteNotAlnumError,
    RouteTooLongError,
    RouteTooShortError,
)
from papi.validation.word_restrictions import find_a_restricted_word
from papi.models import (
    Channel,
)
from papi.validation.validation import Validation


class ValidateChannel:
    @staticmethod
    def all_fields_obj(channel: Channel,
                       db: Session) -> Validation:
        vv = Validation()
        vv.perform(ValidateChannel.check_name(channel.name))
        vv.perform(ValidateChannel.check_notes(channel.notes))
        vv.perform(ValidateChannel.check_route(
            channel.route, channel.project_id, db))
        return vv

    @staticmethod
    def all_fields(notes: str,
                   route: str,
                   project_id: str,
                   db: Session) -> bool:
        return ValidateChannel.check_notes(notes).is_valid and \
            ValidateChannel.check_route(route, project_id, db).is_valid

    @staticmethod
    def check_name(name: str) -> Validation:
        validation = Validation()
        if len(name) < 2:
            validation.add_api_error(NameTooShortError)

        if len(name) > 100:
            validation.add_api_error(NameTooLongError)

        word = find_a_restricted_word(name.lower())
        if word:
            validation.add_api_error(makeNameBadWordError(word))

        return validation

    @staticmethod
    def check_notes(notes: str) -> Validation:
        validation = Validation()
        if len(notes) > 8000:
            validation.add_api_error(NotesTooLongError)
        return validation

    @staticmethod
    def check_route(route: str, project_id: Puid, db: Session) -> Validation:
        validation = Validation()
        route_lower = route.lower()

        if len(route) < 2:
            validation.add_api_error(RouteTooShortError)

        if len(route) > 1000:
            validation.add_api_error(RouteTooLongError)

        word = find_a_restricted_word(route_lower)
        if word:
            validation.add_api_error(makeRouteBadWordError(word))

        if not route.isalnum:
            validation.add_api_error(RouteNotAlnumError)

        channel_check: Channel = db.query(Channel).filter( #type:ignore
            and_(Channel.project_id == project_id,
                 Channel.route_lower == route_lower)).first()

        if channel_check:
            validation.add_api_error(ChannelRouteTakenError)

        return validation
