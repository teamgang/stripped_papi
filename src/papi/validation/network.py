from sqlalchemy.orm import Session

from papi.core.errors import (
    NameTooLongError,
    NameTooShortError,
    NetworkRouteTakenError,
    NotesTooLongError,
    makeNameBadWordError,
    makeRouteBadWordError,
    RouteNotAlnumError,
    RouteTooLongError,
    RouteTooShortError,
)
from papi.validation.word_restrictions import find_a_restricted_word
from papi.models import (
    Network,
)
from papi.validation.validation import Validation


class ValidateNetwork:
    @staticmethod
    def all_fields_obj(network: Network,
                       db: Session) -> Validation:
        vv = Validation()
        vv.perform(ValidateNetwork.check_name(network.name))
        vv.perform(ValidateNetwork.check_notes(network.notes))
        vv.perform(ValidateNetwork.check_route(network.route, db))
        return vv

    @staticmethod
    def all_fields(notes: str,
                   route: str,
                   db: Session) -> bool:
        return ValidateNetwork.check_notes(notes).is_valid and \
            ValidateNetwork.check_route(route, db).is_valid

    @staticmethod
    def check_name(name: str) -> Validation:
        validation = Validation()
        word = find_a_restricted_word(name.lower())
        if len(name) < 2:
            validation.add_api_error(NameTooShortError)

        if len(name) > 100:
            validation.add_api_error(NameTooLongError)

        if word:
            validation.add_api_error(makeNameBadWordError(word))

        return validation

    @staticmethod
    def check_notes(notes: str) -> Validation:
        validation = Validation()
        if len(notes) > 8000:
            validation.add_api_error(NotesTooLongError)
        return validation

    @staticmethod
    def check_route(route: str, db: Session) -> Validation:
        validation = Validation()
        route_lower = route.lower()

        if len(route) < 2:
            validation.add_api_error(RouteTooShortError)

        if len(route) > 1000:
            validation.add_api_error(RouteTooLongError)

        word = find_a_restricted_word(route_lower)
        if word:
            validation.add_api_error(makeRouteBadWordError(word))

        if not route.isalnum:
            validation.add_api_error(RouteNotAlnumError)

        network_check: Network = db.query(Network).filter( #type:ignore
            Network.route_lower == route_lower).first()

        if network_check:
            validation.add_api_error(NetworkRouteTakenError)

        return validation
