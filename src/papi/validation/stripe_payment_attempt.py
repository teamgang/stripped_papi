import json
from sqlalchemy.orm import Session

from papi.core import plog
from papi.models import (
    BASKET_ITEM_TYPE_PLAN,
    Plan,
    Project,
    SILO_ADD_CREDIT,
    SILO_BASKET,
    SILO_BILL_PAY,
    SILO_PLAN,
    StripePaymentAttempt)
from papi.utils.basket_item import BasketItem
from papi.validation.validation import Validation


class ValidateStripePaymentAttempt:
    @staticmethod
    def all_fields(
            pa: StripePaymentAttempt, db: Session) -> bool:
        if pa.silo == SILO_ADD_CREDIT or \
                pa.silo == SILO_BILL_PAY or \
                pa.silo == SILO_PLAN:
            return ValidateStripePaymentAttempt.is_agreement_accepted(
                pa).is_valid

        elif pa.silo == SILO_BASKET:
            return ValidateStripePaymentAttempt.\
                total_price_and_basket_items(pa, db).is_valid and \
            ValidateStripePaymentAttempt.is_agreement_accepted(pa).is_valid

        else:
            msg = f'invalid silo for {pa.attempt_id}'
            plog.critical(msg)
            raise RuntimeError(msg)

    @staticmethod
    def is_agreement_accepted(
            pa: StripePaymentAttempt) -> Validation:
        validation = Validation()
        if not pa.terms_accepted_version:
            msg = f'terms not accepted for payment_attempt {pa.as_dict()}'
            plog.critical(msg)
            validation.addError(msg)
        return validation

    @staticmethod
    def total_price_and_basket_items(
            pa: StripePaymentAttempt, db: Session) -> Validation:
        validation = Validation()

        project = db.query(Project).filter(  #type:ignore
            Project.project_id == pa.project_id).first()
        if project.credit < pa.credit_applied:
            msg = f'invalid credit applied for payment_attempt {pa.as_dict()}'
            plog.critical(msg)
            validation.addError(msg)

        basket = json.loads(str(pa.basket))
        basket_total_price = 0
        for itemRaw in basket:
            item = BasketItem(**itemRaw)
            basket_total_price += item.price

            if item.item_type == BASKET_ITEM_TYPE_PLAN:
                plan = db.query(Plan).filter(  #type:ignore
                    Plan.plan_id == item.plan_id).first()

                if item.price != plan.price:
                    msg = f'invalid item price in payment_attempt {pa.as_dict()}'
                    plog.critical(msg)
                    validation.addError(msg)

        if basket_total_price != pa.total_price:
            msg = f'invalid total price for payment_attempt {pa.as_dict()}'
            plog.critical(msg)
            validation.addError(msg)

        return validation
